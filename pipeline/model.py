from data_analysis.util import classifyTweet
from data_analysis.constants import parameters
from data_analysis.evaluator import Evaluator
from data_analysis.text_analysis import cleanTweet
# from data_analysis.tokenizer_reducer import TokenizerReducer
import apache_beam as beam
import logging
from pipeline.vision import checkVisionAPI
import os, configparser
config = configparser.ConfigParser(inline_comment_prefixes='#')
config.read('settings.cfg')
if os.path.exists(config.get('ALT_CONFIG', 'path')):
  config.read('alt_settings.cfg') # config file ignored by git

class Preprocess(beam.DoFn):
    def process(self, element, parameters):
        element["processedText"]=cleanTweet(element['text'], parameters)
        logging.info("Preprocess"+element["processedText"])
        return [element]


class RunModelText(beam.DoFn):
    def process(self, element, models):
        s=element["processedText"]
        txt=[s]
        logging.info(s+" for "+ str(element["institution_uri"]))
        if (element["institution_uri"]) in models:
            model= models[element["institution_uri"]]
        else:
            model=list(models.items())[0]
        if isinstance(model, tuple):
            model = model[1]
        probabilities,classification  = classifyTweet(model, txt)

        element["classification"]=classification
        element["probabilities"]=probabilities
        return [element]

class RunModelImage(beam.DoFn):
    def process(self, element, models):
        s=element["processedText"]
        txt=[s]
        model= models[element["institution_uri"]]
        classification, probabilities = classifyTweet(model, txt)

        element["classification"]=classification
        element["probabilities"]=probabilities
        return [element]

class ProcessImage(beam.DoFn):
    def process(self, element):
        for img in element["photos"]:
            print (img["media_url"])
            img["visionAPI"]=checkVisionAPI(img["media_url"])
        return [element]

# each class must have at least 5 instances
class TrainModel(beam.DoFn):
    '''Train a model to classify text from tweets. CSV file should have two columns (text and signal)'''
    def process(self, element, parameters):
        numProcesses = 1
        evaluator = Evaluator()

        logging.info('Training text models...')
        logging.info('Institution:'+str(element["institution_uri"]))
        
        bestResults = evaluator.trainAndSelectModel(config, parameters, data=element['array'], nproc=numProcesses)
        # bestResults = evaluator.trainAndSelectModel(config, parameters, data='amtlight.csv', nproc=numProcesses)
        
        logging.info('Trained models. Using:', bestResults['modelName'], '(Score:', bestResults['microF1'], ')')

        # Keys in 'bestResults' include: modelName, accuracy, microF1, macroF1, weightedF1, model, metadata, tokenizer
        element['bestScore'] = bestResults['microF1'] # float
        element['bestModel'] = bestResults['model'] # object
        element['bestModelName'] = bestResults['modelName'] # str
        element['metadata'] = bestResults['metadata'] # dict or None
        element['tokenizer'] = bestResults['tokenizer'] # object or None
        return [element]

class PrepareData(beam.DoFn):
    def process(self, element):
        result=[]
        tweets=element["data"]
        for tweet in tweets:
#            logging.info(tweet)
            if "classification" in tweet:
                el=[]
                el.append(tweet["text"])
                if tweet["classification"][0]=="no_signal" :
                    tweet["classification"][0]="No Signal"
                el.append(tweet["classification"])
                result.append(el)
        element["array"]=result
        logging.info("----------------------------------------------------------------------")
        logging.info(element["institution_uri"])

        return [element]

