import apache_beam as beam
import logging

class ProcessCSV(beam.DoFn):
    def process(self, element):
        try:
            fullLine = element.strip().split(",")
#            logging.info("FULL LINE  " + str(fullLine))

            l0=""
            for x in range(len(fullLine)-1):

                l0=l0+fullLine[x].strip().replace("\"","")
#            l0 =  fullLine[0].strip() # encode('ascii','ignore').encode('utf-8')
            l1 = fullLine[len(fullLine)-1].strip().replace("\"","")
            #            logging.info("FULL LINE 0 " +str(l0))
            #           logging.info("FULL LINE 1 " + l1)

            data = {"id": l0,"text": l0, "classification": [l1],"institution_uri":0}
            return [data]
        except:
            logging.exception('')
            pass


