import logging
import apache_beam as beam
import json
import re
#import unicodedata
logging.getLogger().setLevel(logging.INFO)


class ProcessJSONTweetPandera(beam.DoFn):
    def process(self, element):
        try:
#            logging.info("MESSAGE RECEIVED\n"+element)
            s=str(element).replace("''", "\"")
            s=s.replace("None,", "\"None\",")
            s=str(s.encode('unicode-escape').decode())
#            tweet=json.loads(s)
            s=str(s)+"\n"
            tweet=json.loads(element)
            f = open("tweets3.json", "a+")
            f.write(s)
            created_at          =   tweet["data"]["created_at"]
            content_id          =   tweet["data"]["id"]
            twitter_user_id     =   tweet["data"]["user_id"]
            twitter_user_name   =   tweet["data"]["user_name"]
            person_type="1"
   #         avatar_url=tweet["data"]["avatar_url"]
#            birth_date
            id                  =   tweet["id"]
            person_uri          =   tweet["data"]["person_uri"]
            last_name           =   tweet["data"]["last_name"]
            first_name          =   tweet["data"]["first_name"]
            real_name           =   first_name+" "+last_name
            institution_uri     =   tweet["data"]["institution_uri"]
            photos              =   tweet["data"]["photos"]
            videos              =   tweet["data"]["videos"]
 #           ticket_uri
 #           content_uri
 #           source
            text                =   tweet["data"]["text"].replace("\\n", "").replace("\n", " ").replace("\\t", " ").replace(".", " ").replace("\r", " ").replace("\\", " ").replace("#", " ").replace('“', ' ')
            text                =   re.sub('[^a-zA-Z0-9 \n\.]', '', text)
#            in_reply_to_status_id=tweet["data"]["in_reply_to_status_id"]
#            in_reply_to_user_id=tweet["data"]["in_reply_to_user_id"]
#            user=tweet["data"]["user"]
#            entities=tweet["data"]["entities"]


            data = {"text": text, "created_at": created_at, "id":id,
            "content_id": content_id,
            "twitter_user_id":twitter_user_id,
            "twitter_user_name": twitter_user_name,
            "person_type":  "1",
#            "avatar_url": avatar_url,
            "person_uri": person_uri,
            "last_name":last_name,
            "first_name":first_name,
            "real_name": real_name,
            "institution_uri": institution_uri,
            "photos": photos,
            "videos":  videos

            #                    "in_reply_to_status_id":in_reply_to_status_id, "in_reply_to_user_id":in_reply_to_user_id,
#                    "user":user, "entities":entities,
#                    "media":med
                    }
#            logging.info(str(data))
            return [data]
        except:
            logging.exception('')
            pass

# DEPRECTATED, USED for raw tweet data from fast-protet PubSub
class ProcessJSONTweet(beam.DoFn):
    def process(self, element):
        try:
            tweet=json.loads(element)

            text                    =   tweet["data"]["text"]
            created_at              =   tweet["data"]["created_at"]
            id                      =   tweet["id"]
            in_reply_to_status_id   =  tweet["data"]["in_reply_to_status_id"]
            in_reply_to_user_id     =   tweet["data"]["in_reply_to_user_id"]
            user                    =   tweet["data"]["user"]
            entities                =   tweet["data"]["entities"]
            med                     =   []
#            media=tweet["data"]["entities"]["media"]
            if "media" in entities:
                media=entities["media"]
                for m in media:
                    med.append(m["media_url"])
            else:
                logging.info("MEDIA NOT FOUND")

            data = {"text": text, "created_at": created_at, "id":id,
                    "in_reply_to_status_id":in_reply_to_status_id, "in_reply_to_user_id":in_reply_to_user_id,
                    "user":user, "entities":entities,
                    "media":med
                    }
            return [data]
        except:
            logging.exception('')
            pass

class ConvertToJSON(beam.DoFn):
    def process(self, element):
        logging.info(element)
        data = {"text": "text", "classification": "classification", "insitution_uri":0}

        #            logging.info(str(data))
        return [data]
