import logging
from google.cloud import vision
import re

########################################################
def getLabels(client, image):
    label = dict()
    response_labels = client.label_detection(image=image)
    #LABELS
    labels = response_labels.label_annotations
    for l in labels:
        desc = re.sub('[^a-zA-Z0-9 \n\.]', '', l.description)
        desc = desc.replace("'", "").replace(".", "")
        label[desc]=l.score
    return label

########################################################
def getSafeSearch(client, image):
    safe_search = dict()
    response_safe = client.safe_search_detection(image=image)
    safe = response_safe.safe_search_annotation

    # Names of likelihood from google.cloud.vision.enums
    likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE',
                       'LIKELY', 'VERY_LIKELY')
    safe_search["adult"] = safe.adult
    safe_search["medical"] = safe.medical
    safe_search["spoof"] = safe.spoof
    safe_search["violence"] = safe.violence
    safe_search["racy"] = safe.racy
    return safe_search

def getFaces(client, image):
    response_face = client.face_detection(image=image)
    face = []
    faces = response_face.face_annotations
    for fac in faces:
        f = dict()
        f["anger"] = fac.anger_likelihood
        f["surprise"] = fac.surprise_likelihood
        f["joy"] = fac.joy_likelihood
        f["sorrow"] = fac.sorrow_likelihood
#        f["headware"] = fac.headware_likelihood
        f["detection_confidence"] = fac.detection_confidence
        face.append(f)
    return face

def getText(client, image):
    text = []
    response_text = client.text_detection(image=image)
    for txt in response_text.text_annotations:
        desc=txt.description
        desc=desc.replace("\\n", "").replace("\n", "").replace("\\t", "").replace("\r", "").replace("\\", "")
        desc = re.sub('[^a-zA-Z0-9 \n\.]', '', desc)
        if (len(desc) > 2):
#            text.append(desc.encode())
            text.append(desc)
    return text
#####################################################################################################
def getWeb(client, image):
    web=dict()
    response_web = client.web_detection(image=image).web_detection
    if response_web.web_entities:
        webs = response_web.web_entities
        for w in webs:
            descr                =   re.sub('[^a-zA-Z0-9 \n\.]', '', w.description)
            descr=descr.replace("'","").replace(".","")
            if (len(descr)>0):
                web[descr]= w.score
    return web

def checkVisionAPI(img):
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image()
    image.source.image_uri = img

    label       =getLabels(client, image)
    safe_search =getSafeSearch(client, image)
    face        =getFaces(client, image)
    text        =getText(client, image)
    web         =getWeb(client, image)
    result={
            "web":web,
            "face":face,
            "label":label,
            "safe":safe_search,
            "txt":text
            }
    return result

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    checkVisionAPI("https://pbs.twimg.com/media/EHFnzHmU4AAWkL9.jpg")
