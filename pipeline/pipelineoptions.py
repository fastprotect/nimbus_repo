from apache_beam.options.pipeline_options import PipelineOptions


class ProcessOptions(PipelineOptions):
    @classmethod
    def _add_argparse_args(cls, parser):
        parser.add_value_provider_argument(
                '--input',
                dest='input',
                type=str,
                required=False,
                default='tweetsFAIL.json',
                help='Input file to read. This can be a local file or a file in a Google Storage Bucket.')

        parser.add_value_provider_argument(
                '--entity',
                dest='entity',
                default='NimbusTweets2',
                type=str,
                required=False,
                help='The entity name')

        parser.add_value_provider_argument(
                '--bucket',
                dest='bucket',
                default='fastprotectnimbus',
                type=str,
                required=False,
                help='The bucket')

        parser.add_value_provider_argument(
                '--input_topic',
                dest='input_topic',
                default='fast-prep',
            type=str,
                required=False,
                help='The input_topic name')

        parser.add_value_provider_argument(
                '--train',
                dest='train',
                type=str,
                required=False,
                default='stripped.csv',
                help='Training file')

