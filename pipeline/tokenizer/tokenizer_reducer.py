from nltk.stem import LancasterStemmer, PorterStemmer, WordNetLemmatizer
from nltk.tokenize import TweetTokenizer, WordPunctTokenizer#, TreebankWordTokenizer
from nltk.tokenize.api import TokenizerI

class TokenizerReducer(TokenizerI):
  '''Tokenizes words and reduces them to inflectional forms using stemming/lemmatization'''
  _stemmer = None
  _lemmatizer = None
  _tokenizer = None
  def __init__(self, parameters):
    self.setParameters(parameters)
    
  def tokenize(self, text: str) -> list:
    '''Tokenizes words and applies '''
    if self._tokenizer == None:
      raise ValueError('No tokenizer has been set in TokenizerReducer')

    words = [word for word in self._tokenizer.tokenize(text) if len(word) > 0]
    if self._stemmer != None:
      words = [self._stemmer.stem(word) for word in words]

    if self._lemmatizer != None:
      words = [self._lemmatizer.lemmatize(word) for word in words]
    return words

  def setParameters(self, parameters: dict) -> None:
    self.setTokenizer(parameters)
    self.setStemmer(parameters)
    self.setLemmatizer(parameters)

  def setTokenizer(self, parameters: dict) -> None:
    '''parameters must contain key: tokenizer'''
    if parameters['tokenizer'] == 'wordPunct':
      self._tokenizer = WordPunctTokenizer()
    else:
      self._tokenizer = TweetTokenizer()
    
  def setStemmer(self, parameters: dict) -> None:
    '''Can use Porter & Lancaster Stemmers. Parameters must contain key: stemming'''
    if parameters['stemming'] == 'lancaster':
      self.stemmer = LancasterStemmer()
    elif parameters['stemming'] == 'porter':
      self.stemmer = PorterStemmer()
  
  def setLemmatizer(self, parameters: dict) -> None:
    '''Uses WordNet lemmatizer. Parameters must contain key: lemmatization'''
    if parameters['lemmatization'] is True:
      self._lemmatizer = WordNetLemmatizer()