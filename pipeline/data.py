import apache_beam as beam
from google.cloud import datastore
from google.cloud import bigquery
from google.cloud import storage
import logging
import datetime
import pytz
from datetime import datetime
import _pickle as cPickle

PROJECT_ID  =   "fms-dev-221613"
MODEL_KIND  =   "Model"
TWEET_KIND  =   "NimbusTweets2"
SEED_KIND  =   "SeedData"

class SaveToDataStore(beam.DoFn):
    def process(self, element, PROJECT_ID, ent):
        logging.info(str(element))
        ds=datastore.Client(PROJECT_ID)
        key = ds.key(ent, element["id"])
        entity = datastore.Entity(
            key=key)
        entity.update(element)
        ds.put(entity)
        return [element]

class SaveToBigQuery(beam.DoFn):
    def process(self, element, PROJECT_ID, ent):
        bigquery_client = bigquery.Client()

        query = "INSERT nimbus.model_results (created_at, content_id,twitter_user_id,twitter_user_name,avatar_url,real_name,person_uri ,last_name,first_name," \
                "institution_uri,model,model_version,reason_code,reason,confidence,person_type)   " \
                "VALUES(@created_at,@content_id,@twitter_user_id,@twitter_user_name,@avatar_url,@real_name,@person_uri,@last_name,@first_name,@institution_uri,@model,@model_version,7,@category,@score,1)"

        scores = element['probabilities']
        for score  in scores:
            for cat, val in score.items():

                query_params = [
                    bigquery.ScalarQueryParameter("institution_uri", "INT64", element["institution_uri"]),
                    bigquery.ScalarQueryParameter("created_at","TIMESTAMP",datetime.datetime(2016, 12, 7, 8, 0, tzinfo=pytz.UTC)),
                    bigquery.ScalarQueryParameter("content_id", "STRING", element["content_id"]),
                    bigquery.ScalarQueryParameter("twitter_user_id", "STRING", element["twitter_user_id"]),
                    bigquery.ScalarQueryParameter("twitter_user_name", "STRING", element["twitter_user_name"]),
                    bigquery.ScalarQueryParameter("avatar_url", "STRING", "avatar_url"),
                    bigquery.ScalarQueryParameter("real_name", "STRING", element["real_name"]),
                    bigquery.ScalarQueryParameter("person_uri", "INT64", element["person_uri"]),
                    bigquery.ScalarQueryParameter("last_name", "STRING", element["last_name"]),
                    bigquery.ScalarQueryParameter("first_name", "STRING", element["first_name"]),
                    bigquery.ScalarQueryParameter("model", "STRING", 'MODELNAMEGOESHERE'),
                    bigquery.ScalarQueryParameter("model_version", "FLOAT", 0.1),
                    bigquery.ScalarQueryParameter("category", "STRING", cat),
                    bigquery.ScalarQueryParameter("score", "FLOAT", val),
                ]
                job_config = bigquery.QueryJobConfig()
                job_config.query_parameters = query_params
                try:
                    query_job = bigquery_client.query(
                        query,
                        location="US",
                        job_config=job_config,
                    )  # API request - starts the query
                except Exception as e:
                    print(e)
        return [element]
######################################################################
def findAllModels():
    ds = datastore.Client(PROJECT_ID)
    query = ds.query(kind=MODEL_KIND)
    results = list(query.fetch())
    return results

# USED TO HAVE AN INITIAL LIST - DEV ONLY
def populateInitialModelDataStore():
    inst={26,24,12,31,28,27,103,104,17,33,32,1,29,106,21,18,23,20,22,19,25,102,105}
    ds = datastore.Client(PROJECT_ID)
    for ins in inst:
        model={"name": "logisticRegression.pkl", "version":0.1, "created_at":"Wed Oct 16 15:43:59 +0000 2019"}
        data = {"model": model, "institution_uri": ins}
        key = ds.key(MODEL_KIND, ins)
        entity = datastore.Entity(
            key=key)
        entity.update(data)
        ds.put(entity)

####################################
## PIPELINE 3 RELATED FUNCTIONS FOR MODEL RETRAINING
class ReadInstitutionDataFromDataStore(beam.DoFn):
    def process(self, element):
        ds = datastore.Client(PROJECT_ID)
        query = ds.query(kind=TWEET_KIND)
        query.add_filter("institution_uri", '=',element)
        results = list(query.fetch())
        queryDefault = ds.query(kind=SEED_KIND)
        queryDefault.add_filter("institution_uri", '=',0)
        resultsDefault = list(queryDefault.fetch())
        results=results+resultsDefault
        sliceObj = slice(0, 500)
        results=results[sliceObj]
        result={"institution_uri":element, "data":results }
        logging.info(str(element)+" "+str(len(results))+" DEFAULT "+str(len(resultsDefault)))
        return [result]

class SaveModelToBucket(beam.DoFn):
    def process(self, element,bucketName):
        modelName = element["bestModelName"]
        prefix = str(element["institution_uri"])+"/"+str(element["version"])+"/"
        
        if element['tokenizer'] == None: # assume keras model exists
            filenames = [modelName + '.h5',  'tokenizer.pkl', 'metadata.txt']

            element['model'].save(filenames[0])

            with open(filenames[1], 'wb') as fid:
                cPickle.dump(element['tokenizer'], fid)
                
            with open(filenames[2], 'wb') as fid:
                fid.write(element['metadata'], fid)

        else: # assume sklearn model exists
            filenames = [ modelName + '.pkl' ]
            with open(modelName + '.pkl', 'wb') as fid:
                cPickle.dump(element['bestModel'], fid)

        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucketName)
        
        for name in filenames:
          blob = bucket.blob(prefix + name)
          blob.upload_from_filename(name)
        return [element]

class SaveModelDataToDataStore(beam.DoFn):
    def process(self, element):
        ds = datastore.Client(PROJECT_ID)
        logging.info(element["institution_uri"])
        key = ds.key(MODEL_KIND, element["institution_uri"]) # should 'ins' be 'element.institution_uri'?
#        entity = datastore.Entity(key=key)
 #       key = client.key('Task', 'sample_task')
        entity = ds.get(key)
        logging.info("VERSION????")
        logging.info(entity)
 #       logging.info(entity['version'])

        if  'version' in entity:
            version=entity['version']
            logging.info("VERSION FOUND ")
        else:
            logging.info("VERSION NOT FOUND ")
            version=0

        version=version+1
        if  'scores' in entity:
            scores=entity['scores']
        else:
            scores={}
        logging.info(scores)
        scores["v" + str(version)]={"score": element["bestScore"], "name": element["bestModelName"],
                                    "created_at": datetime.now()}
        logging.info(scores)
        entity.update({
            "score" : element["bestScore"],
            "name":element["bestModelName"],
            "created_at":datetime.now(),
            "version":version,
            "institution_uri":element["institution_uri"],
            "log":scores
        })
        ds.put(entity)
#        result.binary = element["bestModel"]
        element["version"]=version
        return [element]

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    populateInitialModelDataStore()

    # git clone https://vakaloud@bitbucket.org/fastprotect/nimbus_repo.git
