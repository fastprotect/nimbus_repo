import logging
from apache_beam.options.pipeline_options import PipelineOptions
import apache_beam as beam
from google.cloud import storage
from sklearn.externals.joblib import dump, load
from google.cloud import vision
from google.cloud.vision import types
from pipeline.unifymodels import UnifyModels
from pipeline.jsonprocessing import ConvertToJSON
from pipeline.csvprocessing import ProcessCSV
from pipeline.data import SaveModelDataToDataStore, findAllModels, SaveModelToBucket, ReadInstitutionDataFromDataStore, SaveToDataStore
from pipeline.pipelineoptions import ProcessOptions
from pipeline.model import RunModelText,RunModelImage,Preprocess, ProcessImage, TrainModel,PrepareData
from data_analysis.constants import configurations
import sys
import os
import _pickle as cPickle
import json

# LEAVE THIS, IT IS NOT LOADED WHEN DEPLOYED AS A PIPELINE
import nltk
nltk.download('wordnet')
nltk.download('stopwords')
sys.path.append("."+os.sep+"tokenizer")

PROJECT_ID = "fms-dev-221613"

parameters = configurations['default']


# MAIN DATA FLOW FUNCTION
def dataflow(argv=None):
    process_options = PipelineOptions(setup_file="./setup.py").view_as(ProcessOptions)

    p = beam.Pipeline(options=process_options)

    logging.info("STARTING PIPELINE...")
    (p
     | beam.Create([26,24])
#     | beam.Create([26, 24, 12, 31, 28, 27, 103, 104, 17, 33, 32, 1, 29, 106, 21, 18, 23, 20, 22, 19, 25, 102, 105])
     #     | '0. Read list of institutions from data store ' >> ReadInstitutionsFromDataStore()
    | '1. Get Data for each insitution '>>beam.ParDo(ReadInstitutionDataFromDataStore())
    | '2. Prepare data: Convert JSON to 2 DIMENSIONAL ARRAY' >> beam.ParDo(PrepareData())
    | "3. Train Model " >> beam.ParDo(TrainModel(), parameters)
    | "4. Update model data in data store" >> beam.ParDo(SaveModelDataToDataStore())
    | "5. Store Model to bucket">>beam.ParDo(SaveModelToBucket(),process_options.bucket.default_value)

     )

    p.run().wait_until_finish()

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    dataflow()