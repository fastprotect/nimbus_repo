'''
@author: Christian O'Leary
'''

import logging
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.neighbors import KNeighborsClassifier
from sklearn.utils import check_X_y, check_array
from sklearn.utils.estimator_checks import check_estimator
from sklearn.utils.multiclass import check_classification_targets
from sklearn.utils.validation import check_is_fitted
import numpy as np

class ClassifierWrapper(ClassifierMixin):
    '''
    A wrapper for two models designed to classify images based on thei GCP Vision API output. 
    '''

    def __init__(self, params={}, **kwds):
        self.estimators_ = [KNeighborsClassifier(), KNeighborsClassifier()]
        self.params_ = params
        self.params_['estimators_'] = None
        self.params_['weights'] = None
        self.validate_params()


    def get_estimators_from_specifications(self):
        if self.params_['specifications'] != None:
            self.params_['estimators_'] = [
                self.params_['specifications'][key][0] for key in self.params_['specifications'].keys()
            ]


    def get_params(self, deep=None):
        return self.params_


    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            self.params_[parameter] = value
            self.validate_params()
        self.get_estimators_from_specifications()
            
        if hasattr(self, "X_") and hasattr(self, "y_"):    
            self.model.fit(self.X_, self.y_)
        return self


    def validate_params(self):
        if not isinstance(self.params_, dict):
            raise ValueError('Illegal instantiation of parameters (should be dict)')

        if 'specifications' in self.params_.keys() and self.params_['specifications'] != None:
            if not isinstance(self.params_['specifications'], dict):
                raise ValueError('specifications should be a dict')
            if 'features' not in self.params_['specifications'].keys() or 'textOnly' not in self.params_['specifications'].keys():
                raise ValueError('specification keys should be "features" and "textOnly". Received:', self.params_['specifications'].keys())

            for key in self.params_['specifications'].keys():
                if self.params_['specifications'][key] == None or not (isinstance(self.params_['specifications'][key], list) 
                or isinstance(self.params_['specifications'][key], tuple)) or len(self.params_['specifications'][key]) != 2:
                    raise ValueError('Each item in "specifications" should be a list/tuple ')
        
        if 'estimators_' in self.params_.keys() and self.params_['estimators_'] != None:
            if not (isinstance(self.params_['estimators_'], list) and len(self.params_['estimators_']) > 0):
                raise ValueError('Parameter "models" should be a list of models')
            for model in self.params_['estimators_']:
                if not isinstance(model, BaseEstimator):
                    raise ValueError('"estimators_" should only contain scikit-learn models. Found:', model)

        if 'weights' in self.params_.keys() and self.params_['weights'] != None:
            if not (isinstance(self.params_['estimators_'], list) and len(self.params_['estimators_']) == 2):
                raise ValueError('weights should be  a list of length 2')
            for weight in self.params_['weights']:
                if not isinstance(weight, float):
                    raise ValueError('weights should be floats. Found:', weight)

            
    def fit(self, X, y):
        '''Fits estimators on data. If "specifications" parameter is set, use those instead.'''
        if self.params_['specifications'] == None:
            logging.exception('Warning: no specifications for ClassifierWrapper')

        self.X_ = X
        self.y_ = y
        if self.params_['specifications'] == None:
            for i in range(len(self.params_['estimators_'])):
                self.params_['estimators_'][i].fit(X, y)
        else:
            self.params_['estimators_'] = []
            for key in self.params_['specifications'].keys():
                pipeline = self.params_['specifications'][key][0]
                featureNames = self.params_['specifications'][key][1]

                x = X[featureNames]
                if len(featureNames) == 1:
                    x = X['text']
                    
                pipeline.fit(x, self.y_)
                self.params_['estimators_'].append(pipeline)
                self.estimators_.append(pipeline)
        return self


    def predict(self, X):
        '''This classifier only uses soft voting.'''
        check_is_fitted(self, 'estimators_')
        majority = np.argmax(self.predict_proba(X), axis=1)
        return majority


    def predict_proba(self, X):
        '''Predict class probabilities for X in "soft" voting'''
        check_is_fitted(self, 'estimators_')
        if 'weights' in self.params_.keys() and self.params_['weights'] != None:
          avg = np.average(self._collect_probas(X), axis=0, weights=self.params_['weights'])
        else:
          avg = np.average(self._collect_probas(X), axis=0)
        return avg


    def _collect_probas(self, X):
        '''collect results from clf.predict_proba calls.'''
        if self.params_['specifications'] == None:
            probas = np.asarray([clf.predict_proba(X) for clf in self.params_['estimators_']])
        else:
            probas = []
            for key in self.params_['specifications'].keys():
                pipeline = self.params_['specifications'][key][0]
                featureNames = self.params_['specifications'][key][1]
                
                x = X[featureNames]
                if len(featureNames) == 1:
                    x = X['text'].astype(str)
                
                proba = pipeline.predict_proba(x)
                probas.append(proba)
        return probas


if __name__ == "__main__":
    clf = ClassifierWrapper()

    # check_estimator(clf)
    
    # clf.set_params(specifications={
    #     'textOnly': (LogisticRegression(), ['text'])
    #     'features': (LogisticRegression(), ['some_other_feature'])
    # })