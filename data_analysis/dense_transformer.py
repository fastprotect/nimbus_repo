'''
@author: Christian O'Leary
'''
from sklearn.base import TransformerMixin

'''Converts a sparse matrix into a dense one. Used in a (sklearn) pipeline.'''
class DenseTransformer(TransformerMixin):
  def fit(self, X, y=None, **fit_params):
    return self

  def transform(self, X, y=None, **fit_params):
    return X.todense()