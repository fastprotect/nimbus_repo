'''
@author: Christian O'Leary

This file contains the configuration options for cleaning and modelling 
as well as their possible values. Other values will raise errors.
'''
import warnings, sys, os, string, re
warnings.filterwarnings('ignore')
if not sys.warnoptions:
  warnings.simplefilter("ignore")
  os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses

from sklearn.dummy import DummyClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, PassiveAggressiveClassifier, Perceptron
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC, SVC
import pandas as pd

from tensorflow.keras.layers import BatchNormalization, Bidirectional, Conv1D, Dense, \
  Dropout, Embedding, Flatten, GlobalMaxPooling1D, GRU, Dropout, LeakyReLU, LSTM

defaultArchitecture = [ 
  Dense(1024, activation='relu'), Dropout(0.5), 
  Dense(512, activation='relu'), Dropout(0.5), 
  Dense(256, activation='relu'), Dropout(0.5), 
]
imageArchitectures = {
  # Each architecture will be preceded by a flatten layer and followed by a softmax layer
  'arch1': lambda : defaultArchitecture,
  'arch2': lambda : defaultArchitecture[-4:],
  'arch3': lambda : defaultArchitecture[-2:],
  'arch4': lambda : defaultArchitecture[-2],
  'arch5': lambda : [],
}

embeddingDir = 'embedding/'
embeddingPaths = {
  # 'no-embedding': None, # TODO: Need to develop code for no embedding layer
  # TODO: add option for generic untrained embedding layer
  # '50-glove': embeddingDir + 'glove.6B.50d.txt', # commented out as lowest performing embedding
  '100-glove': embeddingDir + 'glove.6B.100d.txt',
  '200-glove': embeddingDir + 'glove.6B.200d.txt',
  '300-glove': embeddingDir + 'glove.6B.300d.txt',
}

imageDirs = {
  'pandera': 'images/pandera/images/', 'art': 'images/art/images/', 'twitter': 'images/twitter/images/'
}

imageCsvDirs = {
  'pandera': 'images/pandera/csv/', 'art': 'images/art/csv/', 'twitter': 'images/twitter/csv/'
}

vApiFiles = {
  'pandera': 'images/pandera/csv/v-api-pandera.csv', 'art': 'images/art/csv/v-api-art.csv',
  'twitter': 'images/twitter/csv/v-api-twitter.csv',
}

signals = {
  'pandera': ['academic_misconduct', 'depressive', 'gambling', 'no_signal', 'offensive', 'substance_abuse'],
  'art': ['negative', 'positive'], 'twitter': ['negative', 'positive']
}

labelFiles = {
  'pandera': lambda: [
    (pd.read_csv('images/pandera/csv/pandera-train-image.csv'), 'path', 'signal'), 
    (pd.read_csv('images/pandera/csv/pandera-mbts.csv'), 'path', 'signal'), 
    (pd.read_csv('images/pandera/csv/pandera-mbts-baseline.csv'), 'path', 'signal'), 
  ],
  'art': lambda: [ (pd.read_csv('images/art/csv/signals_temp.csv'), 'path', 'signal'), ],
  'twitter': lambda: [ (pd.read_csv('images/twitter/csv/signals.csv'), 'path', 'signal'), ]
}

# Pre-processing parameters (which are case-sensitive)
# Notes: vectorizers do not apply custom tokenizers or stopword removal when using ngrams
parameters = {
  # 'dataAugmentation': [ None ],
  'expandContractions': [ True, False ],
  'lemmatization': [ True, False ],
  'lowercase': [ True, False ],
  'maxClassSize': [ None, int ],
  'maxVocabFeatures': [ None, int ],
  'ngram': [ (1, 1), (1, 2), (1, 3), (2, 3), (3, 3) ],
  'removeNonAscii': [ True, False ],
  'removeHashtags': [ True, False ],
  'removeHttp': [ True, False ],
  'removeMention': [ True, False ],
  'removePunctuation': [ True, False ],
  'removeStopwords': [ 'nltk', 'default', None ],
  'removeWww': [ True, False ],
  'resampling': [ None, 'randomOverSampling', 'randomUnderSampling'], #'adasyn', 'smote', 'smoteEnn', 'smoteTomek' ],
  'scaling': [ 'std', None ],
  'soup': [ True, False ],
  'spellCheck': [ 'textblob', 'autocorrect', None ],
  'stemming': [ 'lancaster', 'porter', None],
  'stratifyCv': [ True, False ],
  'stratifySplit': [ True, False ],
  'stripAccents': [ 'ascii', None ], # 'unicode',
  'tokenizer': [ 'tweet', 'wordPunct' ],
  'useLexicons': [ True, False ],
  'vectorizer': [ 'tfidf', 'count' ],
  'tfidf-norm': [ None, 'l1', 'l2' ],
  'tfidf-use_idf': [ False, True ],
  'tfidf-smooth_idf': [ False, True ],
  'tfidf-sublinear_tf': [ False, True ],
}

configurations = {
  'minimal': {
    'expandContractions': False, 'lemmatization': False, 'lowercase': False, 'maxClassSize': None, 
    'maxVocabFeatures': None, 'ngram': (1, 1), 'removeNonAscii': False, 'removeHashtags': False, 'removeHttp': False, 
    'removeMention': False, 'removePunctuation': False, 'removeStopwords': None, 'removeWww': False, 'resampling': None, 
    'scaling': None, 'soup': False, 'spellCheck': None, 'stemming': None, 'stratifyCv': True, 'stratifySplit': True, 
    'stripAccents': None, 'tokenizer': 'wordPunct', 'useLexicons': False, 'vectorizer': 'count', 'tfidf-norm': 'l1', 
    'tfidf-use_idf': True, 'tfidf-smooth_idf': True, 'tfidf-sublinear_tf': True,
  },
  'default': {
    'expandContractions': True, 'lemmatization': True, 'lowercase': True, 'maxClassSize': None, 
    'maxVocabFeatures': 1000000, 'ngram': (1, 1), 'removeNonAscii': True, 'removeHashtags': True, 'removeHttp': True, 
    'removeMention': True, 'removePunctuation': True, 'removeStopwords': 'nltk', 'removeWww': True, 'resampling': None, 
    'scaling': None, 'soup': True, 'spellCheck': None, 'stemming': 'porter', 'stratifyCv': False, 'stratifySplit': True, 
    'stripAccents': 'ascii', 'tokenizer': 'tweet', 'useLexicons': False, 'vectorizer': 'count', 'tfidf-norm': None, 
    'tfidf-use_idf': False, 'tfidf-smooth_idf': False, 'tfidf-sublinear_tf': False,
  },
  'pandera': {
    'expandContractions': True, 'lemmatization': True, 'lowercase': True, 'maxClassSize': None, 
    'maxVocabFeatures': 1000000, 'ngram': (1, 1), 'removeNonAscii': True, 'removeHashtags': True, 'removeHttp': True, 
    'removeMention': True, 'removePunctuation': True, 'removeStopwords': 'nltk', 'removeWww': True, 'resampling': None, 
    'scaling': None, 'soup': True, 'spellCheck': None, 'stemming': 'porter', 'stratifyCv': True, 'stratifySplit': True, 
    'stripAccents': 'ascii', 'tokenizer': 'tweet', 'useLexicons': False, 'vectorizer': 'count', 'tfidf-norm': None, 
    'tfidf-use_idf': False, 'tfidf-smooth_idf': False, 'tfidf-sublinear_tf': False,
  },
  'sstweet': {
    'expandContractions': True, 'lemmatization': True, 'lowercase': True, 'maxClassSize': None, 
    'maxVocabFeatures': 1000000, 'ngram': (1, 1), 'removeNonAscii': True, 'removeHashtags': True, 'removeHttp': True, 
    'removeMention': True, 'removePunctuation': False, 'removeStopwords': 'nltk', 'removeWww': True, 'resampling': None, 
    'scaling': 'std', 'soup': True, 'spellCheck': None, 'stemming': 'porter', 'stratifyCv': True, 'stratifySplit': True, 
    'stripAccents': 'ascii', 'tokenizer': 'tweet', 'useLexicons': False, 'vectorizer': 'count', 'tfidf-norm': None, 
    'tfidf-use_idf': False, 'tfidf-smooth_idf': False, 'tfidf-sublinear_tf': False,
  },
  'stsgold': { # TODO: revise
    'expandContractions': False, 'lemmatization': False, 'lowercase': False, 'maxClassSize': None, 
    'maxVocabFeatures': 1000000, 'ngram': (1, 1), 'removeNonAscii': False, 'removeHashtags': False, 'removeHttp': False, 
    'removeMention': False, 'removePunctuation': False, 'removeStopwords': None, 'removeWww': False, 'resampling': None, 
    'scaling': None, 'soup': False, 'spellCheck': None, 'stemming': 'porter', 'stratifyCv': True, 'stratifySplit': True, 
    'stripAccents': None, 'tokenizer': 'tweet', 'useLexicons': False, 'vectorizer': 'tfidf', 'tfidf-norm': 'l1', 
    'tfidf-use_idf': False, 'tfidf-smooth_idf': False, 'tfidf-sublinear_tf': False,
  },
}
configurations['amt'] = configurations['default']
configurations['amt']['maxClassSize'] = 7500
configurations['readable'] = dict(configurations['minimal'])
configurations['readable']['removeNonAscii'] = True
configurations['readable']['removeHashtags'] = True
configurations['readable']['removeHttp'] = True
configurations['readable']['removeMention'] = True
configurations['readable']['removeWww'] = True
configurations['readable']['soup'] = True

lexiconParameters = [
  dict(Lex=False, Afinn=False, vader=False, sentiStr=False, negonly=False, highvalue=False, soCal=False, mult=.1, gamb=False, dep=False, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=False, dep=True, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=False, dep=True, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=10, gamb=False, dep=True, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.1, gamb=False, dep=True, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=2, gamb=False, dep=True, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=False, dep=False, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=False, dep=False, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=10, gamb=False, dep=False, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.1, gamb=False, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=2, gamb=False, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=False, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=False, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=10, gamb=False, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.1, gamb=True, dep=False, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=2, gamb=True, dep=False, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.1, gamb=True, dep=False, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=2, gamb=True, dep=False, sub=False),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=True, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=True, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=10, gamb=True, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.1, gamb=True, dep=True, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=2, gamb=True, dep=True, sub=True),
]


models = {
  'bernoulliNB': lambda : BernoulliNB(),
  'decisionTree': lambda : DecisionTreeClassifier(),
  'dummy': lambda : DummyClassifier(),
  'gaussianNB': lambda : GaussianNB(),
  'logisticRegression': lambda : LogisticRegression(),
  'linearSvc': lambda : LinearSVC(),
  'knn': lambda : KNeighborsClassifier(),
  'passiveAggressive': lambda : PassiveAggressiveClassifier(),
  'perceptron': lambda : Perceptron(),
  'randomForest': lambda : RandomForestClassifier(),
  'svc': lambda : SVC(probability=True),
}

bestKerasTextModels = {
  '100-glove': ['cnn-5', 'lstm-3', 'lstm-6', 'lstm-7'],
  '200-glove': ['lstm-2', 'lstm-5', 'lstm-8'],
  '300-glove': ['cnn-5']
}

bestModels = {
  'bernoulliNB': lambda : BernoulliNB(alpha=0.2), 
  'decisionTree': lambda : DecisionTreeClassifier(class_weight=None, criterion='entropy', max_depth=50, max_features=None, max_leaf_nodes=None, min_samples_leaf=10, min_samples_split=2, splitter='random'),
  'linearSvc': lambda : LinearSVC(C=0.1, class_weight='balanced', loss='squared_hinge', multi_class='ovr', penalty='l2'),
  'logisticRegression': lambda : LogisticRegression(C=1, class_weight=None, multi_class='multinomial', penalty='l2', solver='sag'),
  'passiveAggressive': lambda : PassiveAggressiveClassifier(C=0.01, class_weight='balanced', early_stopping=False),
  'perceptron': lambda : Perceptron(alpha=1e-06, class_weight=None, early_stopping=True, penalty=None, tol=0.01),
}
bestModels['default'] = bestModels['linearSvc']
models['default'] = bestModels['default']

bestVApiTextModels = { # TODO
  'bernoulliNB': lambda : BernoulliNB(alpha= 0.0), 
  'decisionTree': lambda : DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None, max_features=None, max_leaf_nodes=None, min_samples_leaf=1, min_samples_split=2, splitter='best'),
  'knn': lambda : KNeighborsClassifier(algorithm='kd_tree', metric='euclidean', n_neighbors=4, weights='distance'),
  'linearSvc': lambda : LinearSVC(C=1, class_weight=None, loss='hinge', multi_class='ovr', penalty='l2'),
  'logisticRegression': lambda : LogisticRegression(C=1, class_weight=None, multi_class='ovr', penalty='l2', solver='liblinear'),
  'passiveAggressive': lambda : PassiveAggressiveClassifier(C=0.01, class_weight='balanced', early_stopping=False),
  'perceptron': lambda : Perceptron(alpha=1e-05, class_weight=None, early_stopping=False, penalty=None, tol=0.001),
  'svc': lambda : SVC(C=0.1, class_weight=None, kernel='linear', shrinking=True, probability=True),
}
bestVApiTextModels['default'] = bestVApiTextModels['svc']

bestVApiFeatureModels = { # TODO
  'bernoulliNB': lambda : BernoulliNB(alpha= 0.0), 
  'decisionTree': lambda : DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None, max_features=None, max_leaf_nodes=None, min_samples_leaf=1, min_samples_split=2, splitter='best'),
  'knn': lambda : KNeighborsClassifier(algorithm='kd_tree', metric='euclidean', n_neighbors=4, weights='distance'),
  'linearSvc': lambda : LinearSVC(C=1, class_weight=None, loss='hinge', multi_class='ovr', penalty='l2'),
  'logisticRegression': lambda : LogisticRegression(C=1, class_weight=None, multi_class='ovr', penalty='l2', solver='liblinear'),
  'passiveAggressive': lambda : PassiveAggressiveClassifier(C=0.01, class_weight='balanced', early_stopping=False),
  'perceptron': lambda : Perceptron(alpha=1e-05, class_weight=None, early_stopping=False, penalty=None, tol=0.001),
  'svc': lambda : SVC(C=0.1, class_weight=None, kernel='linear', shrinking=True, probability=True),
}
bestVApiFeatureModels['default'] = bestVApiFeatureModels['passiveAggressive']

gridSearch = {
  'bernoulliNB': {
    'classifier__alpha': [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
  },
  'decisionTree': {
    'classifier__criterion': ['gini', 'entropy'],
    'classifier__splitter': ['best', 'random'],
    'classifier__max_depth': [10, 50, None],
    'classifier__min_samples_split': [2, 16, 128],
    'classifier__min_samples_leaf': [1, 10, 100],
    'classifier__max_features': [None], # handled before modelling
    'classifier__max_leaf_nodes': [2, 8, 32, None],
    'classifier__class_weight': [None, 'balanced'],
  },
  'default': {},
  'dummy': {},
  'gaussianNB': {
    'classifier__var_smoothing': [1e-8, 1e-9, 1e-10]
  },
  'knn': {
    'classifier__n_neighbors': [2, 4, 8, 16],
    'classifier__weights': ['uniform', 'distance'],
    'classifier__algorithm': ['kd_tree', 'brute'],
    'classifier__metric': ['euclidean', 'manhattan', 'chebyshev'],
  },
  'logisticRegression': { 
    'classifier__C': [0.01, 0.1, 1, 10], 
    'classifier__class_weight': [None, 'balanced'], 
    'classifier__multi_class': ['multinomial', 'ovr'],
    'classifier__penalty': ['l1', 'l2', 'none'], 
    'classifier__solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'],
  },
  'linearSvc': {
    'classifier__C': [0.001, 0.01, 0.1, 1, 10],
    'classifier__loss': ['hinge', 'squared_hinge'],
    'classifier__penalty': ['l1', 'l2'],
    'classifier__multi_class': ['ovr', 'crammer_singer'],
    'classifier__class_weight': [None, 'balanced'],
  },
  'passiveAggressive': {
    'classifier__C': [0.001, 0.01, 0.1, 1, 10],
    'classifier__class_weight': [None, 'balanced'], 
    'classifier__early_stopping' : [False],
  },
  'perceptron': {
    'classifier__penalty': ['l1', 'l2', None],
    'classifier__alpha': [1e-6, 1e-5, 1e-4, 1e-3],
    'classifier__tol': [1e-2, 1e-3, 1e-4],
    'classifier__early_stopping': [True, False],
    'classifier__class_weight': [None, 'balanced'], 
  },
  'randomForest': { # using parameters from best Decision Tree
    'classifier__criterion': ['gini'],
    'classifier__max_depth': [None],
    'classifier__max_features': [None], # handled before modelling
    'classifier__n_estimators': [8, 16, 32, 64, 128],#, 256, 512, 1024],
    'classifier__min_samples_split': [2],
    'classifier__min_samples_leaf': [1],
    'classifier__max_leaf_nodes': [None],
    'classifier__class_weight': [None, 'balanced', 'balanced_subsample'],
  },
  'svc': {
    'classifier__C': [0.01, 0.1, 1, 10],
    'classifier__kernel': ['linear', 'rbf', 'sigmoid'],
    'classifier__shrinking': [True, False],
    'classifier__class_weight': ['balanced', None],
    'classifier__probability': [True],
  },
}

mentionPattern = r'@[A-Za-z0-9]+'
hashtagPattern = r'#[A-Za-z0-9]+'
httpspattern = r'https?://[^ ]+'
wwwPattern = r'www.[^ ]+'
punctuationPattern = r'[.,;:\-/\'?!"\(\){}\[\]\u2026]' # \u2026 matches elipsis
# combinedPattern = r'|'.join((ampersandPattern, httpspattern))

# taken from wikipedia: https://stackoverflow.com/questions/19790188/expanding-english-language-contractions-in-python
contractions = {
  "ain't": "am not / are not / is not / has not / have not", "aren't": "are not / am not", "can't": "cannot",
  "can't've": "cannot have", "'cause": "because", "could've": "could have", "couldn't": "could not", 
  "couldn't've": "could not have", "didn't": "did not", "doesn't": "does not", "don't": "do not", "hadn't": "had not",
  "hadn't've": "had not have", "hasn't": "has not", "haven't": "have not", "he'd": "he had / he would",
  "he'd've": "he would have", "he'll": "he shall / he will", "he'll've": "he shall have / he will have",
  "he's": "he has / he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will", 
  "how's": "how has / how is / how does", "I'd": "I had / I would", "I'd've": "I would have", "I'll": "I shall / I will",
  "I'll've": "I shall have / I will have", "I'm": "I am", "I've": "I have", "isn't": "is not", "it'd": "it had / it would",
  "it'd've": "it would have", "it'll": "it shall / it will", "it'll've": "it shall have / it will have", 
  "it's": "it has / it is", "let's": "let us", "ma'am": "madam", "mayn't": "may not", "might've": "might have",
  "mightn't": "might not", "mightn't've": "might not have", "must've": "must have", "mustn't": "must not", 
  "mustn't've": "must not have", "needn't": "need not", "needn't've": "need not have", "o'clock": "of the clock", 
  "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not", "sha'n't": "shall not", 
  "shan't've": "shall not have", "she'd": "she had / she would", "she'd've": "she would have", "she'll": "she shall / she will",
  "she'll've": "she shall have / she will have", "she's": "she has / she is", "should've": "should have", 
  "shouldn't": "should not", "shouldn't've": "should not have", "so've": "so have", "so's": "so as / so is", 
  "that'd": "that would / that had", "that'd've": "that would have", "that's": "that has / that is", 
  "there'd": "there had / there would", "there'd've": "there would have", "there's": "there has / there is", 
  "they'd": "they had / they would", "they'd've": "they would have", "they'll": "they shall / they will",
  "they'll've": "they shall have / they will have", "they're": "they are", "they've": "they have", "to've": "to have",
  "wasn't": "was not", "we'd": "we had / we would", "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have",
  "we're": "we are", "we've": "we have", "weren't": "were not", "what'll": "what shall / what will", 
  "what'll've": "what shall have / what will have", "what're": "what are", "what's": "what has / what is", "what've": "what have",
  "when's": "when has / when is", "when've": "when have", "where'd": "where did", "where's": "where has / where is", 
  "where've": "where have", "who'll": "who shall / who will", "who'll've": "who shall have / who will have", 
  "who's": "who has / who is", "who've": "who have", "why's": "why has / why is", "why've": "why have", "will've": "will have",
  "won't": "will not", "won't've": "will not have", "would've": "would have", "wouldn't": "would not", 
  "wouldn't've": "would not have", "y'all": "you all", "y'all'd": "you all would", "y'all'd've": "you all would have", 
  "y'all're": "you all are", "y'all've": "you all have", "you'd": "you had / you would", "you'd've": "you would have",
  "you'll": "you shall / you will", "you'll've": "you shall have / you will have", "you're": "you are", "you've": "you have"
}
additionalEntries = {}
for key, item in contractions.items(): # add terms without apostrophes
  if "'" in key:
    additionalEntries[key.replace("'", '')] = item
contractions.update(additionalEntries)

capatalisedContractions = { string.capwords(key) : string.capwords(contractions[key]) for key in contractions.keys() }
temp = capatalisedContractions.copy()
temp.update(contractions)
contractions = temp
contractionPattern = re.compile(r'\b(' + '|'.join(contractions.keys()) + r')\b')

# # convert all urls to sting "URL"
# tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', 'URL', tweet)
# # convert all @username to "AT_USER"
# tweet = re.sub('@[^\s]+', 'AT_USER', tweet)
# # correct all multiple white spaces to a single white space
# tweet = re.sub('[\s]+', ' ', tweet)
# # convert "#topic" to just "topic"
# tweet = re.sub(r'#([^\s]+)', r'\1', tweet)

labelEncoding = {
  'offensive': 0, 'Offensive': 0,
  'depressive': 1, 'Depressive': 1,
  'substance_abuse': 2, 'Substance Abuse': 2,
  'gambling': 3, 'Gambling': 3,
  'academic_misconduct': 4, 'Academic Misconduct': 4,
  'no_signal': 5, 'No Signal': 5,
}
reverseEncoding = { value: key for (key, value) in labelEncoding.items() }

# optimizers = { 'adam': 'adam', 'adagrad': Adagrad(learning_rate=0.00001), 'adadelta': Adadelta(),  }

textArchitectures = {
  # architectures will be added to a Sequential model and followed by a softmax layer
  # LSTM ARCHITECTURES
  # 'lstm-0': lambda params : [ LSTM(16) ],
  # 'lstm-1': lambda params : [ LSTM(32) ],
  'lstm-2': lambda params : [ LSTM(64) ],
  'lstm-3': lambda params : [ LSTM(128) ],
  # 'lstm-4': lambda params : [ LSTM(64, return_sequences=True), Dropout(0.2), LSTM(32), ],
  'lstm-5': lambda params : [ # LSTM => DROP => LSTM => DROP => LSTM
    LSTM(128, return_sequences=True), Dropout(0.2), LSTM(64, return_sequences=True),  Dropout(0.2), LSTM(32),
  ],
  'lstm-6': lambda params : [ Bidirectional(LSTM(64)), Dropout(0.2), ],
  'lstm-7': lambda params : [ Bidirectional(LSTM(64)), Dropout(0.2), Dense(64, activation= 'relu'), ],
  'lstm-8': lambda params : [ Bidirectional(LSTM(32)), Dropout(0.2), Dense(32, activation= 'relu'), ],
  # 'lstm-9': lambda params : [ # BI DIR => DROP => BI DIR => DROP => DENSE => DENSE
  #   Bidirectional(LSTM(64, return_sequences=True)), Dropout(0.2), Bidirectional(LSTM(32)), 
  #   Dropout(0.2), Dense(64, activation= 'relu'), Dense(32, activation= 'relu'),
  # ],
  # GRU ARCHITECTURES
  # 'gru-2': lambda params : [ GRU(units=32, dropout=0.2, recurrent_dropout=0.2), ],
  # 'gru-3': lambda params : [ GRU(units=128, dropout=0.2, recurrent_dropout=0.2), BatchNormalization() ],
  # 'gru-4': lambda params : [ GRU(units=64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True), GRU(32), ],
  # 'gru-5': lambda params : [ # GRU => GRU => GRU
  #   GRU(units=128, dropout=0.2, recurrent_dropout=0.2, return_sequences=True), 
  #   GRU(units=64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True), GRU(units=32, dropout=0.2, recurrent_dropout=0.2),
  # ],
  # 'gru-6': lambda params : [ Bidirectional(GRU(units=64, dropout=0.2, recurrent_dropout=0.2)), ],
  # 'gru-7': lambda params : [ Bidirectional(GRU(units=64, dropout=0.2, recurrent_dropout=0.2)),  Dense(64, activation= 'relu'), ],
  # CNN ARCHITECTURES
  # 'cnn-1': lambda params : [ # CONV => POOL => DENSE
  #   Conv1D(64, 5, input_shape=params['shape'], activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu')
  # ],
  # 'cnn-2': lambda params : [ # CONV => CONV => POOL => DENSE
  #   Conv1D(128, 5, input_shape=params['shape'], activation='relu'), Conv1D(64, 5, activation='relu'), 
  #   GlobalMaxPooling1D(), Dense(64, activation='relu'),
  # ],
  # 'cnn-3': lambda params : [ # CONV => CONV => CONV => POOL => DENSE
  #   Conv1D(128, 5, input_shape=params['shape'], activation='relu'), Conv1D(64, 5, activation='relu'),
  #   Conv1D(32, 5, activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu'),
  # ],
  # 'cnn-4': lambda params : [ # CONV => CONV => BATCH NORM => POOL => DENSE => BATCH NORM =>  DENSE
  #   Conv1D(128, 5, input_shape=params['shape'], activation='relu'), Conv1D(64, 5, activation='relu'), 
  #   Conv1D(32, 5, activation='relu'), BatchNormalization(), GlobalMaxPooling1D(), Dense(64, activation='relu'), 
  #   BatchNormalization(), Dense(32, activation='relu'),
  # ],
  'cnn-5': lambda params : [ # CONV => POOL => DENSE
    Conv1D(64, 3, input_shape=params['shape'], activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu')
  ],
  # 'cnn-6': lambda params : [ # CONV => POOL => DENSE
  #   Conv1D(64, 7, input_shape=params['shape'], activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu')
  # ],
  # 'cnn-7': lambda params : [ # CONV => POOL => DENSE
  #   Conv1D(64, 5, input_shape=params['shape'], activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu')
  # ],
  # 'cnn-8': lambda params : [ # CONV => POOL => DENSE
  #   Conv1D(64, 5, input_shape=params['shape'], activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu')
  # ],
  # 'cnn-9': lambda params : [ # CONV => POOL => DENSE
  #   Conv1D(64, 5,  input_shape=params['shape'], activation='relu'), GlobalMaxPooling1D(), Dense(64, activation='relu')
  # ],
  # 'cnn-10': lambda params : [ # CONV => CONV => POOL => DENSE
  #   Conv1D(64, 5, input_shape=params['shape'], activation='relu'), Conv1D(32, 5, activation='relu'), 
  #   GlobalMaxPooling1D(), Dense(32, activation='relu')
  # ],
}