
import data_analysis.text_analysis  as ta
import data_analysis.constants as c
import data_analysis.constants2 as c2
from sklearn.pipeline import Pipeline
import numpy as np
import pandas as pd
from sklearn.model_selection import GridSearchCV, train_test_split,StratifiedKFold
from pycorenlp import StanfordCoreNLP
from sklearn.model_selection import ParameterGrid, cross_validate,cross_val_predict
nlp = StanfordCoreNLP('http://localhost:9000')
#Have put in a reweighting of test vectors and the name step is to chang back the vectoriser
print("have set this now")
from time import time
def modelLex(xTrain, xTest, yTrain, yTest, originalLabels,encodedLabels, models, parameters, outputFunction=None, nproc=1,seed=None,
             verbose=True):



    '''
    Creates pipelines, performs a grid search and calculates scores. Returns a list of tuples
    (one for each model/grid search). Each tuple contains the best model's predictions,
    accuracy, F1 scores, and the grid search output (GridSearchCV.cv_results_)
    '''

    print("HERE in modellex  ARE THE PARAMS",parameters, c.parameters)
    ta.validateParameters(parameters, c.parameters)
    results = []
    #for i in range(0,len(c2.lexiconParameters)):
    for i in range(0,10):
        print("AM NOW ON LOOP ",i)
        for modelName in models.keys():
            if verbose:
                print('\nUsing THIS MODEL ', modelName)
                startTime = time()

            # Create model & pipeline
            classifier = c.models[modelName]()
            print ("classifier is ",classifier)

            pipeline = getPipelinelEX(modelName, classifier, parameters)
            vec = ta.getVectorizer(parameters)
            rawfeats = vec.fit(xTrain)
            print("rawfeats" ,np.shape(rawfeats))
            features = vec.fit_transform(xTrain)
            print("features", np.shape(features))

            # now need to call the lexicon weighting algorithm
            features = getFeaturesLexicon(rawfeats, features, c2.lexiconParameters[i])
            print("features post lexicon", np.shape(features))
            print("features returned position40855",features[:,40855])
            # Perform a grid search
            verbosity = 0
            if verbose:
                verbosity = 1

            print("AM DOING THE NON STRATIFIED lexCV SEARCH")
            print("MOELS NOW IS ",models[modelName])

            splits = 5
            parameters['stratify'] = False
            if parameters['stratify'] == True:
                print("AM USINF STRATIFIED KFOLD")
                splits = StratifiedKFold(n_splits=splits,shuffle =True)
            print("splits is ",splits)
            cvSearch = GridSearchCV(pipeline, models[modelName], cv=splits, n_jobs=nproc, error_score=0.0,
                                    verbose=verbosity)
            modelnew =cvSearch.fit(features, yTrain)
            # Test the model and generate scores

            # Test the model and generate scores


            print("features train" ,np.shape(features))
            # Test the model and generate scores

            # rawfeats = vec.fit(xTest)
            # change here 28/9 - reweighting the test lexicon
            featurestest = vec.transform(xTest)
            featurestest = getFeaturesLexicon(rawfeats, featurestest, c2.lexiconParameters[i])
            print("features test" ,np.shape(features))
            # now need to call the lexicon weighting algorithm
            # features = getFeaturesLexicon(rawfeats, features, c.lexiconParameters)
            #yPred = cvSearch.predict(featurestest)
            print("THESE ARE THE BEST PARAMS",cvSearch.best_params_)
            print("am now stratifying the predicton")
            cv = StratifiedKFold(n_splits=5)
            #prediction_cv = cross_val_predict(clf, X, Y, cv=cv)
            yPred = cross_val_predict(modelnew, featurestest, yTest, cv=cv)
            #change here orer of ypred to match ta review score results
            scores = ta.scoreResults(yTest, yPred, originalLabels,encodedLabels)

            dfscore = pd.DataFrame(scores)
            dfscore.to_csv("csv/scoresmodel.csv")
            bestParameters = cvSearch.best_params_
            allParameters = cvSearch.cv_results_
            bestModel = cvSearch.best_estimator_
            print("besst model is " ,cvSearch.best_estimator_   )

            if verbose:
                print('Time:', round(time() - startTime, 2), 'seconds\nAccuracy: ', scores[0])
            scores.append(modelName)

            results.append([scores, bestParameters, allParameters, bestModel])
            if outputFunction != None:
                outputFunction(results)

    return results

def getFeaturesLexicon(rawfeats, features, lexiconParameters):

    # arguments: data = all the tweets in the form of array, method = type of feature extracter
    # methods of feature extractions: "tfidf" and "doc2vec"
    # path = 'C:\\Users\\admin\\Documents\\Rfiles\\Training data\\'


    path = 'csv/'
    dfVader = pd.read_csv(path+ "vader_lexicon.csv", encoding = "ISO-8859-1")

    dfAfinn = pd.read_csv(path + "AFINN.csv", encoding="ISO-8859-1")

    dfsentiStrength = pd.read_csv(path + "sentimentLookupTable.csv", encoding="ISO-8859-1")
    # dfSS2 = pd.read_csv(path + "BoosterWordList.csv",  encoding="ISO-8859-1")
    # dfsentiStrength.append(dfSS2)

    #dfDepression = pd.read_csv(path + "depression.csv")

    dfDepression = pd.read_csv(path + "depression2.csv")

    ldepression = dfDepression.values.tolist()

    ldep1 = list(np.concatenate(ldepression))

    for item in ldep1:

        if item == 'nan':
            ldep1.remove(item)
    s = []

    [s.append(i.lower()) if not i.islower() else s.append(i) for i in ldep1]
    ldep2 = s
    # cleanedList = [x for x in ldep1 if x != 'NAN']
    # THIS IS THE LIST OF DEWPRESSIVE WORDS

    dfgamblin = pd.read_csv(path + "gambling.csv")

    lgamblin = dfgamblin.values.tolist()
    #ldep1 = list(np.concatenate(ldepression))
    lgamb1 = list(np.concatenate(lgamblin))

    for item in lgamb1:

        if item == 'nan':
            lgamb1.remove(item)
    s2 = []

    [s2.append(i.lower()) if not i.islower() else s2.append(i) for i in lgamb1]
    lgamb2 = s2
    dfsubstance= pd.read_csv(path + "substance_abuse.csv",encoding="ISO-8859-1")

    lsubstance = dfsubstance.values.tolist()
    # ldep1 = list(np.concatenate(ldepression))
    lsub1 = list(np.concatenate(lsubstance))

    for item in lsub1:

        if item == 'nan':
            lsub1.remove(item)
    s3 = []

    [s3.append(i.lower()) if not i.islower() else s3.append(i) for i in lsub1]
    lsub2 = s3
    # cleanedList = [x for x in ldep1 if x != 'NAN']
    # THIS IS THE LIST OF DEWPRESSIVE WORDS
    c=0
    for word in rawfeats.vocabulary_:
        c+=1

        position = rawfeats.vocabulary_[word]
        #if c % 1000 == 0:
            #print(c ,"position is",position)
        f1 = features[:, position]
        a=f1
        b=features[:, position]
        #print("are they equal",(a!=b).nnz==0  )
        #print(type(f1))

        if lexiconParameters['gamb' ]== True:
            if word in lgamb2:
                    #["pick", "gamble", "casino", "bet", "risk", "gambling", "jackpot", "crapshoot", "doubledown",
                     #   "bluechip", "penny ante", "poker", "poker-face", "gimmick"]:
                #print("found gamble word", word)

                features[:, position] = (features[:, position] * (lexiconParameters['mult']))

        if lexiconParameters['dep'] == True:
            if word in ldep2:
               #print("found depression word",word)
                features[:, position] = (features[:, position] * (lexiconParameters['mult']))

        if lexiconParameters['sub'] == True:
            if word in lsub2:
                #print("found substance word",word)
                features[:, position] = (features[:, position] * (lexiconParameters['mult']))

        if lexiconParameters['vader'] == True:
            x = float(1)

            if word in ldep2:
                pass
                #print("dep word",word)
                # print("FOUND DEPRESSIVE WORD", word)
                #x = -4
            # elif  word in ["pick","gamble","casino","bet","risk","gambling","jackpot","crapshoot","doubledown","bluechip","penny ante","poker","poker-face","gimmick"]:
            # x=-4

            else:
                if word in dfVader['word'].values:
                    #print("FOUND ONE!",word)
                    position = rawfeats.vocabulary_[word]
                    x = dfVader[dfVader['word'].values == word]['meansentiment'].values
                    # print("word is ",word,"x is ",x,type(x))
                    x = x.astype(np.float)
                    # print("word is ", word, "x is ", x, type(x), x[0])
                    x = x[0]
                    #print(x,type(x))
                    # set word as high neg if in depressive list
                    # CHANGE REVIEW THIS
                    #if x < 0:
                       # print(word,x)
                       # x = 1

            if x > 0:
                if lexiconParameters['negonly']:
                    pass
                else:
                    if lexiconParameters['highvalue'] == True:
                        if x > 3:
                            # print("high pos",x)
                            features[:, position] = (features[:, position] * abs(x) )
                        else:
                            pass
                    else:
                        #print("am adjusting pos",word,x)
                        #CHANGE THIS VALUE FROM 1
                        features[:, position] = (features[:, position] * (abs(x)))
            elif x == 0:
                pass
            else:

                if lexiconParameters['highvalue'] == True:

                    if abs(x) > 3:
                        # print("highneg",x)
                        features[:, position] *= abs(x)
                    else:
                        pass
                else:
                    #print("am adjusting neg",word,x)
                    #CHANGE TO +1 BELOW
                    features[:, position] = (features[:, position] * abs(x))

        if lexiconParameters['sentiStr'] == True:
            #print("am in sentistr")
            # change below 27/09
            if word in ldep2:
                if word in dfsentiStrength['word'].values:
                    x = dfsentiStrength[dfsentiStrength['word'].values == word]['meansentiment'].values

                    x = x.astype(np.float)
                    x=x[0]
                    #print ("x is ",x)
                    features[:, position] = (features[:, position]) * abs(x)
                else:
                #x = x[0]
                    features[:, position] = (features[:, position] )*lexiconParameters['mult']

            elif word in dfsentiStrength['word'].values:

                position = rawfeats.vocabulary_[word]
                x = dfsentiStrength[dfsentiStrength['word'].values == word]['meansentiment'].values

                x = x.astype(np.float)

                x = x[0]
                if x > 0:
                    if lexiconParameters['negonly']:
                        pass
                    else:
                        if lexiconParameters['highvalue'] == True:
                            if x > 3:
                                print("high pos", x,word)
                                features[:, position] = (features[:, position] * abs(x))
                            else:
                                pass
                        else:
                            features[:, position] = (features[:, position] * abs(x))
                elif x == 0:
                    pass
                else:

                    if lexiconParameters['highvalue'] == True:
                        # print("word is ", word, "x is ", x)
                        if abs(x) > 3:
                            # print("highneg", x)
                            features[:, position] = (features[:, position] * abs(x))
                        else:
                            pass
                    else:
                        # change neg to 1/
                        #rint ("x is ",x)
                        features[:, position] = (features[:, position] * abs(x))
                        #change removing the item below now as not sure why this is there.
                #features[:, position] = (features[:, position] * abs(x))
                #if (a!=features[:, position]).nnz==0 :
                    #print("THIS WORD DIDN'T CHANGE",word)
                #else:
                    #print("this word",word,"was ",a, "now",features[:, position])

        if lexiconParameters['Afinn'] == True:
            if word in dfAfinn['word'].values:

                position = rawfeats.vocabulary_[word]
                x = dfAfinn[dfAfinn['word'].values == word]['meansentiment'].values

                x = x.astype(np.float)

                x = x[0]
                if x > 0:
                    if lexiconParameters['negonly']:
                        pass
                    else:

                        features[:, position] = (features[:, position] * abs(x))
                elif x == 0:
                    pass
                else:

                    features[:, position] = features[:, position].astype(float)

                    features[:, position] = (features[:, position] * abs(x))
        soCalscore = 0
        if lexiconParameters['soCal'] == True:
            # if lexiconParameters['dep']== True:

            position = rawfeats.vocabulary_[word]

            found = False
            # CHANGE - NEED THIS WRAPPER TO PREVENT RANDOM HANGING OF SERVER CAUSING CRASH OUT.
            #print("am about to ping the server")
            while found == False:
                try:

                    res = nlp.annotate(word
                                       , properties={
                            'annotators': 'sentiment',
                            'outputFormat': 'json',
                            'timeout': 2000
                        })
                    found = True
                    #print("found",res)
                except:
                    t=1
                    #print("no response")
            f = 0
            x = 0
            for s in res["sentences"]:
                f += 1
                # print("f is ",f)
                soCalscore = s["sentimentValue"]
                # print(word,soCalscore)
                x = soCalscore
                # print("x socal  is ",x)
                x = float(x)
            gamb = False
            # print("x is ", x)

            if x > 2:
                if lexiconParameters['negonly']:
                    pass
                else:
                    features[:, position] = (features[:, position] * abs(x))
            elif x == 2:
                pass

            else:
                # print("x is ",x)
                # CHANGE THE NEGATIVE TO 1/X
                if x == 0:
                    pass
                else:
                    features[:, position] = (features[:, position] * abs(x))
            if gamb == True:
                pass

    return features


def getPipelinelEX(modelName, classifier, parameters):
    '''
    Creates a pipeline which may include (in order):
    - vectorizer => TF-IDF => DenseTransformer => classifier
    The DenseTransformer is used to convert the sparse matrix output for vectorization
    into a dense matrix so that Naive Bayes and Random Forest models can be used.
    '''
    pipelineParts = [

        ('classifier', classifier)
    ]

    if modelName == 'gaussianNB' or modelName == 'randomForest':
        pipelineParts.insert(1, ('to_dense', DenseTransformer()))

    return Pipeline(pipelineParts)