'''
@author: Christian O'Leary
'''
import os, logging, configparser
import tensorflow.keras.models as models
import tensorflow.keras.layers as layers
from tensorflow.keras import backend as K
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.models import Model, Sequential, load_model 
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.layers import Dense, Dropout, Flatten, LeakyReLU
from tensorflow.keras.models import Sequential, load_model 

from sklearn.metrics import accuracy_score, precision_recall_fscore_support, matthews_corrcoef
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import numpy as np
import pandas as pd
from random import randrange
from time import time

import data_analysis.constants as c
import data_analysis.text_analysis as ta
from data_analysis.keras_importer import KerasImporter


class ImageClassifier():
  def __init__(self):
    pass

  def fineTuneModel(self, config, parameters, imageDir, labelFiles, modelName, signals, layers, createEncoding=True, 
    seed=321):
    logging.debug('USING MODEL: %s', modelName)
    importer = KerasImporter()
    dimensions, kerasModel, preprocess_input, decode_predictions = importer.importImageModel(modelName)
    verbosity = config.getint('LOG', 'keras_verbosity')

    numEpochs = 500
    if 'numEpochs' in parameters.keys():
      numEpochs = parameters['numEpochs']
    batchSize = 32
    if 'batchSize' in parameters.keys(): 
      batchSize = parameters['batchSize']

    logging.debug('Reading images...')
    images, labels = self.getDataFromImages(dimensions, imageDir, labelFiles, preprocess_input, signals)

    encoder = LabelEncoder()
    encodedLabels = (encoder.fit_transform(labels)).astype(int)
    # # origLabels, encodedLabels, encoding = self.encodeData(labels)
    # unique, counts = np.unique(encodedLabels, return_counts=True)
    # print(np.asarray((unique, counts)).T)
    # # X: (1269, 1, 299, 299, 3)
    # # y: (1269,)

    logging.debug('Dividing data into train, validation, test sets...')
    xTrain, xTest, yTrain, yTest = train_test_split(images, encodedLabels, test_size=0.2, random_state=seed)
    xTrain, xVal, yTrain, yVal = train_test_split(xTrain, yTrain, test_size=0.25, random_state=seed)

    xTrain = np.reshape(xTrain, (xTrain.shape[0], dimensions[0], dimensions[1], dimensions[2]))
    xVal = np.reshape(xVal, (xVal.shape[0], dimensions[0], dimensions[1], dimensions[2]))
    xTest = np.reshape(xTest, (xTest.shape[0], dimensions[0], dimensions[1], dimensions[2]))

    logging.debug('Loading model...')
    baseModel = kerasModel(weights='imagenet', include_top=False, input_shape=dimensions)
    model = Sequential()
    model.add(baseModel)
    model.add(Flatten())
    # for layer in layers:
    #   model.add(layer)
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(np.unique(encodedLabels).shape[0], activation='softmax'))

    for layer in baseModel.layers[:-4]:
      layer.trainable = False

    logging.debug('Compiling model...')
    model.compile(loss="sparse_categorical_crossentropy", optimizer='adam', metrics=["accuracy"])
    
    logging.debug('Fitting model...')
    
    filename = 'tmp/checkpoints/' + str(time()) + '.hdf5'
    if os.path.exists(filename):
      os.remove(filename)

    callbacks = [
      ModelCheckpoint(filename, monitor='val_accuracy', verbose=verbosity, save_best_only=True, mode='max'),
      EarlyStopping(monitor='val_accuracy', mode='max', patience=15, verbose=verbosity)
    ]
    history = model.fit(xTrain, yTrain, validation_data=(xVal, yVal), callbacks=callbacks, batch_size=batchSize, 
      epochs=numEpochs, verbose=verbosity)

    for layer in baseModel.layers:
      layer.trainable = True

    logging.debug('Fine tuning model...')
    history = model.fit(xTrain, yTrain, validation_data=(xVal, yVal), callbacks=callbacks, batch_size=batchSize, 
      epochs=numEpochs, verbose=0)

    logging.debug('Loading best model...')
    bestModel = load_model(filename)

    logging.debug('Testing model...')
    predictions = bestModel.predict_classes(xTest)
    
    logging.debug('# of samples in test set', predictions.shape[0], '\n')
    metrics = self.scores(predictions, yTest)
    self.logScores(metrics)

    logging.debug('Clearing session...')
    K.clear_session()

    encoding = dict(zip(encoder.classes_, encoder.transform(encoder.classes_)))
    return yTest, predictions, list(encoding.keys()), [value for key, value in encoding.items()]


  def getDataFromImages(self, dimensions, imageDir, labelFiles, preprocessFunction, SIGNALS):
    images = []
    labels = []
    iteration = 1
    missed = 0

    for filename in os.listdir(imageDir):
      if filename.lower().split('.')[-1] in ['jpg', 'jpeg', 'gif', 'png', 'webp', 'bmp']:
        path = imageDir + filename
        image = load_img(path, target_size=(dimensions[0], dimensions[1]))

        image = img_to_array(image)
        image = np.expand_dims(image, axis=0)
        image = preprocessFunction(image)

        signal = self.parsePath(filename, labelFiles, SIGNALS)
        if signal in ['unknown', None]:
          missed +=1
        else:
          images.append(image)
          labels.append(signal)
      else:
        logging.debug('Found file which is not an image: %s', filename)

    df = pd.DataFrame({'image': images, 'signal': labels})
    logging.warning('Failed to find signals for %d images. %d remaining', missed, len(images))
    return np.array(images), np.array(labels)


  def parsePath(self, path, labelFiles, SIGNALS):
    signal = self.getSignalFromFiles(labelFiles, path, SIGNALS) # search for path in files
    if signal == None:
      signal = self.getSignalFromPath(path, SIGNALS) # search for signal in path
        
    if signal == None:
      logging.debug('No signal found for path: %s', path)
      # raise ValueError('No signal found in path:', path)
      signal = 'unknown'
    return signal


  def getSignalFromFiles(self, labelFiles, path, SIGNALS):
    path = path.lower()
    signal = None
    seperators = ['/', '\\', '\\\\']
    for seperator in seperators:
      fileNameParts = path.lower().split(seperator)
      if len(fileNameParts) > 1: # successful split
        break
    
    matches = []
    for labelFile in labelFiles:
      df = labelFile[0]
      pathCol = labelFile[1]
      signalCol = labelFile[2]
      df[pathCol] = df[pathCol].apply(lambda s : s.lower())    

      for i in range(len(fileNameParts)):
        for seperator in ['/', '\\\\', '\\\\\\\\']:
          fileName = seperator.join(fileNameParts[i:])
          fileName = fileName.replace('-~seperator~-', seperator)

          rows = df[df[pathCol].str.contains(fileName)]
          if rows.shape[0] > 0:
            signal = str(rows[signalCol].mode().iloc[0]).lower()
            matches.append(signal)
            break
        if signal != None: break # filename parts
      if len(set(matches)) > 1:
        signal = max(set(matches), key=matches.count) # mode
        # logging.debug(fileNameParts)
        # raise ValueError('Found conflicting signals from different files')
    return signal


  def getSignalFromPath(self, path, SIGNALS):
    path = path.lower()
    alternatives = {
      'academic_misconduct': ['academic misconduct', 'acaemic misconduct', 'academic-misconduct'],
      'substance_abuse': ['substance abuse', 'substance-abuse'],
      'no_signal': ['no signal', 'no-signal'],
      'offensive': ['aggressive', 'oppressive', 'sexually inappropriate', 'sexualy inappropriate'],
    }
    for signal in alternatives.keys():
      for alternativeSignal in alternatives[signal]:
        path = path.replace(alternativeSignal, signal)
    
    signal = None
    matches = []
    for label in SIGNALS:
      if str(label).lower() in path:
        matches.append(label)
        signal = label
    
    if len(set(matches)) > 1:
      signal = max(set(matches), key=matches.count) # mode
      # logging.error('Found conflicting signals from different files')
      # raise ValueError('Found conflicting signals from different files')
    return signal
  

  def scores(self, predicted, actual):
    scores = [accuracy_score(actual, predicted)]
    scores += precision_recall_fscore_support(actual, predicted, average=None)
    scores.append(matthews_corrcoef(actual, predicted))
    return scores


  def encodeData(self, labels):
    encodings = {}
    encodedLabels = np.copy(labels)
    for label in np.unique(labels):
      encoding = randrange(100, 100000)
      encodedLabels[encodedLabels == label] = encoding
      encodings[label] = encoding
    logging.debug('Encodings:')
    if logging.getLogger().getEffectiveLevel() < 20: # 1-20 = debug in logging module
      for key, value in encodings.items():
        logging.debug('%s:%s', key, str(value))
    return labels, encodedLabels.astype(int), encodings


  def logScores(self, scores):
    logging.debug('Accuracy:', scores[0])
    logging.debug('Per class Precision:', scores[1])
    logging.debug('Per class Recall:', scores[2])
    logging.debug('Per class F1 score:', scores[3])
    logging.debug('Per class Support:', scores[4])
    logging.debug('Matthews Corr. Coef.:', scores[5])

