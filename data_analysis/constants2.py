import warnings, sys, os, string
warnings.filterwarnings('ignore')
if not sys.warnoptions:
  warnings.simplefilter("ignore")
  os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses

import re

from sklearn.dummy import DummyClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier, RandomForestClassifier
from sklearn.linear_model import LogisticRegression, PassiveAggressiveClassifier, Perceptron
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC, SVC

# Pre-processing parameters (which are case-sensitive)
parameters = {
  'expandContractions': [ True, False ],
  'lemmatization': [ True, False ],
  'lowercase': [ True, False ],
  'maxVocabFeatures': [ None, int ],
  'ngram': [ int ], # change to tuple?
  'removeNonAscii': [ True, False ],
  'removeHashtags': [ True, False ],
  'removeHttp': [ True, False ],
  'removeMention': [ True, False ],
  'removePunctuation': [ True, False ],
  'removeStopwords': [ True, False ], # None, default, nltk
  'removeWww': [ True, False ],
  'soup': [ True, False ],
  'spellCheck': [ 'textblob', 'autocorrect', None ],
  'stemming': [ 'lancaster', 'porter', None],
  'stripAccents': [ 'ascii', None ], # 'unicode',
  'tokenizer': [ 'tweet', 'wordPunct' ], # None (use vectorizer's default tokenizer instead)
  'useLexicons': [ True, False ],
  'vectorizer': [ 'count' ]

  # 'vectorizerParams': dict, # TODO
  # 'termWeighting': {
  #   'norm': [ 'l1', 'l2', None ],
  #   'use_idf': [ True, False ],
  #   'smooth_idf ': [ True, False ],
  #   'sublinear_tf ': [ True, False ],
  # },
  # 'translateAbbrevations': [ True, False ], # TODO
}

lexiconParameters = [

  dict(Lex=True, Afinn=False, vader=False, sentiStr=False, negonly=False, highvalue=False, soCal=False, mult=1, gamb=False, dep=False, sub=False),
  dict(Lex=True, Afinn=False,vader=False, sentiStr=False, negonly=False, highvalue=False, soCal=False, mult=1.5, gamb=False, dep=True, sub=False),
  dict(Lex=True,Afinn=False,vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=10, gamb=False, dep=True, sub=False),
 dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=False, dep=True, sub=False),
  dict(Lex=True, Afinn=False,  vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=False, dep=True, sub=False),
  dict(Lex=True,Afinn=False,  vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=False, dep=False, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=False, dep=False, sub=True),
  dict(Lex=True, Afinn=False, vader=False, sentiStr=False, negonly=False, highvalue=False, soCal=False, mult=2.5, gamb=False, dep=True, sub=False),
  dict(Lex=True,Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=1.5, gamb=False, dep=False, sub=True),
dict(Lex=True,Afinn=False, vader=False, sentiStr=True,negonly=False, highvalue=False, soCal=False,mult=10, gamb=False, dep=False, sub=True),
dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=False, dep=True, sub=True),
 dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=False, dep=True, sub=True),
 dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=3, gamb=False, dep=True, sub=True),
  dict(Lex=True, Afinn=False,vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=1.5, gamb=False, dep=True, sub=True),
dict(Lex=True,Afinn=False,vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False,mult=.5, gamb=True, dep=False, sub=False),
dict(Lex=True,Afinn=False,vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=5, gamb=True, dep=False, sub=False),
dict(Lex=True,Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.7, gamb=True, dep=False, sub=False),
dict(Lex=True,Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=10, gamb=True, dep=False, sub=False),
 dict(Lex=True, Afinn=False, vader=False, sentiStr=True,negonly=False, highvalue=False, soCal=False, mult=.7, gamb=True, dep=True, sub=True),
  dict(Lex=True,Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=1.5, gamb=True, dep=True, sub=True),
  dict(Lex=True,Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=.8, gamb=True, dep=True, sub=True),
dict(Lex=True, Afinn=False, vader=False, sentiStr=True,negonly=False, highvalue=False, soCal=False, mult=.5, gamb=True, dep=True, sub=True),
dict(Lex=True, Afinn=False, vader=False, sentiStr=True, negonly=False, highvalue=False, soCal=False, mult=1.5, gamb=True, dep=True, sub=True),

]



models = {
  'default': lambda : LogisticRegression(C=10, class_weight='balanced', multi_class='ovr', penalty='l1', solver='liblinear'),

  # 'adaBoost': lambda : AdaBoostClassifier(),
  'bagging': lambda : BaggingClassifier(),
  'bernoulliNB': lambda : BernoulliNB(),
  'decisionTree': lambda : DecisionTreeClassifier(),
  'dummy': lambda : DummyClassifier(),
  'gaussianNB': lambda : GaussianNB(),
  'logisticRegression': lambda : LogisticRegression(),
  'linearSvc': lambda : LinearSVC(),
  'knn': lambda : KNeighborsClassifier(),
  'mlp': lambda : MLPClassifier(),
  'passiveAggressive': lambda : PassiveAggressiveClassifier(),
  'perceptron': lambda : Perceptron(),
  'randomForest': lambda : RandomForestClassifier(),
  'svc': lambda : SVC(),
}

bestModels = {
  'default': lambda : LogisticRegression(C=10, class_weight='balanced', multi_class='ovr', penalty='l2', solver='newton-cg'),

  'bernoulliNB': lambda : BernoulliNB(alpha=0.0),
  'decisionTree': lambda : DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None, max_features=None, max_leaf_nodes=None, min_samples_leaf=1, min_samples_split=2, splitter='random'),
  'gaussianNB': lambda : GaussianNB(var_smoothing=1e-08),
  'linearSvc': lambda : LinearSVC(C=1, class_weight=None, loss='squared_hinge', multi_class='ovr', penalty='l2'),
  'logisticRegression': lambda : LogisticRegression(C=10, class_weight='balanced', multi_class='ovr', penalty='l2', solver='newton-cg'),
  'passiveAggressive': lambda : PassiveAggressiveClassifier(C=1, class_weight=None, early_stopping=False),
  'perceptron': lambda : Perceptron(alpha=1e-05, class_weight='balanced', early_stopping=False, penalty=None, tol=0.0001),
  'randomForest': lambda : RandomForestClassifier(class_weight='balanced', criterion='gini', max_depth=10, n_estimators=128),
  'svc': lambda : SVC(C=10, class_weight='balanced', kernel='linear', shrinking=True),
}

gridSearch = {
  'default': {},

  # 'adaBoost': {
  #   'classifier__base_estimator': [bestModels[modelName]() for modelName in bestModels.keys()], # does not work
  #   'classifier__n_estimators': [8, 32, 128, 512],
  #   'classifier__learning_rate ': [0.01, 0.1, 1.0],
  #   'classifier__algorithm ': ['SAMME', 'SAMME.R'],
  # },
  # 'bagging': {
  #   'classifier__base_estimator': [bestModels[modelName]() for modelName in bestModels.keys()],
  #   'classifier__n_estimators': [8, 32, 128, 512],
  #   'classifier__max_samples': [0.2, 0.6, 1.0],
  #   # 'classifier__max_features': [0.2, 0.6, 1.0], # handled before modelling
  #   'classifier__bootstrap': [True, False],
  #   'classifier__oob_score': [True, False],
  # },
  'bernoulliNB': {
    'classifier__alpha': [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
  },
  'decisionTree': {
    'classifier__criterion': ['gini', 'entropy'],
    'classifier__splitter': ['best', 'random'],
    'classifier__max_depth': [10, 50, None],
    'classifier__min_samples_split': [2, 16, 128],
    'classifier__min_samples_leaf': [1, 10, 100],
    'classifier__max_features': [None], # handled before modelling
    'classifier__max_leaf_nodes': [2, 8, 32, None],
    'classifier__class_weight': [None, 'balanced'],
  },
  'dummy': {},
  'gaussianNB': {
    'classifier__var_smoothing': [1e-8, 1e-9, 1e-10]
  },
  'logisticRegression': { 
    'classifier__C': [0.01, 0.1, 1, 10], 
    'classifier__class_weight': [None, 'balanced'], 
    # 'classifier__max_iter': [10, 100, 1000], 
    'classifier__multi_class': ['multinomial', 'ovr'],
    'classifier__penalty': ['l1', 'l2', 'none'], 
    'classifier__solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'],
  },
  'knn': {
    'classifier__n_neighbors': [2, 4, 8, 16],
    'classifier__weights': ['uniform', 'distance'],
    'classifier__algorithm': ['kd_tree', 'brute'], #'ball_tree'
    # 'classifier__leaf_size': [10, 50, 90],
    'classifier__metric': ['euclidean', 'manhattan', 'chebyshev'],
  },
  'linearSvc': {
    'classifier__C': [0.01, 0.1, 1, 10],
    'classifier__loss': ['hinge', 'squared_hinge'],
    'classifier__penalty': ['l1', 'l2'],
    'classifier__multi_class': ['ovr', 'crammer_singer'],
    'classifier__class_weight': [None, 'balanced'],
  },
  'passiveAggressive': {
    'classifier__C': [0.01, 0.1, 1, 10],
    'classifier__class_weight': [None, 'balanced'], 
    'classifier__early_stopping' : [False],
  },
  'perceptron': {
    'classifier__penalty': ['l1', 'l2', None],
    'classifier__alpha': [0.00001, 0.0001, 0.001],
    'classifier__tol': [1e-2, 1e-3, 1e-4],
    'classifier__early_stopping': [True, False],
    'classifier__class_weight': [None, 'balanced'], 
  },
  'randomForest': { # using parameters from best Decision Tree
    'classifier__criterion': ['gini'],
    #'classifier__splitter': ['random'],
    'classifier__max_depth': [None],
    'classifier__max_features': [None], # handled before modelling
    'classifier__n_estimators': [8, 16, 32, 64, 128],#, 256, 512, 1024],
    'classifier__min_samples_split': [2],
    'classifier__min_samples_leaf': [1],
    'classifier__max_leaf_nodes': [None],
    'classifier__class_weight': [None, 'balanced', 'balanced_subsample'],
  },
  'svc': {
    'classifier__C': [0.01, 0.1, 1, 10],
    'classifier__kernel': ['linear', 'rbf', 'sigmoid'],
    'classifier__shrinking': [True, False],
    'classifier__class_weight': ['balanced', None],
  },
}

mentionPattern = r'@[A-Za-z0-9]+'
hashtagPattern = r'#[A-Za-z0-9]+'
httpspattern = r'https?://[^ ]+'
wwwPattern = r'www.[^ ]+'
punctuationPattern = r'[.,;:\-/\'?!"\(\){}\[\]\u2026]' # \u2026 matches elipsis
# combinedPattern = r'|'.join((ampersandPattern, httpspattern))

# taken from wikipedia: https://stackoverflow.com/questions/19790188/expanding-english-language-contractions-in-python
contractions = {
  "ain't": "am not / are not / is not / has not / have not", "aren't": "are not / am not", "can't": "cannot",
  "can't've": "cannot have", "'cause": "because", "could've": "could have", "couldn't": "could not", 
  "couldn't've": "could not have", "didn't": "did not", "doesn't": "does not", "don't": "do not", "hadn't": "had not",
  "hadn't've": "had not have", "hasn't": "has not", "haven't": "have not", "he'd": "he had / he would",
  "he'd've": "he would have", "he'll": "he shall / he will", "he'll've": "he shall have / he will have",
  "he's": "he has / he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will", 
  "how's": "how has / how is / how does", "I'd": "I had / I would", "I'd've": "I would have", "I'll": "I shall / I will",
  "I'll've": "I shall have / I will have", "I'm": "I am", "I've": "I have", "isn't": "is not", "it'd": "it had / it would",
  "it'd've": "it would have", "it'll": "it shall / it will", "it'll've": "it shall have / it will have", 
  "it's": "it has / it is", "let's": "let us", "ma'am": "madam", "mayn't": "may not", "might've": "might have",
  "mightn't": "might not", "mightn't've": "might not have", "must've": "must have", "mustn't": "must not", 
  "mustn't've": "must not have", "needn't": "need not", "needn't've": "need not have", "o'clock": "of the clock", 
  "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not", "sha'n't": "shall not", 
  "shan't've": "shall not have", "she'd": "she had / she would", "she'd've": "she would have", "she'll": "she shall / she will",
  "she'll've": "she shall have / she will have", "she's": "she has / she is", "should've": "should have", 
  "shouldn't": "should not", "shouldn't've": "should not have", "so've": "so have", "so's": "so as / so is", 
  "that'd": "that would / that had", "that'd've": "that would have", "that's": "that has / that is", 
  "there'd": "there had / there would", "there'd've": "there would have", "there's": "there has / there is", 
  "they'd": "they had / they would", "they'd've": "they would have", "they'll": "they shall / they will",
  "they'll've": "they shall have / they will have", "they're": "they are", "they've": "they have", "to've": "to have",
  "wasn't": "was not", "we'd": "we had / we would", "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have",
  "we're": "we are", "we've": "we have", "weren't": "were not", "what'll": "what shall / what will", 
  "what'll've": "what shall have / what will have", "what're": "what are", "what's": "what has / what is", "what've": "what have",
  "when's": "when has / when is", "when've": "when have", "where'd": "where did", "where's": "where has / where is", 
  "where've": "where have", "who'll": "who shall / who will", "who'll've": "who shall have / who will have", 
  "who's": "who has / who is", "who've": "who have", "why's": "why has / why is", "why've": "why have", "will've": "will have",
  "won't": "will not", "won't've": "will not have", "would've": "would have", "wouldn't": "would not", 
  "wouldn't've": "would not have", "y'all": "you all", "y'all'd": "you all would", "y'all'd've": "you all would have", 
  "y'all're": "you all are", "y'all've": "you all have", "you'd": "you had / you would", "you'd've": "you would have",
  "you'll": "you shall / you will", "you'll've": "you shall have / you will have", "you're": "you are", "you've": "you have"
}
capatalisedContractions = { string.capwords(key) : string.capwords(contractions[key]) for key in contractions.keys() }
temp = capatalisedContractions.copy()
temp.update(contractions)
contractions = temp
contractionPattern = re.compile(r'\b(' + '|'.join(contractions.keys()) + r')\b')

# # convert all urls to sting "URL"
# tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', 'URL', tweet)
# # convert all @username to "AT_USER"
# tweet = re.sub('@[^\s]+', 'AT_USER', tweet)
# # correct all multiple white spaces to a single white space
# tweet = re.sub('[\s]+', ' ', tweet)
# # convert "#topic" to just "topic"
# tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
