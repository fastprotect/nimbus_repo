'''
@author: Christian O'Leary
'''

from time import time
from itertools import product
from random import choice
from joblib import dump, load
import pandas as pd
import numpy as np
import copy, os, csv, datetime, itertools, traceback, gc, logging
import _pickle as cPickle

from sklearn.calibration import CalibratedClassifierCV
from sklearn.model_selection import cross_validate, train_test_split
from sklearn.pipeline import Pipeline
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer

import data_analysis.constants as c
import data_analysis.image_analysis as ia
# import data_analysis.modelPipeLex as mplex
import data_analysis.text_analysis as ta
from data_analysis.text_analysis import TokenizerReducer
from data_analysis.keras_importer import KerasImporter
from data_analysis.image_deep_models import ImageClassifier
from data_analysis.text_deep_models import TextClassifier


class Evaluator():
  defaultParameters = c.configurations['default']
  minimalParameters = c.configurations['minimal']

  def __init__(self):
    self.createDirs('tmp/checkpoints/')


  def cleanAndModel(self, config, parameters, searchParams, trainData='csv/combined.csv', testData=None, search='grid', 
    nproc=1, seed=None, labelsToDrop=['14', 'unknown']):
    ta.validateParameters(parameters, c.parameters)
    self.validateModelNames(searchParams)
    
    # READ, CLEAN & SPLIT IN DATA
    startTime = time()
    xTrain, xTest, yTrain, yTest, encoding, rowCounts = self.getTrainTestData(parameters, trainData, testData=testData, 
      seed=seed, nproc=nproc, labelsToDrop=labelsToDrop)
    logging.debug('Read data (%.2f) seconds', time() - startTime)
    
    # TRAIN MODELS
    originalLabels = sorted(list(encoding.keys()))
    encodedLabels = [encoding[key] for key in originalLabels]
    startTime = time()
    if parameters['useLexicons'] == True:
        results = mplex.modelLex(xTrain, xTest, yTrain, yTest, originalLabels, encodedLabels, searchParams, parameters, 
          nproc=nproc, verbose=True)
    else:
        results = ta.model(config, xTrain, xTest, yTrain, yTest, originalLabels, encodedLabels, searchParams, 
          parameters, search=search, nproc=nproc, seed=seed)

    return results, originalLabels, rowCounts


  def validateModelNames(self, models):
    for modelName in models:
      if modelName not in c.models:
        logging.error('Invalid model name: %s', modelName)
        logging.error('Allowed model names are: \n%s', ' '.join(list(c.models.keys())))
        raise ValueError('Unexpected model name', modelName)


  def validateEnsembleModels(self, models):
    if not isinstance(models, dict) or len(models.keys()) != 2 or 'features' not in models.keys() or 'textOnly' \
      not in models.keys():
      raise ValueError('"models" should be a dict with keys "features" and "textOnly"')
    self.validateModelNames([ list(models['features'].keys())[0], list(models['textOnly'].keys())[0] ])


  def ensembleModel(self, config, parameters, models, inputPath='csv/combined.csv', inputTestPath=None, search='grid',
    labelsToDrop=['14', 'unknown'], nproc=1, seed=None):
    ta.validateParameters(parameters, c.parameters)
    self.validateEnsembleModels(models)

    df = ia.readData(inputPath, labelsToDrop)
    df, encoding = ta.encodeData(df, yCol='signal')
    df = ta.cleanText(df, parameters, textCol='text', nproc=nproc)
    
    originalLabels = sorted(list(encoding.keys()))
    encodedLabels = [encoding[key] for key in originalLabels]

    df, textDf = ia.divideDataByType(df)
    dataframes = { 'features': df, 'textOnly': textDf }
    results, ensemble = ia.trainImageEnsemble(config, df, dataframes, models, parameters, originalLabels, encodedLabels, 
      search=search, nproc=nproc, seed=None)
    return results, ensemble, originalLabels, encodedLabels


  def getTrainTestData(self, parameters, trainData, testData=None, seed=None, nproc=1, labelsToDrop=['14', 'unknown']):
    if isinstance(trainData, str):
      df, rowCounts = ta.readData(trainData, parameters, labelsToDrop=labelsToDrop, seed=seed)
      df.columns = ['X', 'y']
      df, encoding = ta.encodeData(df)
      startTime = time()
      df = ta.cleanText(df, parameters, nproc=nproc)
      rowCounts['After cleaning'] = df.shape[0]
      cleanTime = time() - startTime

      if testData != None:
        testDf, testRowCounts = ta.readData(testData, parameters, labelsToDrop=labelsToDrop, seed=seed)
        if set(encoding.keys()) != set(testDf.y.unique()):
          raise ValueError('Train and test labels do not match. Train Labels:', df.y.unique(), 'Test labels:', 
            testDf.y.unique())

        for label in encoding.keys():
          logging.debug('%s:%d', label, (testDf.y == label).sum())
          indices = testDf.y == label
          testDf.loc[indices, 'y'] = encoding[label]
        testDf = testDf.astype(str)

        startTime = time()
        testDf = ta.cleanText(testDf, parameters, nproc=nproc)
        cleanTime += time() - startTime
        xTrain = df.X
        yTrain = df.y
        xTest = testDf.X
        yTest = testDf.y
      else:
        xTrain, xTest, yTrain, yTest = ta.splitData(df, seed=seed, stratify=parameters['stratifySplit'])
        # train = pd.concat([xTrain, yTrain], axis=1)
        # train.to_csv('train.csv', index=False)
        # test = pd.concat([xTest, yTest], axis=1)
        # test.to_csv('test.csv', index=False)
        rowCounts['Training set'] = xTrain.shape[0]
        rowCounts['Testing set'] = xTest.shape[0]

    else: # data as a 2D list
      if not isinstance(trainData, pd.DataFrame):
        df = pd.DataFrame.from_records(trainData)
      else:
        df = trainData
      rowCounts = { 'Initial': df.shape[0] }
      df.columns = ['X', 'y']
      if df['y'].count() < 2:
        raise ValueError('Only one class present in training data:', df['y'].unique()[0])

      df, encoding = ta.encodeData(df)
      startTime = time()
      df = ta.cleanText(df, parameters, nproc=nproc)
      rowCounts['After cleaning'] = df.shape[0]
      cleanTime = time() - startTime
      xTrain, xTest, yTrain, yTest = ta.splitData(df, seed=seed, stratify=parameters['stratifySplit'])
      rowCounts['Training set'] = xTrain.shape[0]
      rowCounts['Testing set'] = xTest.shape[0]

    yTrain = yTrain.astype(int)
    yTest = yTest.astype(int)
    logging.debug('Read & cleaned data (%.2f) seconds', cleanTime)
    return xTrain, xTest, yTrain, yTest, encoding, rowCounts


  def imageFineTuning(self, config, excludedModels, outputPath,  params, iterations=30):
    clf = ImageClassifier()
    importer = KerasImporter()
    
    modelParams = { 'numEpochs': 500, 'batchSize': 32 }
    modelNames = importer.imageModelNames
    for model in excludedModels:
      modelNames.remove(model)
      
    orderedKeys = list(modelParams.keys())
    results = []

    imageDir = params['imageDir']
    labelFiles = params['labelFiles']
    signals = params['signals']
    dataset = imageDir.split('/')[1]

    for i in range(iterations):
      for modelName in modelNames:
        for name, layers in c.imageArchitectures.items():
          logging.info('Using architecture %s', name)
          startTime = time()
          yTest, yPred, originalLabels, encodedLabels = clf.fineTuneModel(config, modelParams, imageDir, labelFiles, 
            modelName, signals, layers=layers())
          originalLabels = np.unique(originalLabels)
          encodedLabels = np.unique(encodedLabels)

          scores = ta.scoreResults(yTest, yPred, originalLabels, np.unique(encodedLabels))
          scores.append(modelName)
          results = [[scores, {}, {}, None]]
          self.outputToFile(outputPath + dataset + '-raw-image.csv', '', None, orderedKeys, modelParams, 
            originalLabels, results, [time()-startTime])


  def runWithIndependentParameters(self, config, scoresPath, metaDataPath, trainPath, testPath=None, excludedKeys=[], 
    sklearn=True, search='grid', iterations=30, nproc=1, seed=321):
    c.parameters['maxVocabFeatures'] = [None, 1000, 10000, 100000, 1000000]
    c.parameters['maxClassSize'] = [None, 2500, 5000, 7500, 10000, 15000, 20000]
    orderedKeys = list(self.minimalParameters.keys())
    orderedKeys.append(orderedKeys.pop(orderedKeys.index('spellCheck'))) # place spellcheck last

    # Assume preprocessing technique performances are independant and try each in isolation
    for i in range(iterations):
      seed += 1
      for key in orderedKeys:
        if key not in excludedKeys:
          for option in c.parameters[key]:
            newParameters = copy.deepcopy(self.minimalParameters)
            newParameters[key] = option
            if newParameters['tfidf-norm'] != None or newParameters['tfidf-use_idf'] != False or \
              newParameters['tfidf-smooth_idf'] != False or newParameters['tfidf-sublinear_tf'] != False: 
              newParameters['vectorizer'] = 'tfidf'

            logging.info('%s: %s', key, str(option))
            if not os.path.exists(scoresPath) or self.isNewCombination(newParameters, scoresPath):
              if sklearn:
                self.runSklearnModels(config, newParameters, trainPath, testPath, scoresPath, metaDataPath, orderedKeys, 
                  search, nproc, seed)
              else:
                self.runKerasModels(config, newParameters, trainPath, scoresPath, orderedKeys, nproc=nproc, seed=seed)


  def runSklearnModels(self, config, parameters, trainPath, testPath, scoresPath, metaDataPath, orderedKeys, search, 
    nproc, seed, randomModel=False):
    '''Use best sklearn models for text classification'''
    if randomModel:
      modelNames = [choice(list(c.bestModels.keys()))]
    else:
      modelNames = list(c.bestModels.keys())
      if 'default' in modelNames:
        modelNames.remove('default')

    for modelName in modelNames:
      logging.debug('%s', modelName)
      models = { modelName: {} }
      startTime = time()
      results, originalLabels, rowCounts = self.cleanAndModel(config, parameters, models, trainPath, testPath, 
        search=search, nproc=nproc, seed=seed)
      if len(results) > 0:
        self.outputToFile(scoresPath, metaDataPath, rowCounts, orderedKeys, parameters, originalLabels, results, 
          [time()-startTime])


  def runKerasModels(self, config, parameters, trainPath, scoresPath, orderedKeys, architectures=c.textArchitectures, 
    embeddingPaths=c.embeddingPaths, iterations=1, nproc=1, seed=None, serializePath=None, 
    labelsToDrop=['14', 'unknown'], plotDir=None):
    '''Use deep learning models for text classification'''
    self.createDirs('tmp/checkpoints/')
    self.createDirs(scoresPath)
    self.createDirs(plotDir)

    clf = TextClassifier()
    if orderedKeys == None:
      orderedKeys = list(parameters.keys())
    dataName = trainPath.split('.')[-2].split('/')[-1]
    
    xTrain, xTest, yTrain, yTest, encoding, rowCounts = self.getTrainTestData(parameters, trainPath, None, seed=seed, 
      nproc=nproc, labelsToDrop=labelsToDrop)
    originalLabels = sorted(list(encoding.keys()))
    encodedLabels = [encoding[key] for key in originalLabels]

    outputParams = (scoresPath, '', rowCounts, orderedKeys, parameters, originalLabels)

    for i in range(iterations):
      startTime = time()
      for embedding, path in c.embeddingPaths.items():
        modelParams = {'embedding': (embedding, path)}
        results, times, tokenizer, metadata = clf.trainModel(config, xTrain, xTest, yTrain, yTest, encoding, parameters,
          modelParams, tokenizer=None, architectures=architectures, serializePath=serializePath, plotDir=plotDir, 
          outputFunction=self.outputToFile, outputParams=outputParams)

      logging.info('Models took %.2f seconds to train', time()-startTime)
    return results, times, tokenizer, metadata


  def createDirs(self, path):
    '''Ensure a directory path exists. Ignores any filename specified at the end of the path'''
    if path != None and not os.path.exists(path):
      currentPath = ''
      for part in path.split('/'):
        currentPath += part + '/'
        if not os.path.exists(currentPath) and '.' not in currentPath:
          os.makedirs(currentPath)


  def isNewCombination(self, parameters, scoresPath):
    '''Checks to see if a parameter combination has already been tried'''
    if not os.path.exists(scoresPath):
      return True
    
    results = pd.read_csv(scoresPath).astype(str)
    for key in parameters.keys():
      results = results.loc[results[key] == str(parameters[key])]
    return results.shape[0] == 0


  def createScoresFile(self, filePath, orderedKeys, originalLabels):
    if not os.path.isfile(filePath): # Create file and subdirs if not existing
      self.createDirs(filePath)

      with open(filePath, 'a+', newline='') as outputFile:# Write header if this is a new file
        writer = csv.writer(outputFile, delimiter=',')
        values = orderedKeys + [
          'accuracy', 'microPrecision', 'microRecall', 'microF1',
          'macroPrecision', 'macroRecall', 'macroF1',
          'weightedPrecision', 'weightedRecall', 'weightedF1',
        ]
        values = values + [str(label) + 'Precision' for label in originalLabels]
        values = values + [str(label) + 'Recall' for label in originalLabels]
        values = values + [str(label) + 'F1' for label in originalLabels]
        values = values + [str(label) + 'Support' for label in originalLabels]
        values.append('MatthewsCorrCoef')
        values.append('model')
        values.append('time')
        writer.writerow(values)


  def outputToFile(self, scoresPath, metaDataPath, rowCounts, orderedKeys, parameters, originalLabels, results, 
    times):
    '''Record modelling results in CSV file (Create if not present). 'results' is a list of lists of length 2'''
    if isinstance(scoresPath, str) and len(scoresPath) > 0:
      self.createScoresFile(scoresPath, orderedKeys, originalLabels)

      with open(scoresPath, 'a+', newline='') as scoresFile:
        writer = csv.writer(scoresFile, delimiter=',')
        values =[]
        for parameterName in orderedKeys:
          if parameters[parameterName] is None:
            values.append('None')
          else:
            values.append(parameters[parameterName])
        
        for i, iteration in enumerate(results):
          values += iteration[0]
          values.append(times[i])

        writer.writerow(values)

    # scoresDir = scoresPath[:scoresPath.index('.')]
    # if isinstance(rowCounts, dict) and len(rowCounts) > 0:
    #   counts = ''
    #   for key, value in rowCounts.items():
    #     counts += str(value) + ':' + str(key) + '\n'
    #   with open(counts, 'w') as fid:
    #     fid.write(counts)

    if isinstance(metaDataPath, str) and len(metaDataPath) > 0:
      self.createDirs(metaDataPath)
      with open(metaDataPath, 'a+', newline='') as metaDataFile:
        metaDataFile.write('\n=======================================\n')
        for iteration in results:
          metaDataFile.write('Model: ' + iteration[0][-1])
          metaDataFile.write('Accuracy: ' + str(iteration[0][0]) 
            + '\nMICRO (P/R/F1):' + str(iteration[0][1:4]) 
            + '\nMACRO (P/R/F1):' + str(iteration[0][4:7]) 
            + '\nWEIGHTED (P/R/F1):' + str(iteration[0][7:10]))
          metaDataFile.write('\nBest parameters:\n' + str(iteration[1]))
          allParameters = pd.DataFrame(iteration[2]).to_string()
          metaDataFile.write('\n All parameters:\n' + str(allParameters))


  def randomizedPreprocessing(self, config, resultPath, fixedParams, models, trainPath, testPath=None, iterations=10000, 
    sklearn=True, search='grid', nproc=1, seed=None):
    '''Randomized search of parameters'''
    c.parameters['maxVocabFeatures'] = [None, 1000, 10000, 100000, 1000000]
    c.parameters['maxClassSize'] = [None, 2500, 5000, 7500, 10000, 15000, 20000]
    orderedKeys = list(self.defaultParameters.keys())
    totalTime = 0
    logging.info('Running %d iterations', iterations)

    iteration = 0
    for i in range(iterations):
      parameters = self.getRandomParams(fixedParams)
      iteration += 1
      logging.info('Iteration %d of %d', iteration, iterations)

      if not os.path.exists(resultPath) or self.isNewCombination(parameters, resultPath):
        startTime = time()
        if sklearn:
          self.runSklearnModels(config, parameters, trainPath, testPath, resultPath, '', orderedKeys, search, nproc, 
            seed, randomModel=True)
        else:
          self.runKerasModels(config, parameters, trainPath, resultPath, orderedKeys, nproc, seed)
        
        runTime = time() - startTime
        totalTime += runTime
        averageTime = totalTime / iteration
        estimate = str(datetime.timedelta(seconds=((iterations - iteration) * averageTime)))
        logging.info('Took %.2f seconds. Total time so far: %.2f. Remaining estimate: %s', runTime, totalTime, estimate)


  def getRandomParams(self, fixedParams={}):
    parameters = fixedParams.copy()
    for param, options in c.parameters.items():
      if param not in parameters.keys():
        parameters[param] = choice(options)
    return parameters


  def ensembleModelSelection(self, config, modelNames, scoresPath, inputPath, parameters=defaultParameters, 
    search='grid', nproc=1, numIterations=30, seed=None):
    orderedKeys = list(parameters.keys())

    for i in range(numIterations):
      logging.info('Iteration %d', i)
      for pair in itertools.product(modelNames, repeat=2):
        logging.debug('%s-%s', pair[0], pair[1])
        models = {
          'features': { pair[0]: c.gridSearch[pair[0]] },
          'textOnly': { pair[1]: c.gridSearch[pair[1]] },
        }
        metaDataPath = scoresPath.split('.')[0] + '-' + pair[0] + '-' + pair[1] + '.txt'

        startTime = time()
        results, ensemble, originalLabels, encodedLabels = self.ensembleModel(config, parameters, models, 
          inputPath=inputPath, nproc=nproc, seed=seed)
        results = [results]
        if len(results) > 0:
          self.outputToFile(scoresPath, metaDataPath, orderedKeys, parameters, originalLabels, results, 
            [time()-startTime])

      if isinstance(seed, int):
        seed += 1


  def runModelSelection(self, config, scoresPath, trainData, testData=None, parameters=defaultParameters, search='grid', 
    nproc=1, numIterations=30, seed=None, labelsToDrop=['14', 'unknown'], ignoredModels=['default', 'dummy', 'svc']):
    orderedKeys = list(parameters.keys())
    bestScore = -np.inf
    bestModel = None
    bestModelName = None

    for i in range(numIterations):
      modelKeys = [key for key in list(c.bestModels.keys()) if key not in ignoredModels]
      
      for modelName in modelKeys:
        retries = 0
        if len(scoresPath) > 0:
          metaDataPath = scoresPath.split('.')[0] + '-' + modelName + '.txt'
        searchParams = { modelName: c.gridSearch[modelName] }
        startTime = time()
        
        while retries < 5:
          results, originalLabels, rowCounts = self.cleanAndModel(config, parameters, searchParams, trainData, testData, 
            search=search, nproc=nproc, seed=seed, labelsToDrop=labelsToDrop)
          microF1 = results[0][0][4]

          if len(results) > 0 and len(scoresPath) > 0:
            self.outputToFile(scoresPath, metaDataPath, rowCounts, orderedKeys, parameters, originalLabels, results, 
              [time()-startTime])
            retries = float('inf')

          elif len(scoresPath) == 0:
            retries = float('inf')

          else:
            logging.debug('Model training failed. Model: %s Retry: %d', modelName, retries)
            retries += 1
        if microF1 > bestScore:
          bestResults = results

      if isinstance(seed, int):
        seed += 1
    return bestResults


  def serializeSklearnModels(self, parameters, inputPath='text/amt.csv', outputPath='models/', bestModel=False, nproc=1,
    seed=None):
    try:
      df, rowCounts = ta.readData(inputPath, parameters, labelsToDrop=['14', 'unknown'])
    except:
      logging.error(traceback.format_exc())
      raise ValueError('Failed to find/read data. Please use a csv file with two columns')
    
    df, encoding = ta.encodeData(df)
    df = ta.cleanText(df, parameters)

    if isinstance(bestModel, str):
      models = { bestModel: c.models[bestModel] }
    elif bestModel == True:
      models = { 'default': c.models['default'] }
    else:
      models = c.bestModels
    
    self.createDirs(outputPath)

    results = {}
    for modelName in models.keys():
      if modelName != 'default':
        logging.info('Serializing %s', modelName)
        model = c.models[modelName]()
        if not hasattr(model, 'predict_proba'):
          model = CalibratedClassifierCV(model)
          
        pipeline = ta.getPipeline(modelName, model, parameters)
        pipeline.fit(df['X'], df['y'])

        dump(pipeline, outputPath + modelName + '.joblib') # save as .joblib
        with open(outputPath + modelName + '.pkl', 'wb') as fid: # save as .pkl
          cPickle.dump(pipeline, fid)


  def runBestKerasModels(self, config, parameters, data='text/amt.csv', serializePath='models/text/', 
    models=c.bestKerasTextModels, nproc=1, seed=None, plotDir=None):
    '''Train and serialize Keras text models'''
    self.createDirs('tmp/checkpoints/')
    self.createDirs(serializePath)
    self.createDirs(plotDir)
    results = []
    clf = TextClassifier()
    orderedKeys = list(parameters.keys())
    tokenizer = None
    xTrain, xTest, yTrain, yTest, encoding, rowCounts = self.getTrainTestData(parameters, data, None, seed=seed, 
      nproc=nproc)
    gc.collect()

    for embedding, modelNames in models.items():
      if len(modelNames) > 0:
        modelParams = { 'embedding': (embedding, c.embeddingPaths[embedding]) }
        # architectures = { model: func for (model, func) in c.textArchitectures.items() if model in modelNames }
        architectures = c.textArchitectures
        try:
          ongoingResults, times, tokenizer, metadata = clf.trainModel(config, xTrain, xTest, yTrain, yTest, encoding, 
            parameters, modelParams, tokenizer=tokenizer, architectures=architectures, serializePath=serializePath, 
            plotDir=plotDir)
          results = results + ongoingResults
        except:
          logging.error(traceback.format_exc())
          logging.error('Failed to train models %s', str(modelNames))
    return results, tokenizer, metadata


  def trainAndSelectModel(self, config, parameters, data, nproc=1, seed=None, useDeepLearning=None):
    '''Train sklearn models and return the highest performer along with some metrics'''
    if useDeepLearning == None:
      useDeepLearning = (config.get('GCP', 'deep_learning').lower() in ['t', 'true', '1', 'yes', 'y'])
    metadata = None
    tokenizer = None
    
    if useDeepLearning == True:
      allResults, tokenizer, metadata = self.runBestKerasModels(config, parameters, data, serializePath=None, 
        nproc=nproc, seed=seed)
    else:
      bestResults = self.runModelSelection(config, '', data, testData=None, parameters=parameters, search='random', 
        nproc=nproc, numIterations=1, seed=seed)

    bestResult = self.pickBestResults(allResults)
    result = {
      'modelName': bestResult[0][-1],
      'accuracy': bestResult[0][0],
      'microF1': bestResult[0][3],
      'macroF1': bestResult[0][6],
      'weightedF1': bestResult[0][9],
      'model': bestResult[-1],
      'metadata': metadata,
      'tokenizer': tokenizer,
    }
    return result


  def pickBestResults(self, allResults):
    '''Pick best model based on micro F1'''
    bestScore = -float('inf')
    bestResult = None
    for result in allResults:
      microF1 = result[0][3]
      if microF1 > bestScore:
        bestScore = microF1
        bestResult = result
    return bestResult
  