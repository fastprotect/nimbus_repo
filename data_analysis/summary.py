'''
@author: Christian O'Leary
'''
import pandas as pd
import seaborn as sns
import matplotlib as plt
import os, csv

from xlsxwriter.workbook import Workbook

class Summary():
  __defaultSignals = ['offensive', 'depressive', 'substance_abuse', 'gambling', 'academic_misconduct', 'no_signal']

  def __init__(self, path=None, signals=None):
    self.__path = path
    if signals == None:
      signals = self.__defaultSignals
    self.__signals = signals


  def modelSummary(self, path, signals=None):
    '''Summarise modelling results'''
    self.__path = path
    pathParts = self.__path.split('/')
    self.__outputDir =  '/'.join(pathParts[:-1]) + '/'
    self.createOutputDir()

    self.__resultsFile = pathParts[-1].split('.')[0]
    meanCsvFile = self.__outputDir + self.__resultsFile + '_mean.csv'
    maxCsvFile = self.__outputDir + self.__resultsFile + '_max.csv'
    summaryXlsxFile = self.__outputDir + self.__resultsFile + '.xlsx'

    df = pd.read_csv(self.__path)
    maxDf = df.groupby('model').max()
    maxDf.to_csv(maxCsvFile)
    meanDf = df.groupby('model').mean()
    meanDf.to_csv(meanCsvFile)
    meanDf = pd.read_csv(meanCsvFile, index_col=False)

    self.createExcelModelSummary(meanDf, summaryXlsxFile, signals)


  def createOutputDir(self):
    '''Determine the output directory for summary components'''
    dirCount = 0
    while os.path.exists(self.__outputDir + str(dirCount)):
      dirCount += 1
    self.__outputDir = self.__outputDir + str(dirCount) + '/'
    os.makedirs(self.__outputDir) # create a directory for summary


  def createExcelModelSummary(self, df, summaryXlsxFile, signals):
    cols = list(df.columns)
    cols = [col.strip() for col in cols]
    columnIndices = { col: cols.index(col) for col in cols }

    writer = pd.ExcelWriter(summaryXlsxFile, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Sheet1')
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Summary table
    rowNum = df.shape[0] + 2
    worksheet.write(rowNum, 0, 'Summary')
    rowNum += 1
    headerNum = rowNum
    summaryCols = ['model', 'accuracy', 'microF1', 'macroF1', 'weightedF1', 'MatthewsCorrCoef', 'time']
    for colNum, header in enumerate(summaryCols):
      worksheet.write(headerNum, colNum, header) # Write header
      dfColNum = columnIndices[header] # Get col index of header in original df

      rowNum = headerNum + 1
      for row in range(df.shape[0]): # for each model
        worksheet.write(rowNum, colNum, df.iloc[row][dfColNum])
        rowNum += 1

    # 3 tables for class scores
    titles = ['Precision', 'Recall', 'F1']
    for title in ['Precision', 'Recall', 'F1']:
      rowNum += 3
      worksheet.write(rowNum, 0, title)
      rowNum += 1

      for colNum, header in enumerate(['model'] + signals):
        worksheet.write(rowNum, colNum, header) # Write header
      rowNum += 1

      for row in range(df.shape[0]): # for each model
        worksheet.write(rowNum, 0, df.iloc[row][0]) # write model name

        for colNum, signal in enumerate(signals): # for each signal
          colIndex = columnIndices[signal + title] # get index of metric (e.g. offensivePrecision)
          worksheet.write(rowNum, colNum+1, df.iloc[row][colIndex]) # write to excel
        rowNum += 1

    # Set column widths and save
    maxColSize = max(df.astype('str').applymap(lambda x: len(x)).max())
    worksheet.set_column('A:BZ', int(maxColSize * 0.7))
    writer.save()


  def paramSummary(self, path, signals=None):
    '''Generate summary based on paramters used'''
    self.__path = path
    pathParts = self.__path.split('/')
    self.__outputDir =  '/'.join(pathParts[:-1]) + '/'
    # self.createOutputDir()

    self.__resultsFile = pathParts[-1].split('.')[0]
    summaryXlsxFile = self.__outputDir + self.__resultsFile + '.xlsx'

    df = pd.read_csv(self.__path)
    self.createExcelParamSummary(df, summaryXlsxFile, signals)


  def createExcelParamSummary(self, df, summaryXlsxFile, signals):
    cols = list(df.columns)
    paramCols = cols[:cols.index('accuracy')]
    if 'spellCheck' in paramCols: paramCols.remove('spellCheck')
    if 'useLexicons' in paramCols: paramCols.remove('useLexicons')
    columnIndices = { col: cols.index(col) for col in cols }

    writer = pd.ExcelWriter(summaryXlsxFile, engine='xlsxwriter')
    pd.DataFrame().to_excel(writer, sheet_name='Sheet1')
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Summary table
    rowNum = 0
    worksheet.write(rowNum, 0, 'Summary')
    rowNum += 1
    headerNum = rowNum
    
    metrics = ['accuracy', 'microF1', 'macroF1', 'weightedF1', 'MatthewsCorrCoef', 'time']
    worksheet.write(rowNum, 0, 'Parameter')
    for index, metric in enumerate(metrics):
      worksheet.write(rowNum, index+1, metric)
    rowNum += 1

    # Add a 'default' row, i.e. least amount of pre-processing possible
    worksheet, rowNum, thresholds = self.addRows(worksheet, workbook, df, metrics, rowNum, paramCols)

    # Add rows for other pre-processing steps
    worksheet, rowNum, _ = self.addRows(worksheet, workbook, df, metrics, rowNum, paramCols, thresholds=thresholds, excluded=[None, False, 'None', 'False'])
    
    # 3 tables for class scores
    titles = ['Precision', 'Recall', 'F1']
    for title in ['Precision', 'Recall', 'F1']:
      rowNum += 3
      worksheet.write(rowNum, 0, title)
      rowNum += 1
      for colNum, header in enumerate(['model'] + signals):
        worksheet.write(rowNum, colNum, header) # Write header
      rowNum += 1

      metrics = [signal+title for signal in signals]

      # Add a 'default' row, i.e. least amount of pre-processing possible
      worksheet, rowNum, thresholds = self.addRows(worksheet, workbook, df, metrics, rowNum, paramCols)

      # Add rows for other pre-processing steps
      worksheet, rowNum, _ = self.addRows(worksheet, workbook, df, metrics, rowNum, paramCols, thresholds=thresholds, excluded=[None, False, 'None', 'False'])
    
    # Set column widths and save
    maxColSize = max(df.astype('str').applymap(lambda x: len(x)).max())
    worksheet.set_column('A:BZ', int(maxColSize * 0.7))
    writer.save()


  def addRows(self, worksheet, workbook, df, metrics, rowNum, paramCols, thresholds=[], excluded=[], default='(1, 1)'):
    BRIGHTNESS = 2 # brightness of fill (1 is fairly dark)
    newThresholds = list()
    for param in paramCols:
      options = df[param].unique()
      for option in options:
        if option not in excluded:
          rows = df.groupby(param).mean()
          if len(excluded) > 0 and option != default:
            worksheet.write(rowNum, 0, str(param) + '-' + str(option))
          elif len(excluded) < 1 and option == default:
            worksheet.write(rowNum, 0, 'Default')
          
          if len(excluded) > 0 or option == default:
            colNum = 1
            for i in range(len(metrics)):
              score = float(rows[metrics[i]].mean())
              if option == default:
                newThresholds.append(score)
                
              color = workbook.add_format()
              color.set_bg_color('#aaaaaa')
              if len(thresholds) > 0:
                color = self.getColor(color, thresholds[i], score, BRIGHTNESS)

              if len(excluded) > 0 or option == default:
                worksheet.write(rowNum, colNum, score, color)
              colNum += 1
            rowNum += 1
    return worksheet, rowNum, newThresholds


  def getColor(self, color, expected, score, BRIGHTNESS):
    '''Determine cell color based on score difference'''
    difference = (expected - score) / ((expected + score + 0.0000001) / 2)
    decColor = round(128 * abs(difference) * BRIGHTNESS) + 128
    if decColor > 255: decColor = 255
    colorCode = '{:02x}'.format(decColor)

    if score > expected:
      color.set_bg_color('#22' + colorCode + '22')
    elif score < expected:
      color.set_bg_color('#' + colorCode + '2222')
    else:
      color.set_bg_color('#dddddd')
    return color


if __name__ == "__main__":
  summary = Summary()
  # summary.modelSummary('results/model-selection/twitter.csv', signals=['negative', 'positive'])
  # summary.modelSummary('results/model-selection/art.csv', signals=['negative', 'positive'])
  # summary.modelSummary('results/model-selection/art-ensemble.csv', signals=['0', '1'])
  # summary.modelSummary('results/model-selection/art-model-selection.csv', signals=['0', '1'])
  # summary.modelSummary('results/model-selection/pandera.csv', signals=['academic_misconduct', 'depressive', 'gambling', 'no_signal', 'offensive', 'substance_abuse'])

  # summary.paramSummary('results/pandera-pp.csv', signals=['academic_misconduct', 'depressive', 'gambling', 'no_signal', 'offensive', 'substance_abuse'])
  # summary.modelSummary('results/sstweet-keras.csv', signals=['0', '1', '2'])
  # summary.modelSummary('results/stsgold-keras.csv', signals=['0', '1'])

  summary.modelSummary('results/amt.csv', signals=['Academic Misconduct', 'Depressive', 'Gambling', 'No Signal', 'Offensive', 'Substance Abuse'])