'''
@author: Christian O'Leary
'''

from time import time
from sklearn.calibration import CalibratedClassifierCV
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import GridSearchCV, train_test_split, cross_val_predict, StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from data_analysis.classifier_wrapper import ClassifierWrapper
import data_analysis.text_analysis as ta
import data_analysis.constants as c
import pandas as pd
import traceback, datetime, logging


def readData(path, parameters, yCol='signal', labelsToDrop=None):
  '''Reads a .csv file from a path and filters columns and rows (by y label)'''
  df = pd.read_csv(path, encoding='ISO-8859-1')
  if labelsToDrop is not None:
    for label in labelsToDrop:
      df = df[df[yCol] != label]
  logging.debug('# rows: %d, # cols: %d', df.shape[0], df.shape[1])
  return encodeData(df)


def encodeData(df):
  df['text'] = df['text'].astype(str)
  # TODO
  return df


def divideDataByType(df, textCol='text', yCol='signal'):
  '''Return a dataframe without text and a dataframe with text as the only feature.'''
  return df.loc[:, df.columns != textCol], df[[textCol, yCol]]


def splitData(df, yCol, seed=None, testSize=0.2, stratify=True):
  y = df[[yCol]]
  features = df.drop(yCol, 1)
  X = features
  if features.shape[1] == 1:
    X = features['text'].astype(str)

  if stratify:
    xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=testSize, random_state=seed, stratify=y)
  else:
    xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=testSize, random_state=seed)

  logging.debug('Training data: %s %s', str(xTrain.shape), str(yTrain.shape))
  logging.debug('Testing data: %s %s', str(xTxTestrain.shape), str(yTest.shape))
  return xTrain, xTest, yTrain, yTest, df.drop(yCol, 1).columns.values


def trainImageEnsemble(config, df, dataframes, models, parameters, originalLabels, encodedLabels, search='grid', 
  nproc=1, seed=None):
  ta.validateParameters(parameters, c.parameters)
  # validateModels(models) # TODO
  # validateData(dataframes) # TODO

  originalNProcesses = nproc
  results = {}
  trainedModels = {}
  pipelines = {}
  weights = []
  modelNames = ''

  for dataType in dataframes.keys():
    modelName = list(models[dataType].keys())[0]
    if len(modelNames) == 0:
      modelNames = modelName
    else:
      modelNames += '-' + modelName
    paramSpace = models[dataType][modelName]

    if modelName in ['randomForest', 'gaussianNB', 'keras']:
      nproc = 1
    else:
      nproc = originalNProcesses

    # try:
    logging.debug('Using model %s', modelName)
    logging.debug('Starting time %s', str(datetime.datetime.now()))
    startTime = time()

    # Create model & pipeline
    classifier = c.models[modelName]()
    params = models[modelName]

    if seed != None:
      if modelName != 'bernoulliNB' and modelName != 'knn':
        classifier.set_params(random_state=seed) # use np.random.seed() instead
      else:
        logging.Warning('Warning: cannot set random state of bernoulliNB or KNN')

    if not hasattr(classifier, 'predict_proba'):
      # linearSvc, passiveAggressive and perceptron all need this wrapper to output probabilities
      logging.debug('Wrapping model (%s) in CalibratedClassifierCV to output probabilities', modelName)
      classifier = CalibratedClassifierCV(classifier)
      params = {}
      for key, param in params.items():
        parts = key.split('__')
        newParams[ parts[0] + '__base_estimator__' + parts[1] ] = param
      paramSpace = params
    
    xTrain, xTest, yTrain, yTest, featureNames = splitData(dataframes[dataType], yCol='signal', seed=seed, testSize=0.2,
     stratify=True)

    if dataType == 'features':
      pipeline = getPipeline(modelName, classifier, parameters)
    elif dataType == 'textOnly':
      pipeline = ta.getPipeline(modelName, classifier, parameters)
      df['text'] = dataframes[dataType]['text']

    splits = 5
    if parameters['stratifyCv'] == True:
      splits = StratifiedKFold(n_splits=splits)

    verbosity = config.getint('LOG', 'sklearn_verbosity')
    if search == 'grid':
      cvSearch = GridSearchCV(pipeline, params, cv=splits, n_jobs=nproc, error_score=0.0, verbose=verbosity)
    elif search == 'random':
      cvSearch = RandomizedSearchCV(pipeline, params, random_state=seed, n_iter=30, cv=splits, n_jobs=nproc, \
        error_score=0.0, verbose=verbosity)
    else:
      logging.exception('Unexpected value for search parameter: %s', search)
      raise ValueError('Unexpected value for search parameter:', search)

    cvSearch.fit(xTrain, yTrain)
    yPred = cross_val_predict(cvSearch, xTest, yTest, cv=5)

    bestParameters = cvSearch.best_params_
    allParameters = cvSearch.cv_results_
    bestModel = cvSearch.best_estimator_
    scores = ta.scoreResults(yTest, yPred, originalLabels, encodedLabels)
    weights.append(scores[4]) # Micro F1

    logging.debug('Time: %.2f, seconds', time()  - startTime)
    logging.debug('Accuracy: %.6f', scores[0])
    scores.append(modelName)

    results[dataType] = [scores, bestParameters, allParameters, bestModel]
    trainedModels[dataType] = (pipeline, featureNames)
    pipelines[dataType] = pipeline
    # except:
    #   trace = traceback.format_exc()
    #   logging.error(trace)
    #   raise ValueError('Failed to run model:', modelName, 'Data type:', dataType)

  ensemble = ClassifierWrapper()
  ensemble.set_params(specifications=trainedModels)
  ensemble.set_params(weights=weights)
  scores = evaluateEnsemble(ensemble, df, originalLabels, encodedLabels, seed, nproc)
  scores.append(modelNames)  
  results = [scores, bestParameters, allParameters, bestModel]
  return results, ensemble


def evaluateEnsemble(ensemble, df, originalLabels, encodedLabels, seed, nproc):
  xTrain, xTest, yTrain, yTest, featureNames = splitData(df,  yCol='signal', seed=seed, testSize=0.2, stratify=True)
  yPred = cross_val_predict(ensemble, xTest, yTest, cv=5)
  return ta.scoreResults(yTest, yPred, originalLabels, encodedLabels)


def getPipeline(modelName, classifier, parameters):
  '''
  Creates a pipeline which may include (in order):
  - selectKBest => DenseTransformer => scaling => classifier
  The DenseTransformer is used to convert the sparse matrix output for vectorization 
  into a dense matrix so that Naive Bayes and Random Forest models can be used.
  '''
  pipelineParts = []
  if parameters['scaling'] == 'std':
    pipelineParts.append(('scaler', StandardScaler()))
  pipelineParts.append(('classifier', classifier))
  return Pipeline(pipelineParts)
    

def validateModels(models):
  '''
  models is a dict with two keys. Keys should be "features" and "textOnly".
  Values should take the form: { "modelName": gridParamsDict }
  '''
  logging.warning('TODO: validateModels()')
