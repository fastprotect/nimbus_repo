# -*- coding: utf-8 -*-
'''
@author: Christian O'Leary
'''
import warnings, sys, os, traceback, datetime, logging
warnings.filterwarnings('ignore')
if not sys.warnoptions:
  warnings.simplefilter("ignore")
  os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses

import data_analysis.constants as c
from data_analysis.dense_transformer import DenseTransformer
import pandas as pd
import numpy as np

from sklearn.calibration import CalibratedClassifierCV
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.metrics import accuracy_score, precision_recall_fscore_support, matthews_corrcoef
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, train_test_split, cross_val_predict, \
  StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
# from imblearn.combine import SMOTETomek, SMOTEENN
# from imblearn.over_sampling import RandomOverSampler#, SMOTE, ADASYN
# from imblearn.pipeline import Pipeline
# from imblearn.under_sampling import RandomUnderSampler

from nltk.stem import LancasterStemmer, PorterStemmer, WordNetLemmatizer
from nltk.tokenize import TweetTokenizer, WordPunctTokenizer
from nltk.tokenize.api import TokenizerI

# from autocorrect import spell
from multiprocessing import Pool
from functools import partial
from nltk.corpus import stopwords
from textblob import TextBlob
from bs4 import BeautifulSoup
from time import time
import re, itertools


def readData(path, parameters, colsToUse=None, labelsToDrop=None, seed=None, minClassSize=25, encoding='ISO-8859-1'):
  '''Reads a .csv file from a path and filters columns and rows (by y label). Default value of minClassSize is 25 as the
  default CV split is currently hardcoded to 5; thus: 5/(1/5)=25. Otherwise there won't be enough instances to perform 
  CV splits during training and testing.'''
  if path.split('.')[-1] == '.xlsx':
    df = pd.read_excel(path, usecols=colsToUse, encoding=encoding)  
  else:
    df = pd.read_csv(path, usecols=colsToUse, encoding=encoding)

  rowCounts = { 'Initial': df.shape[0] }
  df.columns = ['X', 'y']
  if labelsToDrop is not None:
    for label in labelsToDrop:
      df = df[df['y'] != label]
  rowCounts['Drop unknown values'] = df.shape[0]
  logging.debug('# rows: %d, # cols: %d', df.shape[0], df.shape[1])

  if isinstance(parameters['maxClassSize'], int):
    logging.debug('Enforcing max class size of %s', str(parameters['maxClassSize']))
  
  uniqueLabels = df['y'].unique()
  if uniqueLabels.shape[0] < 2:
    logging.exception('Only one label type present in the dataset: %s', str(uniqueLabels[0]))
    raise ValueError('Only one label type present in the dataset:', uniqueLabels[0])

  for label in uniqueLabels:
    instances = df[df['y'] == label]
    classSize = instances.shape[0]
    if classSize < minClassSize: # not enough instances for CV splits during training and test
      logging.exception('Not enough instances of class: %s. Found: %d', label, classSize)
      raise ValueError('Not enough instances of class', label, 'Found', classSize)

    if isinstance(parameters['maxClassSize'], int):
      if classSize > parameters['maxClassSize']:
        logging.debug('Dropping %d from %s', (classSize - parameters['maxClassSize']), label)
        df = df[df['y'] != label] # drop all instances of class
        instances = instances.sample(frac=1, random_state=seed).reset_index(drop=True) # shuffle
        instances = instances.head(parameters['maxClassSize'])
        df = pd.concat([df, instances])
        rowCounts['Drop from ' + str(label), '(max. ' + str(parameters['maxClassSize']) + ')'] = df.shape[0]
  return df, rowCounts


def encodeData(df, yCol='y'):
  '''Converts y labels to numbers and ensures values are strings'''
  listMask = df.applymap(lambda x: isinstance(x, list)).all()
  if yCol in listMask.index[listMask].tolist():
    df['y'] = df['y'].apply(lambda signals: signals[0])
  
  uniqueLabels = sorted(df[yCol].unique())
  standardLabels = [(label in c.labelEncoding.keys()) for label in uniqueLabels]
  
  if all(standardLabels) == True: # use default encoding
    encodings = c.labelEncoding
  else:
    encodings = { uniqueLabels[i]: i for i in range(len(uniqueLabels)) }

  # Encode classes
  newEncoding = {}
  for label, encoding in encodings.items():
    if label in uniqueLabels:
      indices = df[yCol] == label
      df.loc[indices, yCol] = encoding
      newEncoding[label] = encoding
      logging.debug('Label: %s -> Encoding: %s -> Frequency: %d', label, encoding, indices.sum())

  # Ensure all values are string
  df = df.applymap(str)
  df = df.astype(str)
  return df, newEncoding


def cleanText(df, parameters, textCol='X', nproc=1, threshold=100000):
  '''Clean data using cleanTweet() and dropna(). Uses multiprocessing if the dataframe is big enough (threshold 
  parameter).'''
  if nproc > 1 and df.shape[0] > threshold:
    results = [df['X'].iloc[i] for i in range(1000)]
    start = time()
    dfSplit = np.array_split(df[textCol], nproc)
    pool = Pool(nproc)
    df[textCol] = pd.concat(pool.map(partial(cleanTweets, parameters=parameters), dfSplit))
    pool.close()
    pool.join()
  else:
    df[textCol] = df[textCol].apply(lambda tweet : cleanTweet(tweet, parameters))

  return df.dropna()


def cleanTweets(tweets, parameters):
  '''Used to apply cleanTweet() to a pandas series. This needs to exist as a first class function for multiprocessing'''
  return tweets.apply(lambda tweet : cleanTweet(tweet, parameters))


def cleanTweet(text, parameters):
  '''Validates cleaning parameters and uses them to clean text (e.g. a tweet). The functions called are not 
  commutative.'''
  validateParameters(parameters, c.parameters)
  text = soup(text, parameters)
  text = expandContractions(text, parameters)
  text = removePatterns(text, parameters)
  text = removeNonAscii(text, parameters)
  text = checkSpelling(text, parameters)
  return text.strip()


def validateParameters(parameters, parameterDefinitions):
  '''Ensures that the parameters only contains valid keys and values'''
  for key in parameters.keys():
    if key not in parameterDefinitions.keys():
      raise ValueError('Unexpected parameter type: ', key, 'Expected:', parameterDefinitions.keys())

    if isinstance(parameterDefinitions[key], list):
      unexpectedValue = parameters[key] not in parameterDefinitions[key]
      validInt = isValidInt(parameters[key], parameterDefinitions, key)
      if unexpectedValue and not validInt:
        raise ValueError('Unexpected parameter value for: ', key, 'Expected:', parameterDefinitions[key], 'Received:', \
          parameters[key])

  for key in parameterDefinitions.keys():
    if key not in parameters.keys():
      raise ValueError('Missing key:', key)


def isValidInt(value, parameterDefinitions, key):
  '''Checks is a value is an int (but not a bool) and is in the list specified by the parameterDefinitions value'''
  return isinstance(value, tuple) or (isinstance(value, int) and not isinstance(value, bool) and int in \
    parameterDefinitions[key])


def soup(text, parameters):
  '''Convert HTML encodings and remove UTF-8 BOM'''
  if parameters['soup'] is True:
    soup = BeautifulSoup(text, 'lxml')
    souped = soup.get_text()
    try: text = souped.decode("utf-8-sig").replace(u"\ufffd", "?")
    except: text = souped
  return text


def removePatterns(text, parameters):
  '''Remove characters based on regex matching'''
  if parameters['removeHashtags'] is True:
    text = re.sub(c.hashtagPattern, '', text)

  if parameters['removeHttp'] is True:
    text = re.sub(c.httpspattern, '', text)

  if parameters['removeMention'] is True:
    text = re.sub(c.mentionPattern, '', text)

  if parameters['removeWww'] is True:
    text = re.sub(c.wwwPattern, '', text)

  if parameters['removePunctuation'] is True: 
    text = re.sub(c.punctuationPattern, '', text)
  return text


def expandContractions(text, parameters):
  '''Expands contractions in text. Note: could also consider using pycontractions although the module seems to be slow 
  based on some quick testing.'''
  if parameters['expandContractions'] is True:
    text = c.contractionPattern.sub(lambda x : c.contractions[x.group()], text)
  return text


def removeNonAscii(text, parameters):
  '''Strip non-ascii characters (which includes emoji) from text'''
  if parameters['removeNonAscii'] is True:
    text = text.encode('ascii', 'ignore').decode('ascii')
  return text


def checkSpelling(text, parameters):
  '''Corrects word spelling in text'''
  if parameters['spellCheck'] == 'textblob':
    text = str(TextBlob(text).correct())
  elif parameters['spellCheck'] == 'autocorrect':
    text = spell(text)
  return text


def splitData(df, seed=None, testSize=0.2, stratify=True):
  '''Expects pandas dataframe with two cols ('X' and 'y')'''
  y = df[['y']]
  test = df.drop('y', 1)
  X = df['X']

  # Split data into train and test
  if stratify:
    xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=testSize, random_state=seed, stratify=y)
  else:
    xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=testSize, random_state=seed)

  logging.debug('Training data: %s %s', str(xTrain.shape), str(yTrain.shape))
  logging.debug('Testing data: %s %s', str(xTest.shape), str(yTest.shape))
  return xTrain, xTest, yTrain, yTest


def model(config, xTrain, xTest, yTrain, yTest, originalLabels, encodedLabels, searchParams, parameters, search='grid', 
  nproc=1, seed=None):
  '''
  Creates pipelines, performs a grid search and calculates scores. Returns a list of tuples 
  (one for each model/grid search). Each tuple contains the best model's predictions, 
  accuracy, F1 scores, and the grid search output (GridSearchCV.cv_results_)
  '''
  validateParameters(parameters, c.parameters)
  results = []
  originalNProcesses = nproc

  for modelName in searchParams.keys():
    if modelName in ['randomForest', 'gaussianNB', 'keras']:
      nproc = 1
    else:
      nproc = originalNProcesses
    
    try:
      logging.info('Using model %s', modelName)
      logging.info('Starting time %s', str(datetime.datetime.now()))
      startTime = time()

      # Create model & pipeline
      classifier = c.models[modelName]()
      params = searchParams[modelName]

      if seed != None:
        if modelName != 'bernoulliNB' and modelName != 'knn':
          classifier.set_params(random_state=seed) # use np.random.seed() instead
        else:
          logging.warning('Cannot set random state of bernoulliNB or KNN')

      if not hasattr(classifier, 'predict_proba'):
        # linearSvc, passiveAggressive and perceptron all need this wrapper to output probabilities
        classifier = CalibratedClassifierCV(classifier)
        newParams = {}
        for key, param in params.items():
          parts = key.split('__')
          newParams[ parts[0] + '__base_estimator__' + parts[1] ] = param
        params = newParams

      pipeline = getPipeline(modelName, classifier, parameters)
      
      splits = 5
      if parameters['stratifyCv'] == True:
        splits = StratifiedKFold(n_splits=splits)

      verbosity = config.getint('LOG', 'sklearn_verbosity')
      if search == 'grid':
        cvSearch = GridSearchCV(pipeline, params, cv=splits, n_jobs=nproc, error_score=0.0, verbose=verbosity)
      elif search == 'random':
        cvSearch = RandomizedSearchCV(pipeline, params, random_state=seed, n_iter=30, cv=splits, n_jobs=nproc, \
          error_score=0.0, verbose=verbosity)
      else:
        logging.exception('Unexpected value for search parameter: %s', search)
        raise ValueError('Unexpected value for search parameter:', search)
      
      cvSearch.fit(xTrain, yTrain)
      yPred = cross_val_predict(cvSearch, xTest, yTest, cv=5) # uses StratifiedKFold by default

      bestParameters = cvSearch.best_params_
      allParameters = cvSearch.cv_results_
      bestModel = cvSearch.best_estimator_
      scores = scoreResults(yTest, yPred, originalLabels, encodedLabels)

      logging.debug('----------------------------------')
      logging.debug('Time: %.2f seconds.', time()  - startTime)
      logging.debug('MicroF1: %.6f seconds.', scores[7])
      scores.append(modelName)
      results.append([scores, bestParameters, allParameters, bestModel])
    except:
      logging.exception(traceback.format_exc())
      logging.error('Failed to run model: %s', modelName)
      # raise ValueError('Failed to run model:', modelName)
  
  return results


def getPipeline(modelName, classifier, parameters):
  '''
  Creates a pipeline which may include (in order):
  - vectorizer => selectKBest => DenseTransformer => scaler => classifier
  The DenseTransformer is used to convert the sparse matrix output for vectorization into a 
  dense matrix so that scaling or Naive Bayes, Random Forest and Keras models can be used.
  '''
  pipelineParts = [
    ('vectorizer', getVectorizer(parameters)),
    ('classifier', classifier)
  ]

  if parameters['resampling'] != None:
    pipelineParts.insert(1, ('resampling', getResampler(parameters)))

  if modelName == 'keras':
    vocabSize = parameters['maxVocabFeatures']
    if vocabSize == None:
      vocabSize = 10000
    pipelineParts.insert(1, ('selectKBest', SelectKBest(k=vocabSize)))

  if modelName in ['gaussianNB', 'randomForest', 'keras'] or parameters['scaling'] != None:
    pipelineParts.insert(1, ('to_dense', DenseTransformer()))
    
  if parameters['scaling'] == 'std':
    pipelineParts.insert(2, ('scaler', StandardScaler(with_mean=False)))

  return Pipeline(pipelineParts)


def getVectorizer(parameters):
  '''Create a vectorizer'''
  if parameters['vectorizer'] == 'count':
    vectorizer = CountVectorizer()
  else:
    vectorizer = TfidfVectorizer()
    vectorizer.set_params(norm=parameters['tfidf-norm'])
    vectorizer.set_params(use_idf=parameters['tfidf-use_idf'])
    vectorizer.set_params(smooth_idf=parameters['tfidf-smooth_idf'])
    vectorizer.set_params(sublinear_tf=parameters['tfidf-sublinear_tf'])

  vectorizer.set_params(lowercase=parameters['lowercase'])
  vectorizer.set_params(max_features=parameters['maxVocabFeatures'])
  vectorizer.set_params(ngram_range=parameters['ngram'])
  vectorizer.set_params(strip_accents=parameters['stripAccents'])
  vectorizer.set_params(tokenizer=TokenizerReducer(parameters).tokenize)

  if parameters['removeStopwords'] == 'nltk':
    vectorizer.set_params(stop_words=set(stopwords.words('english')))
  elif parameters['removeStopwords'] == 'default':
    vectorizer.set_params(stop_words='english')

  return vectorizer


def getResampler(parameters, n_jobs=1, seed=None):
  method = None
  resampler = None
  if 'resampling' in parameters:
    method = parameters['resampling']
    
  if method == 'adasyn':
    resampler = ADASYN(n_jobs=n_jobs, random_state=seed)
  elif method == 'randomOverSampling':
    resampler = RandomOverSampler(random_state=seed)
  elif method == 'randomUnderSampling':
    resampler = RandomUnderSampler(random_state=seed, sampling_strategy='majority')
  elif method == 'smote':
    resampler = SMOTE(n_jobs=n_jobs, random_state=seed)
  elif method == 'smoteEnn':
    resampler = SMOTEENN(n_jobs=n_jobs, random_state=seed)
  elif method == 'smoteTomek':
    resampler = SMOTETomek(n_jobs=n_jobs, random_state=seed)
  elif method != None:
    raise ValueError('Resampling method not recognised:', method)
  return resampler


def scoreResults(yTest, yPred, originalLabels, encodedLabels):
  '''
  Returns the accuracy, micro/macro/weighted/per-class precision, recall and F1 scores.
  E.g. If there are 6 classes, this function returns 35 scores:
   - 1 accuracy score
   - 3 micro Precision/Recall/F1 scores
   - 3 macro Precision/Recall/F1 scores
   - 3 weighted Precision/Recall/F1 scores
   - 3 * 6 Precision/Recall/F1 scores (3 scores for each class)
   - 6 support values
   - 1 Matthew's Correlation Coefficient
  '''
  yTest = yTest.astype(str)
  yPred = yPred.astype(str)
  scores = [accuracy_score(yTest, yPred)]
  averages = ['micro', 'macro', 'weighted', None]
  for average in averages:
    # output is a list of lists
    output = precision_recall_fscore_support(yTest, yPred, average=average, labels=encodedLabels)
    if average is None:
      scores += list(itertools.chain(*output))
    else:
      scores += list(output)[:-1] # take first three numbers of output (exclude support)
  scores.append(matthews_corrcoef(yTest, yPred))
  return scores


class TokenizerReducer(TokenizerI):
  '''Tokenizes words and reduces them to inflectional forms using stemming/lemmatization'''
  _stemmer = None
  _lemmatizer = None
  _tokenizer = None

  def __init__(self, parameters):
    self.setParameters(parameters)
    

  def tokenize(self, text):
    '''Tokenizes words and applies '''
    if self._tokenizer == None:
      raise ValueError('No tokenizer has been set in TokenizerReducer')

    words = [word for word in self._tokenizer.tokenize(text) if len(word) > 0]
    if self._stemmer != None:
      words = [self._stemmer.stem(word) for word in words]

    if self._lemmatizer != None:
      words = [self._lemmatizer.lemmatize(word) for word in words]
    return words


  def setParameters(self, parameters):
    self.setTokenizer(parameters)
    self.setStemmer(parameters)
    self.setLemmatizer(parameters)


  def setTokenizer(self, parameters) :
    '''parameters must contain key: tokenizer'''
    if parameters['tokenizer'] == 'wordPunct':
      self._tokenizer = WordPunctTokenizer()
    else:
      self._tokenizer = TweetTokenizer()
    

  def setStemmer(self, parameters):
    '''Can use Porter & Lancaster Stemmers. Parameters must contain key: stemming'''
    if parameters['stemming'] == 'lancaster':
      self.stemmer = LancasterStemmer()
    elif parameters['stemming'] == 'porter':
      self.stemmer = PorterStemmer()
  

  def setLemmatizer(self, parameters):
    '''Uses WordNet lemmatizer. Parameters must contain key: lemmatization'''
    if parameters['lemmatization'] is True:
      self._lemmatizer = WordNetLemmatizer()