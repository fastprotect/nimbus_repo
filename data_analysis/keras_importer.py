'''
@author: Christian O'Leary
'''
class KerasImporter():
    '''Handles importing of keras models to prevent importing delays and memory issues'''

    def __init__(self):
        self.imageModelNames = [
            'densenet_121', 'densenet_169', 'densenet_201',
            'inception_v3', 'inception_resnet_v2',
            'mobilenet', 'mobilenet_v2', 
            'nasnet_large', 'nasnet_mobile',
            'resnet_50', 'resnet_101', 'resnet_152', 'resnet_50_v2', 'resnet_101_v2', 'resnet_152_v2', 
            'vgg16', 'vgg19',
            'xception'
        ]

    def importImageModel(self, modelName):
        dimensions = (224, 224, 3)
        if modelName == 'densenet_121':
            from tensorflow.keras.applications.densenet import DenseNet121, preprocess_input, decode_predictions
            model = DenseNet121
        elif modelName == 'densenet_169':
            from tensorflow.keras.applications.densenet import DenseNet169, preprocess_input, decode_predictions
            model = DenseNet169
        elif modelName == 'densenet_201':
            from tensorflow.keras.applications.densenet import DenseNet201, preprocess_input, decode_predictions
            model = DenseNet201
        elif modelName == 'inception_v3':
            from tensorflow.keras.applications.inception_v3 import InceptionV3, preprocess_input, decode_predictions
            model = InceptionV3
            dimensions = (299, 299, 3)
        elif modelName == 'inception_resnet_v2':
            from tensorflow.keras.applications.inception_resnet_v2 import InceptionResNetV2, preprocess_input, decode_predictions
            model = InceptionResNetV2
            dimensions = (299, 299, 3)
        elif modelName == 'mobilenet':
            from tensorflow.keras.applications.mobilenet import MobileNet, preprocess_input, decode_predictions
            model = MobileNet
        elif modelName == 'mobilenet_v2':
            from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2, preprocess_input, decode_predictions
            model = MobileNetV2
        elif modelName == 'nasnet_large':
            from tensorflow.keras.applications.nasnet import NASNetLarge, preprocess_input, decode_predictions
            model = NASNetLarge
            dimensions = (331, 331, 3)
        elif modelName == 'nasnet_mobile':
            from tensorflow.keras.applications.nasnet import NASNetMobile, preprocess_input, decode_predictions
            model = NASNetMobile
        elif modelName == 'resnet_50':
            from tensorflow.keras.applications.resnet import ResNet50, preprocess_input, decode_predictions
            model = ResNet50
        elif modelName == 'resnet_101':
            from tensorflow.keras.applications.resnet import ResNet101, preprocess_input, decode_predictions
            model = ResNet101
        elif modelName == 'resnet_152':
            from tensorflow.keras.applications.resnet import ResNet152, preprocess_input, decode_predictions
            model = ResNet152
        elif modelName == 'resnet_50_v2':
            from tensorflow.keras.applications.resnet_v2 import ResNet50V2, preprocess_input, decode_predictions
            model = ResNet50V2
        elif modelName == 'resnet_101_v2':
            from tensorflow.keras.applications.resnet_v2 import ResNet101V2, preprocess_input, decode_predictions
            model = ResNet101V2
        elif modelName == 'resnet_152_v2':
            from tensorflow.keras.applications.resnet_v2 import ResNet152V2, preprocess_input, decode_predictions
            model = ResNet152V2
        elif modelName == 'vgg16':
            from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions
            model = VGG16
        elif modelName == 'vgg19':
            from tensorflow.keras.applications.vgg19 import VGG19, preprocess_input, decode_predictions
            model = VGG19
        elif modelName == 'xception':
            from tensorflow.keras.applications.xception import Xception, preprocess_input, decode_predictions
            model = Xception
            dimensions = (299, 299, 3)
        return dimensions, model, preprocess_input, decode_predictions

if __name__ == "__main__":
    importer = KerasImporter()
    modelNames = importer.imageModelNames
    for modelName in modelNames:
        dimensions, model, preprocess_input, decode_predictions = importer.importImageModel('xception')
        example = model()
        del example
        logging.debug('Successfully imported %s', modelName)