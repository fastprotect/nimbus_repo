'''
@author: Christian O'Leary
'''
import traceback, datetime, os, gc, logging
import _pickle as cPickle
from time import time
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
from tensorflow.keras import backend
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard, TerminateOnNaN, \
  LambdaCallback
from tensorflow.keras.initializers import Constant
from tensorflow.keras.layers import Activation, Conv1D, Dense, Dropout, Embedding, Flatten, GlobalMaxPooling1D, GRU, \
  LeakyReLU, LSTM, TimeDistributed
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.optimizers import Adam, Adadelta, Adagrad
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer

import data_analysis.constants as c
from data_analysis.dense_transformer import DenseTransformer
import data_analysis.text_analysis as ta


class TextClassifier():

  def __init__(self):
    '''Initialise default parameters'''
    self.params = {
      'serializationEpochs': 10,
      'numEpochs': 50,
      'batchSize': 32,
      'vocabSize': 10000,
      'loss': 'sparse_categorical_crossentropy',
      'optimizer': Adam(), #'adam', # Adagrad(learning_rate=0.00001), #Adadelta(), 'adam'
      'metrics': ['accuracy'],
    }


  def trainModel(self, config, xTrain, xTest, yTrain, yTest, encoding, parameters, modelParameters, tokenizer, 
    architectures, serializePath=None, plotDir=None, outputFunction=None, outputParams=None):
    '''Train a Keras model to perform sentiment analysis on text'''
    verbosity = config.getint('LOG', 'keras_verbosity')
    self.checkpoints = [] # record epochs of checkpoints
    self.bestValLoss = -np.inf
    self.params['vocabSize'] = parameters['maxVocabFeatures']
    results = []
    times = []
    originalLabels = sorted(list(encoding.keys()))
    encodedLabels = [encoding[key] for key in originalLabels]

    self.params['numClasses'] = np.unique(yTrain).shape[0]
    xTrainEmbed, yTrainEmbed, xTestEmbed, yTestEmbed, tokenizer, maxlen = self.embeddingFeatures(xTrain, yTrain, xTest, 
      yTest, parameters, tokenizer)
    # xTrain, yTrain, xTest, yTest = self.standardFeatures(xTrain, yTrain, xTest, yTest, parameters)

    gc.collect()
    
    metadata = str(maxlen) + '\n'
    for key, value in encoding.items():
      metadata += str(value) + ':' + str(key) + '\n'

    if serializePath != None:
      with open(serializePath + 'tokenizer.pkl', 'wb') as fid:
        cPickle.dump(tokenizer, fid)
      with open(serializePath + 'metadata.txt', 'w') as fid:
        fid.write(metadata)
    
    logging.info('Starting time: %s', str(datetime.datetime.now()))
    for modelName, arch in architectures.items():
      logging.info('Model: %s Embedding: %s', modelName, modelParameters['embedding'])
      modelName += '-' + modelParameters['embedding'][0]

      # try:
      model, usesEmbedding = self.getModel(parameters, modelParameters, architecture=arch)

      self.params['loss'] = 'sparse_categorical_crossentropy'
      if usesEmbedding:
        trainX = xTrainEmbed
        testX = xTestEmbed
        # trainY = yTrainEmbed
        # textY = yTestEmbed
      else:
        raise ValueError('Unfinished implementation. Must specify shape.')
        trainX = xTrain
        testX = xTest
      #   trainY = yTrain
      #   textY = yTest

      filepath = 'tmp/checkpoints/' + modelName + '.hdf5'
      if os.path.exists(filepath):
        os.remove(filepath)

      callbacks = [
        EarlyStopping(monitor='val_loss', patience=10, verbose=verbosity, mode='auto'), 
        ModelCheckpoint(filepath, monitor='val_loss', verbose=verbosity, save_best_only=True, mode='auto'),
        ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, verbose=verbosity, mode='auto'),
        TerminateOnNaN()
      ]
      if plotDir!= None and len(str(plotDir)) > 0:
        callbacks.append(TensorBoard(log_dir=plotDir, write_graph=True, write_images=True, profile_batch=0))
        callbacks.append(LambdaCallback(on_epoch_end=lambda epoch, logs: self.recordCheckpoints(epoch, logs)))

      startTime = time()

      try:
        history = model.fit(trainX, yTrain, validation_split=0.2, epochs=self.params['numEpochs'], 
          batch_size=self.params['batchSize'], callbacks=callbacks, verbose=verbosity)
      except Exception as e:
        # Try reshaping the input (for TF versions 2.*)
        yTrain = yTrain.values.reshape(-1, 1, yTrain.shape[1])
        history = model.fit(trainX, yTrain, validation_split=0.2, epochs=self.params['numEpochs'], 
          batch_size=self.params['batchSize'], callbacks=callbacks, verbose=verbosity)
      
      times.append(time() - startTime)

      # if plotDir!= None and len(str(plotDir)) > 0:
      #   numEpochs = callbacks[0].stopped_epoch # if using early stopping
      #   logging.info('Stopped epoch:' + str(numEpochs))
      #   # numEpochs = self.params['numEpochs'] - 1 # if not using early stopping
      #   self.createPlot(history, numEpochs, self.checkpoints, modelName, str(plotDir))
      
      bestModel = load_model(filepath)
      if xTest is not None: 
        predictions = bestModel.predict_classes(testX)
        yTest = np.array(yTest)
        scores = ta.scoreResults(yTest, predictions, originalLabels, encodedLabels)
        scores.append(modelName)
      else:
        scores = []
      ongoingResults = [scores, {}, {}, bestModel]
      results.append(ongoingResults)

      if serializePath != None:
        callbacks = [
          EarlyStopping(patience=10, verbose=verbosity),
          ModelCheckpoint(filepath, verbose=verbosity, save_best_only=True),
          ReduceLROnPlateau(factor=0.1, patience=5, verbose=verbosity),
          TerminateOnNaN()
        ]

        try: # try to use whole dataset
          for __ in range(self.params['serializationEpochs']):
            bestModel.fit(trainX, yTrain, validation_split=0.1, epochs=1, batch_size=self.params['batchSize'], 
              callbacks=callbacks, verbose=verbosity)

            bestModel.fit(testX, yTest, validation_split=0.1, epochs=1, batch_size=self.params['batchSize'], 
              callbacks=callbacks, verbose=verbosity)

          bestModel.save(serializePath + modelName + '.h5')
          
        except MemoryError as e: # resort to just using test data (1 epoch to prevent catastrophic interference)
          logging.error('Failed to train model on data (MemoryError). Defaulting to using test data over one epoch.')
          bestModel.fit(testX, yTest, validation_split=0.1, epochs=1, batch_size=self.params['batchSize'], 
            callbacks=callbacks, verbose=verbosity)
          bestModel.save(serializePath + modelName + '.h5')

        except:
          logging.error('Failed due to non-memory related bug: %s', modelName)
          raise ValueError('Failed due to non-memory related bug:', modelName)

      if outputFunction != None and outputParams != None:
        outputFunction(*outputParams, [ongoingResults], times)
      # except:
      #   logging.error('Error while running model: %s', modelName)
      backend.clear_session()
      gc.collect()


    return results, times, tokenizer, metadata


  def recordCheckpoints(self, epoch, logs):
    if logs['val_loss'] > self.bestValLoss:
      self.checkpoints += [epoch]
      self.bestValLoss = logs['val_loss']


  def standardFeatures(self, xTrain, yTrain, xTest, yTest, parameters):
    logging.debug('Transforming data (standard preprocessing)...')
    vectorizer = ta.getVectorizer(parameters)
    xTrain = vectorizer.fit_transform(xTrain)
    if xTest is not None:
      xTest = vectorizer.transform(xTest)

    # if parameters['resampling'] != None:
    #   resampler = ta.getResampler(parameters)
    #   xTrain, yTrain = resampler.fit_sample(xTrain, yTrain)
    #   yTrain = pd.DataFrame({ 'y': yTrain }) # undo resampler's conversion from df to array

    k = self.params['vocabSize']
    if k != None and xTrain.shape[1] > k:
      selectKBest = SelectKBest(k=k)
      xTrain = selectKBest.fit_transform(xTrain, yTrain)
      if xTest is not None:
        xTest = selectKBest.transform(xTest)

    denseTransformer = DenseTransformer()
    xTrain = denseTransformer.fit_transform(xTrain)
    self.params['standardVocabSize'] = xTrain.shape[1]
    if xTest is not None:
      xTest = denseTransformer.transform(xTest)

    if parameters['scaling'] == 'std':
      scaler = StandardScaler(with_mean=False)
      xTrain = scaler.fit_transform(xTrain)
      if xTest is not None:
        xTest = scaler.transform(xTest)

    xTrain = xTrain.reshape(-1, 1, xTrain.shape[1])
    yTrain = yTrain.values.reshape(-1, 1, yTrain.shape[1])
    if xTest is not None:
      xTest  = xTest.reshape(-1, 1, xTest.shape[1])
    # yTest = yTest.values.reshape(-1, 1, yTest.shape[1]) # no reshape needed
    return xTrain, yTrain, xTest, yTest


  def embeddingFeatures(self, xTrain, yTrain, xTest, yTest, parameters, tokenizer):
    logging.debug('Transforming data (keras tokenization)...')
    # if parameters['resampling'] != None:
    #   resampler = ta.getResampler(parameters)
    #   xTrain, yTrain = resampler.fit_sample(xTrain, yTrain)
    #   yTrain = pd.DataFrame({ 'y': yTrain }) # undo resampler's conversion from df to array
    #   # yTrain = yTrain.values.reshape(-1, 1, yTrain.shape[1])
    
    if tokenizer == None:
      tokenizer = Tokenizer(oov_token='UNKNOWN_TOKEN')
    
    tokenizer.fit_on_texts(xTrain)
    xTrain = tokenizer.texts_to_sequences(xTrain)
    xTrain = pad_sequences(xTrain)
    if xTest is not None:
      xTest = tokenizer.texts_to_sequences(xTest)
      xTest = pad_sequences(xTest)
    maxlen = xTrain.shape[1]
    
    self.params['embeddingVocabSize'] = len(tokenizer.word_index)
    self.params['embeddingVocab'] = tokenizer.word_index
    return xTrain, yTrain, xTest, yTest, tokenizer, maxlen


  def getModel(self, parameters, modelParameters, architecture):
    '''Create and return a Keras model'''
    ta.validateParameters(parameters, c.parameters)
    self.validateParameters(modelParameters)

    model = Sequential()
    model, usesEmbedding = self.applyEmbedding(modelParameters, model)
    for layer in architecture(self.params):
      model.add(layer)

    # if 'lstm' in model.layers[-1].name.lower():
    #   model.add(TimeDistributed(Activation('softmax')))
    model.add(Dense(self.params['numClasses'], activation='softmax'))

    model.compile(loss=self.params['loss'], optimizer=self.params['optimizer'], metrics=self.params['metrics'])
    return model, usesEmbedding


  def applyEmbedding(self, parameters, model):
    layer = None
    dimension = 100
    usesEmbedding = False
    if parameters['embedding'][0] != 'no-embedding':
      usesEmbedding = True
      dimension = int(parameters['embedding'][0].split('-')[0])
      path = parameters['embedding'][1]

      # Read embeddings_index
      embeddings_index = {}
      with open(path, encoding='utf-8') as f:
        for line in f:
          word, coefs = line.split(maxsplit=1)
          coefs = np.fromstring(coefs, 'f', sep=' ')
          embeddings_index[word] = coefs

      # Create weights matrix
      embedding_matrix = np.zeros((self.params['embeddingVocabSize']+1, dimension))
      for word, i in self.params['embeddingVocab'].items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
          embedding_matrix[i] = embedding_vector

      # Apply embedding
      layer = Embedding(self.params['embeddingVocabSize']+1, dimension, \
        embeddings_initializer=Constant(embedding_matrix), mask_zero=True)
      layer.trainable = True
      
      self.params['shape'] = (None, self.params['embeddingVocabSize']+1, dimension)
      self.params['seq_shape'] = (None, self.params['embeddingVocabSize']+1, 1, dimension)
      model.add(layer)
    return model, usesEmbedding


  def validateParameters(self, parameters):
    '''Ensure model parameters have expected values'''
    for key in ['numEpochs', 'batchSize']:
      if key in parameters.keys():
        if not isinstance(key, int):
          logging.exception('Unexpected type for parameter %s (should be int):', str(key), str(parameters[key]))
          raise ValueError('Unexpected type for parameter ', key,  '(should be int):', parameters[key])
        self.params[key] = parameters[key]

    for key in ['loss', 'optimizer']:
      if key in parameters.keys():
        if not isinstance(key, str):
          logging.exception('Unexpected type for parameter %s (should be string):', str(key), str(parameters[key]))
          raise ValueError('Unexpected type for parameter ', key,  '(should be string):', parameters[key])
        self.params[key] = parameters[key]


  def createPlot(self, history, numEpochs, checkpoints, title, plotDir):
    '''Creates a plot to illustrate training metrics'''
    plt.style.use("ggplot")
    plt.figure()
    for key in history.history.keys():
      values = history.history[key]
      if key == 'lr': # scale learning rate to be visible on plot
        minimum = min(values)
        values = (values - minimum) / (max(values) - minimum)
        key = 'lr (scaled)'

      if isinstance(values, list):
        values = np.array(values)

      # print(values.shape)
      # print(np.arange(0, numEpochs+1))
      plt.plot(np.arange(0, numEpochs+1), values, label=key)
    
    plt.axis([0, numEpochs, 0, 3])
    plt.title(title)
    for checkpoint in checkpoints:
      plt.axvline(x=checkpoint)
    plt.xlabel("Epoch #")
    plt.ylabel("Metrics")
    plt.legend()
    plt.savefig(plotDir + title + '.png')