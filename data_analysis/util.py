'''
@author: Christian O'Leary
'''
import os, datetime, re, json, traceback, shutil, logging
import _pickle as cPickle
from joblib import dump, load
from time import time

from textblob import TextBlob
import data_analysis.image_analysis as ia
import data_analysis.text_analysis as ta
from data_analysis.text_analysis import TokenizerReducer
# import data_analysis.modelPipeLex as mplex
import data_analysis.constants as c

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer

import pandas as pd
import numpy as np
from google.cloud import datastore


defaultParameters = c.configurations['default']
defaultSignals = ['no_signal', 'offensive', 'gambling', 'substance_abuse', 'academic_misconduct', 'depressive']


def classifyTweet(model, tweets, tokenizer=None, maxlen=150, encodings=None, defaultEncoding=False):
  '''
  Uses a given model to output probability scores for some text.
  Outputs: probabilities (a list of labels), classifications (a list of dictionaries)
  Example use:
    1. with open('models/other.pkl', 'rb') as fid:
    2.   model = cPickle.load(fid)
    3. 
    4. tweets = [ 'a tweet', 'another tweet' ]
    5. probabilities, classification = classifyTweet(model, tweets)
  '''
  if isinstance(model, tuple):
    message = 'A tuple was passed to data_analysis.util.classifyTweet() instead of a model'
    logging.exception(message)
    raise ValueError(message)
  
  if not isinstance(tweets, list):
    logging.exception('Variable "tweets" is of type %s instead of list', str(type(tweets)))
    raise ValueError('Variable "tweets" is of type ', type(tweets), ' instead of list')
  
  if len(tweets) < 1:
    message = 'Variable "tweets" is an empty list'
    logging.exception(message)
    raise ValueError('Variable "tweets" is an empty list')
  
  for i in range(len(tweets)):
    if not isinstance(tweets[i], str):
      logging.exception('Variable "tweets" should only contain strings. It contains %s at index %d', \
        str(type(tweets[i])), i)
      raise ValueError('Variable "tweets" should only contain strings. It contains ', type(tweets[i]), ' at index ', i)

  if tokenizer == None: # sklearn model
    classes = model.classes_
    try:
      int(classes[0])
      defaultEncoding = True
    except:
      defaultEncoding = False

    if defaultEncoding:
      classes = [c.reverseEncoding[int(key)] for key in classes]

    probabilities = model.predict_proba(tweets).tolist()
    probabilities = [dict(zip(classes, probabilities[i])) for i in range(len(probabilities))]

    classifications = model.predict(tweets).tolist()
    if defaultEncoding:
      classifications = [c.reverseEncoding[int(label)] for label in classifications]
 
  else: # keras model
    tweets = tokenizer.texts_to_sequences(tweets)
    tweets = pad_sequences(tweets, maxlen=maxlen)

    if encodings == None:
      message = 'Error: Keras models need a specified encoding to interpret results (encoding was None).'
      logging.exception(message)
      raise ValueError(message)
    probabilities = model.predict(tweets).tolist()
    probabilities = { encodings[i]: probabilities[i] for i in range(len(probabilities)) }
    
    classifications = model.predict_classes(tweets).tolist()
    if defaultEncoding:
      classifications = [c.reverseEncoding[int(label)] for label in classifications]
    
  return probabilities, classifications


def loadAndRunModel(modelPath, tokenizerPath=None, metadataPath=None):
  start = time()
  tokenizer = None
  maxlen = 150
  encodings = None
  if '.joblib' in modelPath:
    model = load(modelPath)

  elif '.pkl' in modelPath:
    with open(modelPath, 'rb') as fid:
      model = cPickle.load(fid)
      
  else:
    model = load_model(modelPath)
    with open(tokenizerPath, 'rb') as fid:
      tokenizer = cPickle.load(fid)
    maxlen, encodings = parseMetadata(metadataPath)
  logging.info('model loaded in %.2f', time() - start)

  txt = ''
  while txt != 'exit':
    txt = str(input('Type a tweet for classification (or "exit" to exit). Please type in lower case:\n'))
    if txt != 'exit':
      start = time()
      probabilities, classification = classifyTweet(model, [txt], tokenizer, maxlen, encodings)
      print('Inference took:', time() - start)
      print('CLASSIFICATION:', classification, '\n')
      print('PROBABILITIES:', probabilities, '\n\n\n')
  print('Exiting...')


def parseMetadata(metadataPath='models/metadata.txt'):
  '''Parse a metadata file produced during training of keras text models. The first line should contain the embedding
  max length. Subsequent lines contain class encodings.'''
  with open(metadataPath, 'r') as f:
    lines = f.readlines()
    maxlen = int(lines[0].strip())
    encodings = {}

    for i in range(1, len(lines)):
      lineParts = lines[i].split(':')
      signal = int(lineParts[0].strip())
      encoding = lineParts[1].strip()
      encodings[signal] = encoding
    return maxlen, encodings


def labelTweets(path, sklearnModelPaths, kerasModelPaths, tokenizerPath, metadataPath, parameters):
  sklearnModels = {}
  kerasModels = {}
  tokenizer = None

  start = time()
  for modelPath in sklearnModelPaths:
    modelName = modelPath.split('.')[0].split('/')[-1]
    if '.joblib' in modelPath:
      model = load(modelPath)
    elif '.pkl' in modelPath:
      with open(modelPath, 'rb') as fid:
        model = cPickle.load(fid)
    sklearnModels[modelName] = model
  
  for modelPath in kerasModelPaths:
    model = load_model(modelPath)
    modelName = modelPath.split('.')[0].split('/')[-1]
    kerasModels[modelName] = model
    if tokenizer == None:
      with open(tokenizerPath, 'rb') as fid:
        tokenizer = cPickle.load(fid)

  maxlen, encodings = parseMetadata(metadataPath)

  logging.debug('Took %.2f to load models', time() - start)
  numModels = len(sklearnModels) + len(kerasModels)

  cleanStart = time()
  initDf = pd.read_csv(path, encoding='ISO-8859-1')
  initDf.columns = ['X']
  initDf = initDf.dropna()
  cleanDf = ta.cleanText(initDf, parameters)
  tweets = cleanDf['X'].values
  logging.debug('Took %.2f to clean data', time() - cleanStart)

  formatStart = time()
  textDf = ta.cleanText(initDf, c.configurations['readable'])
  df = pd.DataFrame()
  df['text'] = textDf[textDf.columns[0]]
  logging.debug('Took %.2f to format text for readability', time() - formatStart)

  inferenceStart = time()
  for name, model in sklearnModels.items():
    modelTime = time()
    predictions = model.predict(tweets)
    results = np.array([encodings[int(p)] for p in predictions])
    logging.debug('%s took %.2f to for inference', name, time() - modelTime)
    saveLabels(path, df, name, results, numModels)
  
  if len(kerasModels) > 0:
    tweets = tokenizer.texts_to_sequences(tweets)
    tweets = pad_sequences(tweets, maxlen=maxlen)
    for name, model in kerasModels.items():
      modelTime = time()
      predictions = model.predict_classes(tweets)
      results = np.array([encodings[int(p)] for p in predictions])
      logging.debug('%s took %.2f to for inference', name, time() - modelTime)
      saveLabels(path, df, name, results, numModels)
  logging.debug('Took %.2f to run inferences for %d models and save labels', time() - formatStart, numModels)


def saveLabels(path, df, modelName, results, numModels):
  '''Save labels applied to unlabelled tweets along with class frequencies'''
  df[modelName] = results
  modelCols = [col for col in df.columns if col not in ['label', 'score', 'text']]
  labels = []
  scores = []
  for row in df[modelCols].values:
    signals, counts = np.unique(row, return_counts=True)
    score = len(modelCols)
    if signals.shape[0] > 1:
      freqs = np.asarray((signals, counts)).T # signal frequencies
      freqs[freqs[:,1].argsort()[::-1]] # sort by signal frequencies
      freqs = freqs[np.all(freqs != 'No Signal', axis=1), :] # drop No Signal
      signals = [freqs[0][0]]
      score = freqs[0][1]

    labels.append(signals[0])
    scores.append(score)
      
  df['label'] = labels
  df['score'] = scores
  cols = list(df.columns) # for reordering columns
  cols.remove('label')
  cols.remove('score')
  cols.append('label')
  cols.append('score')
  df = df[cols]

  logging.debug('DF SHAPE: %s', str(df.shape))
  name = path.split('.')[0].split('/')[1]
  df.to_csv('results/' + name + '.csv', index=False)
  
  frequencies = df['label'].value_counts()
  frequencies.to_csv('results/frequencies.csv', index=True)
  logging.debug('CLASS FREQUENCIES:')
  logging.debug('\t'+ frequencies.to_string().replace('\n', '\n\t'))

  df = df[df['score'] > 1]
  df.to_csv('results/' + name + '-with-agreement.csv', index=False)

  frequencies = df['label'].value_counts()
  frequencies.to_csv('results/frequencies-with-agreement.csv', index=True)
  logging.debug('CLASS FREQUENCIES (with agreement):')
  logging.debug('\t'+ frequencies.to_string().replace('\n', '\n\t'))


def splitFormatRawTweets(path='text/amt/raw-text-v3.csv'):
  df = pd.read_csv(path)
  logging.info('Initial shape: %s', str(df.shape))
  df['text'] = df['text'].apply(lambda tweet : stripAnnotation(tweet))
  df = df.replace(' ', np.nan)
  df = df.dropna().drop_duplicates()
  logging.info('Final shape', str(df.shape))
  df.to_csv('text/amt/raw-text.csv', index=False)


def stripAnnotation(tweet):
  tweet = ' '.join(tweet.split()).strip() # convert all whitespace to space and strip
  if len(tweet) >= 2 and tweet[:2] == 'RT': # strip retweet annotation
    tweet = tweet[:2]
  if len(tweet) >= 5 and tweet[-5:] == '"text': # strip trailing text annotation
    tweet = tweet[:-5]
  tweet = ' '.join(tweet.split()).strip() # format/strip again
  return tweet.strip()
  

def formatTextData(inputPath='csv/mvp-test.csv', outputPath='csv/test.csv'):
  '''
  Used on malformed test data file.
  1. Remove new-line characters. Replace commas with spaces.
  2. Join lines with no seperator
  3. Add commas before labels and newline after labels
  4. For line with missing text, add label to previous line
  5. Write to results to CSV file
  6. Read into pandas dataframe and write from dataframe to CSV again (solves excel formatting issue)
  '''
  with open(inputPath, encoding="utf8") as inputFile:
    content = inputFile.read().splitlines()
    # Strip newline characters and commas
    lines = [
      line.replace('\n','').replace('\r','').replace(',',' ').strip() 
      for line in content if len(re.sub(c.punctuationPattern, '', line).strip()) > 0
    ]
    lines[0] = lines[0].replace(' ', ',').strip() + '\n' # reformat first line
    content = ''.join(lines)

    # add comma before labels and \n after labels
    labels = ['offensive', 'depressive', 'substance_abuse', 'gambling', 'academic_misconduct', 'no_signal']
    for label in labels:
      content = content.replace(label, ',' + label + '\n').strip()

    # deal with missing 'text caused by multiple labels in a line.
    lines = content.split('\n')
    for i in range(len(lines)):
      if i > 1 and lines[i].strip()[0] == ',': # lines missing text
        lines[i-1] = lines[i-1].replace(',', '') + lines[i] # add label to previous line
        lines[i] = '' # such rows are dropped later from dataframe
    lines[0] += '\n'
    content = '\n'.join(lines)

    # Write to csv file
    with open(outputPath, 'w', encoding="utf-8") as outputFile:
      outputFile.write(content)

    # Corrects formatting for excel
    df = pd.read_csv('csv/test.csv', encoding='utf-8', quotechar=None, quoting=3)
    df = df[df.text.map(len) > 0] # drop rows with missing text
    df.to_csv('csv/test.csv', index=False)


def fetchImagesFromGcp(PROJECT_ID, KIND, outputFile):
  logging.info('Fetching from GCP. Project ID: %s Kind: %s', PROJECT_ID, KIND)
  client = datastore.Client(PROJECT_ID)
  query = client.query(kind=KIND)
  results = list(query.fetch())
  if len(results) == 0:
    logging.exception('No results for %s in %s', str(KIND), str(PROJECT_ID))
    raise ValueError('No results for', KIND, 'in', PROJECT_ID)

  allResults = []
  dropped = 0

  logging.debug('Parsing data...')
  for entity in results:
    path = str(entity['file'])
    if path.lower().split('.')[-1] not in ['jpg', 'jpeg', 'gif', 'png', 'webp', 'bmp', 'svg']:
      logging.debug('Ignoring file:', path)
      response = ''
    else:
      response = str(entity['response'])
      response = formatResponse(response)
      try:
        response = json.loads(response)
      except:
        # trace = traceback.format_exc()
        response = ''

    if len(path) > 0 and len(response) > 0:
      allResults.append([path, response])
    else:
      dropped += 1
  logging.info('Dropped', dropped, 'entities with missing values. %d remaining', len(allResults))
  
  df = pd.DataFrame(allResults, columns=['file', 'response'])
  df.to_csv(outputFile, index=False)


def formatResponse(response):
  response = response.replace('\\n', ' ') # remove 'newlines'
  response = response.replace('\n', ' ') # remove 'newlines'
  response = response.replace('\r', ' ') # remove 'newlines'
  
  response = response.replace("Entity('Images',)", '') # remove Entity references
  response = response.replace("Entity('Images2',)", '') # remove Entity references
  response = response.replace("Entity('Images3',)", '') # remove Entity references
  response = re.sub(r'[<>\s]', ' ', response) # remove '<', '>' and spaces
  
  response = response.replace("'", '"') # replace single quotes with double quotes
  
  # Deal with double quotes in text sections
  response = re.sub(r'([A-Za-z0-9!.()])[\s]*"[\s]*([A-Za-z0-9!.()])', r"\1\2", response)
  response = re.sub(r'([A-Za-z0-9])"([A-Za-z0-9])', r"\1\2", response)
  response = re.sub(r'([A-Za-z0-9])[\s]*"[\s]*([A-Za-z0-9])', r"\1\2", response)
  # response = re.sub(r'["]+[\s]*["]+', r'', response)
  # response = re.sub(r'([A-Za-z0-9]+[\s]*):[^"]+', r'\1"', response)
  
  response = response.replace('“', '')
  response = response.replace('”', '')

  colors = ['blue', 'red', 'green', 'pixel_fraction', 'score']
  for color in colors:
    response = response.replace(color, ','+color) # add missing commas into color sections
    
  response = re.sub(r'[\s]*,', ',', response) # remove extra commas
  response = re.sub(r'{,', '{', response) # remove extra commas
  response = response.replace('}color', '},color') # more missing commas

  response = response.strip()
  response = re.sub('[\s]+', ' ', response) # translate multiple white spaces to one space
  
  repsonse = response.replace('blur,red', 'blurred') # fix incorrectly split word
  response = re.sub('blur,red', 'blurred', response)
  return response


def createInputFiles(checkSpelling, inputPath, outputPath, textOutputPath, labelFiles, SIGNALS=defaultSignals):
  logging.debug('Formatting response (%s)...', inputPath)
  startTime = time()
  data = pd.read_csv(inputPath).values.tolist()
  logging.debug('# vision API entries: %d', len(data))

  facialFeatures = sorted(['joy', 'sorrow', 'anger', 'surprise', 'underexposed', 'blurred', 'headwear', 
    'detectionConfidence'])
  safeSearchFeatures = sorted(['violence', 'adult', 'spoof', 'medical', 'racy'])
  likelihoodScores = { 'UNKNOWN': 0, 'VERY_UNLIKELY': 1, 'UNLIKELY': 2, 'POSSIBLE': 3, 'LIKELY': 4, 'VERY_LIKELY': 5 }

  logging.debug('Finding signals...')
  signals = parsePaths(data, labelFiles, SIGNALS)

  logging.debug('Parsing response data...')
  responseData = parseResponseSection(data, facialFeatures, safeSearchFeatures, likelihoodScores, \
    checkSpelling=checkSpelling)
  for i in range(len(signals)):
    responseData[i].append(signals[i])

  columns = facialFeatures + ['text']
  columns = columns + safeSearchFeatures
  columns = columns + ['signal']
  
  df = pd.DataFrame(responseData, columns=columns)
  # df.to_csv(outputPath, index=False)
  logging.debug('# successfully formatted entries: %s', str(df.shape[0]))
  logging.debug(df['signal'].value_counts())

  # df.to_csv(textOutputPath, columns=['text', 'signal'], index=False, header=False)
  logging.debug('Took %d seconds', time() - startTime)


def parsePaths(data, labelFiles, SIGNALS):
  signals = []
  failCount = 0

  for i in range(len(data)):
    path = data[i][0]
    signal = parsePath(path, labelFiles, SIGNALS)
    signals.append(signal)
    if signal == 'unknown':
      failCount += 1

    if i % 100 == 0:
      logging.debug('%d done', i)

  logging.warning('Failed to identify signals for %d entities', failCount)
  return signals


def parsePath(path, labelFiles, SIGNALS):
  signal = getSignalFromFiles(labelFiles, path, SIGNALS) # search for path in files
  if signal == None:
    signal = getSignalFromPath(path, SIGNALS) # search for signal in path
      
  if signal == None:
    # raise ValueError('No signal found in path:', path)
    signal = 'unknown'
  return signal


def getSignalFromFiles(labelFiles, path, SIGNALS):
  path = path.lower()
  signal = None
  seperators = ['/', '\\', '\\\\']
  for seperator in seperators:
    fileNameParts = path.lower().split(seperator)
    if len(fileNameParts) > 1: # successful split
      break
  
  matches = []
  for labelFile in labelFiles:
    df = labelFile[0]
    pathCol = labelFile[1]
    signalCol = labelFile[2]
    df[pathCol] = df[pathCol].apply(lambda s : s.lower())    

    for i in range(len(fileNameParts)):
      for seperator in ['/', '\\\\', '\\\\\\\\']:
        fileName = seperator.join(fileNameParts[i:])
        fileName = fileName.replace('-~seperator~-', seperator)

        rows = df[df[pathCol].str.contains(fileName)]
        if rows.shape[0] > 0:
          signal = str(rows[signalCol].mode().iloc[0]).lower()
          matches.append(signal)
          break
      if signal != None: break # filename parts
    if len(set(matches)) > 1:
      signal = max(set(matches), key=matches.count) # mode
      # raise ValueError('Found conflicting signals from different files')
  return signal


def getSignalFromPath(path, SIGNALS):
  path = path.lower()
  alternatives = {
    'academic_misconduct': ['academic misconduct', 'acaemic misconduct', 'academic-misconduct'],
    'substance_abuse': ['substance abuse', 'substance-abuse'],
    'no_signal': ['no signal', 'no-signal'],
    'offensive': ['aggressive', 'oppressive', 'sexually inappropriate', 'sexualy inappropriate'],
  }
  for signal in alternatives.keys():
    for alternativeSignal in alternatives[signal]:
      path = path.replace(alternativeSignal, signal)
  
  signal = None
  matches = []
  for label in SIGNALS:
    if str(label).lower() in path:
      matches.append(label)
      signal = label
  
  if len(set(matches)) > 1:
    signal = max(set(matches), key=matches.count) # mode
    # raise ValueError('Found conflicting signals from different files')
  return signal


def parseResponseSection(data, facialFeatures, safeSearchFeatures, likelihoodScores, checkSpelling=False):
  '''Parses the "response" section of a GCP entity'''
  imageResults = []
  SECTIONS = ['face', 'image', 'label', 'landmarks', 'localised', 'logos', 'safeSearch', 'text', 'web']
  LABELS = { 'UNKNOWN': 0, 'VERY_UNLIKELY': 1, 'UNLIKELY': 2, 'POSSIBLE': 3, 'LIKELY': 4, 'VERY_LIKELY': 5 }
  if checkSpelling == True:
    logging.debug('Checking spelling')
  else:
    logging.debug('Not checking spelling')

  for i in range(len(data)):
    response = data[i][1]
    responseJson = response.replace("'", '"')
    responseJson = re.sub(r'([A-Za-z0-9!.()])[\s]*"[\s]*([A-Za-z0-9!.()])', r"\1\2", responseJson)
    responseJson = re.sub(r'([A-Za-z0-9])"([A-Za-z0-9])', r"\1\2", responseJson)
    responseJson = re.sub(r'([A-Za-z0-9])[\s]*"[\s]*([A-Za-z0-9])', r"\1\2", responseJson)
    response = json.loads(responseJson)
    text = ''

    for section in SECTIONS:
      if section == 'face':
        facialScores = getFacialScores(response, section, facialFeatures, LABELS)

      elif section == 'image':
        pass

      elif section in ['label', 'text', 'web', 'landmarks', 'logos']:
        for key in response[section].keys():
          text += key + ' '

      elif section == 'localised':
        if len(response[section].keys()) > 0:
          logging.error('First time encountering data in "localised" section of GCP Entity.')
          logging.error('Please revise function parseResponseSection() in util.py')
      
      elif section == 'safeSearch':
        safeSearchScores = getSafeSearchScores(response, section, safeSearchFeatures, LABELS)
      
    if checkSpelling == True:
      text = str(TextBlob(text).correct())
    results = [facialScores[key] for key in sorted(facialScores.keys())] + [text]
    results = results + [safeSearchScores[key] for key in sorted(safeSearchScores.keys())]
    imageResults.append(results)

  return imageResults


def getFacialScores(response, sectionName, features, LABELS):
  scores = dict(zip(features, [0]*len(features))) # initial scores = 0
  for key in response[sectionName].keys():
    for category in response[sectionName][key]:
      likelihood = response[sectionName][key][category]
      if likelihood in LABELS.keys():
        scores[category] += LABELS[likelihood]
      else:
        scores[category] = likelihood # number instead of label
  return scores


def getSafeSearchScores(response, sectionName, features, LABELS):
  scores = dict(zip(features, [0]*len(features))) # initial scores = 0
  for category in response[sectionName].keys():
    likelihood = response[sectionName][category]
    if likelihood in LABELS.keys():
      scores[category] += LABELS[likelihood]
    else:
      scores[category] = likelihood # number instead of label
  return scores


def generateCsvFromImageNames(imageDirPath, outputPath, seperator):
  '''Creates a CSV file mapping image files to classifications'''
  paths = []
  signals = []
  positiveSignals = ['amusement', 'excitement', 'contentment', 'awe']
  negativeSignals = ['sad', 'fear', 'disgust', 'anger']
  for filename in os.listdir(imageDirPath):
    fileParts = filename.split(seperator)
    emotion = fileParts[0]
    if emotion in positiveSignals:
      emotion = 'positive'
    elif emotion in negativeSignals:
      emotion = 'negative'
    
    if emotion in ['positive', 'negative']:
      paths.append(filename)
      signals.append(emotion)

  df = pd.DataFrame({'path': paths, 'signal': signals})
  df.to_csv(outputPath, index=False)
  

def retrievePanderaImages():
  '''Walk through the original Pandera image directory and copy the image files into one directory, handling name 
  conflicts'''
  # Create an empty dir for all images
  dirCount = 0
  outputDir = 'images/pandera/'
  while os.path.exists(outputDir + str(dirCount)):
    dirCount += 1
  outputDir = outputDir + str(dirCount) + '/'
  os.makedirs(outputDir)

  # copy images to new image dir
  for subdir, dirs, files in os.walk('images/pandera/images/'):
    for filename in files:
      path = os.path.join(subdir, filename)
      filename = subdir + '-~SEPERATOR~-' + filename
      filename = filename.replace('images/pandera/images/', '')

      if filename.lower().split('.')[-1] in ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'webp']:
        while os.path.exists(outputDir + filename):
          filename = 'CONFLICT-' + filename
        shutil.copy(path, outputDir + filename)
      else:
        logging.debug('ignoring', path)
  