# -*- coding: utf-8 -*-

import warnings, sys, os, configparser
config = configparser.ConfigParser(inline_comment_prefixes='#')
config.read('settings.cfg')
if os.path.exists(config.get('ALT_CONFIG', 'alt_path')):
  config.read('alt_settings.cfg') # config file ignored by git

os.environ['TF_CPP_MIN_LOG_LEVEL'] = config['LOG']['tf_level']
if not sys.warnoptions:
  warnings.simplefilter("ignore")
  os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses

from time import time
from datetime import datetime
startTime = time()
import random, logging
from data_analysis.evaluator import Evaluator
import data_analysis.constants as c
import data_analysis.text_analysis as ta
import data_analysis.util as util
import numpy as np
import pandas as pd
import nltk
nltk.download('stopwords', quiet=True)
nltk.download('wordnet', quiet=True)

importTime = time() - startTime

def main(config=config):
  startTime = datetime.now().strftime("%d-%m-%y %H-%M-%S")
  log_path = config.get('LOG', 'log_path')
  if not os.path.exists(log_path):
      path = ''
      for part in log_path.split('/'):
        path += part + '/'
        if '.' not in path and not os.path.exists(path):
          os.makedirs(path)
  
  logging.getLogger('matplotlib.font_manager').disabled = True
  logging.getLogger('matplotlib').disabled = True
  logging.basicConfig(filename=log_path, filemode='a', level=config.get('LOG', 'level'),
                      format='%(levelname)s:  %(message)s')
  logging.info('===============================================================\n')
  logging.info('Started logging at %s', datetime.now().strftime("%d-%m-%y %H:%M:%S"))
  logging.debug('Configuration:\n')
  if logging.getLogger().getEffectiveLevel() < 20:
    with open(log_path, 'a') as fid:
      for section in config.sections():
        fid.write('-'*35 + '\n' + section.rjust(15) + '\n' + '-'*35 + '\n')
        for (key, value) in config.items(section):
          fid.write(key.rjust(25) + ': ' + value + '\n')
      fid.write('-'*35 + '\n')
  option = int(config.get('SCRIPT_OPTIONS', 'option'))
  numProcesses = int(config.get('SCRIPT_OPTIONS', 'num_processes'))
  seed = int(config.get('SCRIPT_OPTIONS', 'seed'))
  iterations = int(config.get('SCRIPT_OPTIONS', 'iterations'))
  parameters = c.configurations[config.get('SCRIPT_OPTIONS', 'default_parameters')]
  text_datasets = config.get('SCRIPT_OPTIONS', 'text_datasets').split(',')
  image_datasets = config.get('SCRIPT_OPTIONS', 'image_datasets').split(',')
  text_model_path = config.get('PATHS', 'text_model_path')
  image_model_path = config.get('PATHS', 'image_model_path')
  unlabelled_tweets_path = config.get('PATHS', 'unlabelled_tweets')

  logging.info('Imported libraries (%s) seconds', str(round(importTime, 2)))

  skip_prompt = config.get('SCRIPT_OPTIONS', 'skip_prompt')
  if skip_prompt == 'true' or skip_prompt == 'True': 
    skip_prompt = True
  
  argsPresent = True
  try:
    int(sys.argv[1])
  except:
    argsPresent = False

  if argsPresent and len(sys.argv) > 1 and int(sys.argv[1]) >= 0:
    option = int(sys.argv[1])
    config.set('SCRIPT_OPTIONS', 'option', str(option))
    skip_prompt = True
    if len(sys.argv) > 2 and int(sys.argv[2]) > 0:
      numProcesses = int(sys.argv[2])
      config.set('SCRIPT_OPTIONS', 'num_processes', str(numProcesses))

  if skip_prompt != True:
    response = input("""Please select an operation to run:
      (0) Run default model without lexicons
      (1) Run default model with lexicons
      (2) Run default model without lexicons on image (v-api text) data
      (3) Run through different preprocessing steps
      (4) Run through randomized preprocessing steps
      (5) Model selection for text models
      (6) Serialise scikit-learn text models
      (7) Serialise scikit-learn Vision API text-only models
      (8) Provide signals for unlabelled tweets
      (9) Serialise keras text models
     (10) Serialise keras image models
     (11) Run ensemble model for vision API text and other features
     (12) Run ensemble model selection for vision API
     (13) Fetch vision API results from GCP
     (14) Transfer learning for images
     (15) Transfer learning for text
     (16) Load model and run on cli
     (17) Quit program
     Enter number: """)
    if int(response) >= 0 and int(response) <= 17:
      option = int(response)
    else:
      logging.error('Invalid option selected. Expected 1-17. Exiting')
      sys.exit()

  evaluator = Evaluator()

  if option >= 0 and option < 3: # MODEL ONCE
    if option == 1:
      parameters['useLexicons'] = True

    inputTrainPath = 'text/amt.csv'
    if option == 2:
      inputTrainPath = c.imageCsvDirs['pandera'] + 'text.csv'
    inputTestPath = None

    modelName = 'default'
    logging.debug('Using model: %s', modelName)
    searchParams = { modelName: c.gridSearch[modelName] }

    startTime = time()
    results, originalLabels, rowCounts = evaluator.cleanAndModel(config, parameters, searchParams, inputTrainPath, 
      inputTestPath, 'grid', numProcesses, seed=seed, labelsToDrop=['14', 'unknown', 'academic_misconduct', 'Academic Misconduct'],)
    scores = results[0][0]
    logging.info('Accuracy:%s', scores[0])
    logging.info('MICRO (P/R/F1): %.4f, %.4f, %.4f', scores[1], scores[2], scores[3])
    logging.info('MACRO (P/R/F1): %.4f, %.4f, %.4f', scores[4], scores[5], scores[6])
    logging.info('WEIGHTED (P/R/F1): %.4f, %.4f, %.4f', scores[7], scores[8], scores[9])
    logging.info('Time: %.2f', time() - startTime)


  elif option == 3: # INDEPENDENT PREPROCESSING STEPS (APPLY ONE STEP AT A TIME)
    logging.info('Running different preprocessing steps for text models')
    excludedKeys = ['spellCheck', 'useLexicons']
    resultsDir = config.get('PATHS', 'preprocessing')

    for dataset in text_datasets:
      inputPath = 'text/' + dataset + '.csv'
      scoresPath = resultsDir + 'independent-' + dataset + '.csv'
      evaluator.runWithIndependentParameters(config, scoresPath, '', inputPath, None, excludedKeys, sklearn=True, 
        iterations=iterations, nproc=numProcesses, seed=seed)


  elif option == 4: # RANDOMIZED PREPROCESSING
    logging.info('Running randomized preprocessing for text models')
    fixedParams = { 'spellCheck': None, 'useLexicons': False, 'stratifyCv': True }
    resultsDir = config.get('PATHS', 'preprocessing')
    
    for dataset in text_datasets:
      inputPath = 'text/' + dataset + '.csv'
      scoresPath = resultsDir + 'randomized-' + dataset + '.csv'
      evaluator.randomizedPreprocessing(config, scoresPath, fixedParams, c.bestModels, inputPath, None, search='random', 
        sklearn=True, nproc=numProcesses, seed=None)


  elif option == 5: # MODEL SELECTION
    logging.info('Model selection for text data')
    startTime = time()
    resultsDir = config.get('PATHS', 'model_selection')

    # (resultsDir + 'twitter-model-selection.csv', c.twitterImageDir + 'text.csv', parameters),
    # (resultsDir + 'art-model-selection.csv', c.artImageDir + 'text.csv', parameters),
    # (resultsDir + 'pandera-model-selection.csv', c.panderaImageDir + 'text.csv', parameters),
    
    for dataset in text_datasets:
      resultPath = resultsDir + dataset + '.csv'
      inputTrainPath = 'text/' + dataset + '.csv'
      params = c.configurations[dataset]
      evaluator.runModelSelection(config, resultPath, inputTrainPath, testData=None, parameters=params, search='random', 
        nproc=numProcesses, numIterations=iterations, seed=seed)#, 
        # labelsToDrop=['14', 'unknown', 'academic_misconduct', 'Academic Misconduct'])

    logging.info('Model Selection time: %.2f', time() - startTime)


  elif option == 6: # SERIALISE SKLEARN TEXT MODELS
    dataset = text_datasets[0]
    if len(text_datasets) > 1:
      logging.warning('More than one dataset specified for training models. Using: %s', dataset)

    inputPath = 'text/' + dataset + '.csv'
    evaluator.serializeSklearnModels(parameters, inputPath=inputPath, outputPath=text_model_path, nproc=numProcesses, 
      seed=seed, bestModel=False)


  elif option == 7: # SERIALISE SKLEARN V. API TEXT-ONLY MODELS
    dataset = image_datasets[0]
    if len(image_datasets) > 1:
      logging.warning('More than one dataset specified for training models. Using: %s', dataset)

    imageDir = c.imageDirs[dataset]
    evaluator.serializeSklearnModels(parameters, inputPath=imageDir + 'text.csv', outputPath=image_model_path, 
      nproc=numProcesses, seed=seed, bestModel=False)
    

  elif option == 8: # LABEL TWEETS
    sklearnModelPaths = [ 
      # text_model_path + 'perceptron.pkl',
      # text_model_path + 'decisionTree.pkl',
      # text_model_path + 'linearSvc.pkl',
    ]
    kerasModelPaths = []
    for embedding, models in c.bestKerasTextModels.items():
      for model in models:
        kerasModelPaths.append(text_model_path + model + '-' + embedding + '.h5')

    tokenizerPath = text_model_path + 'tokenizer.pkl'
    metadataPath = text_model_path + 'metadata.txt'
    util.labelTweets(unlabelled_tweets_path, sklearnModelPaths, kerasModelPaths, tokenizerPath, metadataPath, 
      c.configurations['default'])


  elif option == 9: # SERIALISE KERAS TEXT MODELS
    dataset = text_datasets[0]
    if len(text_datasets) > 1:
      logging.warning('More than one dataset specified for training models. Using: %s', dataset)
    
    evaluator.runBestKerasModels(config, parameters, 'text/' + dataset + '.csv', serializePath='models/text/', 
      nproc=numProcesses, seed=seed)


  elif option == 10: # SERIALISE KERAS IMAGE MODELS
    dataset = image_datasets[0]
    if len(image_datasets) > 1:
      logging.warning('More than one dataset specified for training models. Using: %s', dataset)
    
    logging.error('Called option (10) in scripts that is not yet implemented')
    raise ValueError('Not implemented')


  elif option == 11: # 
    logging.error('Option 11 not implemented')
    raise ValueError('Option 11 not implemented')


  elif option == 12: # ENSEMBLE MODEL SELECTION
    outputPath = config.get('PATHS', 'model_selection')
    startTime = time()
    modelKeys = [key for key in list(c.bestModels.keys()) if key not in ['default', 'dummy']]

    for dataset in image_datasets:
      inputPath = c.imageDirs[dataset] + 'allfeatures.csv'
      outputPath = outputPath + dataset + '-ensemble.csv'
      evaluator.ensembleModelSelection(config, modelKeys, outputPath, inputPath, nproc=numProcesses, 
        numIterations=iterations, seed=seed)

    loggin.info('Took: %.2f seconds', time() - startTime)


  elif option == 13: # DOWNLOAD VISION API DATA AND CREATE AN INPUT FILE
    logging.info('Downloading vision API data from GCP')
    project_id = config.get('GCP', 'project_id')
    kind = config.get('GCP', 'kind')
    logging.debug('ID: %s, Kind: %s', project_id, kind)

    dataset = image_datasets[0]
    if len(image_datasets) > 1:
      logging.warning('More than one dataset specified. Using: %s', dataset)

    vApiFile = c.vApiFiles[dataset]
    util.fetchImagesFromGcp(project_id, kind, vApiFile)
    util.createInputFiles(checkSpelling=False, inputPath=vApiFile, outputPath=c.imageDirs[dataset] + 'allfeatures.csv', 
      textOutputPath=c.imageDirs[dataset] + 'text.csv', labelFiles=c.labelFiles[dataset](), SIGNALS=c.signals[dataset])


  elif option == 14: # FINE TUNING OF IMAGE MODELS
    excludedModels = config.get('SCRIPT_OPTIONS', 'excluded_image_models').split(',')
    outputPath = config.get('PATHS', 'model_selection')
    for dataset in image_datasets:
      logging.info('Running image fine tuning on %s dataset', dataset)
      params = { 
        'imageDir': c.imageDirs[dataset], 
        'labelFiles': c.labelFiles[dataset](), 
        'signals': c.signals[dataset] 
      }
      evaluator.imageFineTuning(config, excludedModels, outputPath, params, iterations=iterations)


  elif option == 15: # FINE TUNING OF TEXT MODELS
    for dataset in text_datasets:
      logging.info('Running text fine tuning on %s dataset', dataset)
      inputPath = 'text/' + dataset + '.csv'
      outputPath = config.get('PATHS', 'model_selection') + dataset + '-keras.csv'
      parameters = c.configurations[dataset]

      evaluator.runKerasModels(config, parameters, inputPath, outputPath, list(parameters.keys()), 
        iterations=iterations, nproc=numProcesses, seed=seed, serializePath=None, 
        # labelsToDrop=['14', 'unknown', 'academic_misconduct', 'Academic Misconduct'],
        plotDir='results/model-selection/ongoing/' + dataset + '/')


  elif option == 16: # LOAD AND RUN A MODEL ON CLI
    model = config.get('SCRIPT_OPTIONS', 'cli_model')
    if model.split('.')[1] == 'h5':
      util.loadAndRunModel(text_model_path + model, text_model_path + 'tokenizer.pkl', text_model_path + 'metadata.txt')
    else:
      util.loadAndRunModel(text_model_path + model)


  elif option == 17:
    logging.warning('Exiting...')
  else:
    raise ValueError('Unknown option', option)

  logging.info('Successfully finished scripts at %s', datetime.now().strftime("%d-%m-%y %H:%M:%S"))
  logging.info('===============================================================\n\n')

if __name__ == '__main__': # required for multiprocessing module
  main()
