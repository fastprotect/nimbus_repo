import logging
from apache_beam.options.pipeline_options import PipelineOptions
import apache_beam as beam
from google.cloud import storage
from sklearn.externals.joblib import dump, load
from google.cloud import vision
from google.cloud.vision import types
from pipeline.unifymodels import UnifyModels
from pipeline.jsonprocessing import ProcessJSONTweetPandera
from pipeline.data import SaveToDataStore,SaveToBigQuery,findAllModels
from pipeline.pipelineoptions import ProcessOptions
from pipeline.model import RunModelText,RunModelImage,Preprocess, ProcessImage
from data_analysis.constants import configurations
import sys
import os
import _pickle as cPickle
import json

# LEAVE THIS, IT IS NOT LOADED WHEN DEPLOYED AS A PIPELINE
import nltk
nltk.download('wordnet')
sys.path.append("."+os.sep+"tokenizer")

PROJECT_ID = "fms-dev-221613"

parameters = configurations['default']

# CALLED BY dataflow() TO GET A MODEL FOR EACH INSTITUTION
def getModels(bucket):
    results=findAllModels()
    models=dict()
    gcs = storage.Client()
    bucket = gcs.get_bucket(bucket)
    for m in results:
        modelPath=str(m["institution_uri"])+"/"+str(m["model"]["version"])+"/"+m["model"]["name"]
#        modelPath="1/"+str(m["model"]["version"])+"/"+m["model"]["name"]
        localPath = str(m["institution_uri"]) + str(m["model"]["version"])  +m["model"]["name"]

## TODO: ERROR CHECKING, MODEL NOT FOUND, REPORT ERROR AND USE DEFAULT?
        try:
            blob = bucket.blob(modelPath)
            blob.download_to_filename(localPath)
            with open(localPath, 'rb') as fid:
                model = cPickle.load(fid)
                models[m["institution_uri"]]=model
                print (modelPath)
        except Exception as e:
            logging.info(e)
    return models

# FOR DEVELOPMENT SO WE DO NOT DOWNLOAD FROM BUCKETS ALL MODELS
def getModelsLocally():
    results = findAllModels()
    models = dict()
    for m in results:
        localPath = str(m["institution_uri"]) + "0.1logisticRegression.pkl"
        try:
            with open(localPath, 'rb') as fid:
                model = cPickle.load(fid)
                models[m["institution_uri"]] = model
                logging.info("LOADED MODEL LOCALLY FOR INST URI "+str(m["institution_uri"]))
        except Exception as e:
            logging.info(e)
    return models

# MAIN DATA FLOW FUNCTION
def dataflow(argv=None):
    process_options = PipelineOptions(setup_file="./setup.py").view_as(ProcessOptions)
    LOCAL_MODEL="logisticRegression.pkl"

    p = beam.Pipeline(options=process_options)

#    models=getModels(process_options.bucket)
    models=getModelsLocally()

#    gcs = storage.Client()
#    bucket = gcs.get_bucket("fastprotectnimbus")
#    blob = bucket.blob("logisticRegression.pkl")
#    blob.download_to_filename(LOCAL_MODEL)
#    with open(LOCAL_MODEL, 'rb') as fid:
#        model = cPickle.load(fid)

#    topic_name="projects/" + PROJECT_ID + "/topics/" + "fast-prep"
    topic_name="projects/" + PROJECT_ID + "/topics/" + process_options.input_topic.default_value
    logging.info(topic_name)
#    logging.info(len(models))
    logging.info("STARTING PIPELINE...")
    (p
#     | '0. Read From Text (for development)' >> beam.io.ReadFromText(process_options.input,  skip_header_lines=0)
     | '1. Read from PubSub '>>beam.io.ReadFromPubSub(topic=topic_name)
 #    | '(OLD) 2. Process Tweets as JSON from row tweet' >> beam.ParDo(ProcessJSONTweet())
     | '2. Process Tweets as JSON from prep' >> beam.ParDo(ProcessJSONTweetPandera())
     | "3. Save to DataStore the tweeted data before processing" >> beam.ParDo(SaveToDataStore(), PROJECT_ID, process_options.entity.default_value)
     | "4. Preprocess">>beam.ParDo(Preprocess(),parameters)
     | "5. Process Image" >> beam.ParDo(ProcessImage())
     | "6. Run Model for Text" >> beam.ParDo(RunModelText(),models)
#     | "7. Run Model for images" >> beam.ParDo(RunModelImage(), models)
#     | "8. Unify Models" >> beam.ParDo(UnifyModels(), model)
    | "9. Save to DataStore after processing" >> beam.ParDo(SaveToDataStore(), PROJECT_ID, process_options.entity.default_value)
     | "10. Save to Big Query" >> beam.ParDo(SaveToBigQuery(), PROJECT_ID, process_options.entity.default_value)
     )

    p.run().wait_until_finish()

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    dataflow()