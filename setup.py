from setuptools import setup, find_packages

setup(
    name='fppipeline1',
    version='0.4.0',
    packages=['data_analysis','tokenizer','pipeline'],
#   packages=find_packages(),
    install_requires=['sklearn',
                      'nltk==3.4.5',
                      'pandas',
                      'pycorenlp' ,
                      'textblob',
                      'bs4',
                      'joblib==0.13.2',
                      'google-cloud-storage',
                      'google-cloud-vision',
                      'google-cloud-bigquery',
                      'tensorflow',
                      'imblearn',
                      'tensorflow',
                      'imblearn',
                      'matplotlib', # pipeline3
                      'lxml'],
    description='Dependencies',
 )
#'sklearn', 'ntlk','pandas','pycorenlp' ,'textblob','bs4'
