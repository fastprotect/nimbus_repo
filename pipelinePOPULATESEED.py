import logging
from apache_beam.options.pipeline_options import PipelineOptions
import apache_beam as beam
from google.cloud import storage
from sklearn.externals.joblib import dump, load
from google.cloud import vision
from google.cloud.vision import types
from pipeline.unifymodels import UnifyModels
from pipeline.jsonprocessing import ConvertToJSON
from pipeline.csvprocessing import ProcessCSV
from pipeline.data import SaveModelDataToDataStore, findAllModels, SaveModelToBucket, ReadInstitutionDataFromDataStore, SaveToDataStore
from pipeline.pipelineoptions import ProcessOptions
from pipeline.model import RunModelText,RunModelImage,Preprocess, ProcessImage, TrainModel,PrepareData
from data_analysis.constants import defaultParameters
import sys
import os
import _pickle as cPickle
import json

# LEAVE THIS, IT IS NOT LOADED WHEN DEPLOYED AS A PIPELINE
import nltk
nltk.download('wordnet')
nltk.download('stopwords')
sys.path.append("."+os.sep+"tokenizer")

PROJECT_ID = "fms-dev-221613"

parameters = defaultParameters


# MAIN DATA FLOW FUNCTION
def dataflow(argv=None):
    process_options = PipelineOptions(setup_file="./setup.py").view_as(ProcessOptions)

    p = beam.Pipeline(options=process_options)

    logging.info("STARTING PIPELINE to populate seed...")
    (p
     | beam.Create([26])
     | 'A. Prepare DAta: development get from CSV' >> beam.io.ReadFromText(process_options.train,  skip_header_lines=0)
     | 'B. Create Seed Data as JSON' >> beam.ParDo(ProcessCSV())
     | "C. Save to DataStore after processing" >> beam.ParDo(SaveToDataStore(), PROJECT_ID,"SeedData")

     )

    p.run().wait_until_finish()

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    dataflow()