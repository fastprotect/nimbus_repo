'''
@author Christian O'Leary
'''
import pytest
from tensorflow.keras import backend
from data_analysis.keras_importer import KerasImporter

# def test_imports():
#   importer = KerasImporter()
#   modelNames = importer.imageModelNames
#   for modelName in modelNames:
#     dimensions, model, preprocess_input, decode_predictions = importer.importImageModel(modelName)
#     example = model()
#     del example
#     backend.clear_session()