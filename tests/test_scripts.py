'''
@author Christian O'Leary
'''
import pytest, configparser, os
from time import time
import pandas as pd

import data_analysis.constants as c
import scripts as s

dataset = 'amt'
c.configurations[dataset] = c.configurations['amt']
c.configurations[dataset]['maxClassSize'] = 200

@pytest.fixture(autouse=True)
def setup():
  '''Setup for tests. Yields default modelling parameters'''
  config = configparser.ConfigParser(inline_comment_prefixes='#')
  config.read('test_settings.cfg')
  config.set('SCRIPT_OPTIONS', 'text_datasets', dataset)
    
  default_parameters = dict(c.configurations['default'])
  minimal_parameters = dict(c.configurations['minimal'])
  
  yield (config, default_parameters, minimal_parameters)


# def test_default_text(setup):
#   '''Test option 0 (run default text model)'''
#   config, parameters, __ = setup
#   config.set('SCRIPT_OPTIONS', 'option', '0')
#   s.main(configuration=config)
#   # TODO: assertions


# def test_default_text_lexicons(setup):
#   '''Test option 1 (run default text model with lexicons)'''
#   config, parameters, __ = setup

# def test_sklearn_text_model_selection(setup):
#   '''Test option 5 (run scikit-learn text model selection)'''
#   config, parameters, __ = setup
#   # TODO: set config
#   exit_status = os.system('python scripts.py 0 5')
#   assert exit_status == 0
#   # TODO: assertions
#   # assert result file has correct number of lines


# def test_exit_prompt(setup):
#   '''Test option 17 (exit prompt)'''
#   exit_status = os.system('python scripts.py 17 1')
#   assert exit_status == 0