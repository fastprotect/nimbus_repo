'''
@author Christian O'Leary
'''
import pytest, configparser
from time import time
import pandas as pd
import data_analysis.constants as c
import data_analysis.text_analysis as ta

@pytest.fixture(autouse=True)
def setup():
  '''Setup for tests. Yields default modelling parameters'''
  default_parameters = dict(c.configurations['default'])
  minimal_parameters = dict(c.configurations['minimal'])
  yield (default_parameters, minimal_parameters)


def test_param_validation(setup):
  '''Tests parameter validation for (1) valid params (2) params with an unexpected key (3) params with an unexpected 
  value for a known key'''
  parameters, __ = setup
  ta.validateParameters(parameters, c.parameters)
  with pytest.raises(ValueError):
    parameters['an illegal key'] = 'placeholder value'
    ta.validateParameters(parameters, c.parameters)
  with pytest.raises(ValueError):
    # maxVocabFeatures is an expected key
    parameters['maxVocabFeatures'] = 'an illegal value'
    ta.validateParameters(parameters, c.parameters)


def test_read_data(setup):
  '''Tests that a valid dataset can be read without any exceptions'''
  parameters, __ = setup
  df, rowCounts = ta.readData('./tests/amt-full.csv', parameters)
  df, rowCounts = ta.readData('./tests/amt-max-100-per-class.csv', parameters)
  df, rowCounts = ta.readData('./tests/amt-max-1000-per-class.csv', parameters)
  df, rowCounts = ta.readData('./tests/amt-max-10000-per-class.csv', parameters)
  assert df.shape[0] > 0 # ensure some rows are present
  assert df.shape[1] == 2 # ensure tweet and signal columns exist


def test_read_invalid_data(setup):
  '''Tests that an invalid dataset raises exceptions'''
  parameters, __ = setup
  with pytest.raises(ValueError): # insufficient class size
    df, rowCounts = ta.readData('./tests/amt-max-1-per-class.csv', parameters)
  with pytest.raises(ValueError): # only one class present
    df, rowCounts = ta.readData('./tests/one-class.csv', parameters)


def test_encode_data(setup):
  '''Tests that datasets are encoded correctly'''
  parameters, __ = setup
  df, rowCounts = ta.readData('./tests/amt-max-100-per-class.csv', parameters)
  df, encoding = ta.encodeData(df)
  assert isinstance(encoding, dict)
  assert len(encoding) == 6 # 6 classes in AMT dataset

  # expectedEncoding = dict(c.labelEncoding)
  # for k, v in encoding.items():
  #   assert k in expectedEncoding.keys()
  #   assert expectedEncoding[k] == v


def test_clean_tweet(setup):
  '''Tests that tweets can be cleaned without raising exceptions'''
  parameters, __ = setup
  df, rowCounts = ta.readData('./tests/amt-max-100-per-class.csv', parameters)
  df, encoding = ta.encodeData(df)
  df = ta.cleanText(df, parameters)


def test_soup(setup):
  __, parameters = setup
  parameters['soup'] = True
  texts = [
    'some test text that should NOT CHANGE',
    'Depression memes &gt;&gt;&gt;&gt;',
    'when you have an asthma attack and you have albuterol at home &lt;&lt;&lt;',
  ]
  expected = [
    'some test text that should NOT CHANGE',
    'Depression memes >>>>',
    'when you have an asthma attack and you have albuterol at home <<<',
  ]
  for i in range(len(texts)):
    assert ta.soup(texts[i], parameters) == expected[i]

  parameters['soup'] = False
  assert ta.soup(texts[0], parameters) == expected[0]
  for i in range(1, len(texts)):
    assert ta.soup(texts[i], parameters) != expected[i]


def test_remove_patterns(setup):
  __, parameters = setup
  texts = [
    'some test text that should NOT CHANGE',
    'Example text with a hashtag: #amt',
    'A tweet with a link: https://example.com/test/index.html',
    '@mention this is a tweet @asdf',
    'This one has a link www.asdf.co.uk and punctuation : ; , \' " ',
  ]
  expected = [
    'some test text that should NOT CHANGE',
    'Example text with a hashtag: #amt',
    'A tweet with a link: https://example.com/test/index.html',
    '@mention this is a tweet @asdf',
    'This one has a link www.asdf.co.uk and punctuation : ; , \' " ',
  ]
  for i in range(0, len(texts)):
    assert ta.removePatterns(texts[i], parameters) == expected[i]

  parameters['removeHashtags'] = True
  expected[1] = 'Example text with a hashtag: '
  for i in range(0, len(texts)):
    assert ta.removePatterns(texts[i], parameters) == expected[i]

  parameters['removeHttp'] = True
  expected[2] = 'A tweet with a link: '
  for i in range(0, len(texts)):
    assert ta.removePatterns(texts[i], parameters) == expected[i]

  parameters['removeMention'] = True
  expected[3] = ' this is a tweet '
  for i in range(0, len(texts)):
    assert ta.removePatterns(texts[i], parameters) == expected[i]

  parameters['removeWww'] = True
  expected[4] =  'This one has a link  and punctuation : ; , \' " '
  for i in range(0, len(texts)):
    assert ta.removePatterns(texts[i], parameters) == expected[i]

  parameters['removePunctuation'] = True
  expected[1] = 'Example text with a hashtag '
  expected[2] = 'A tweet with a link '
  expected[4] =  'This one has a link  and punctuation      '
  for i in range(0, len(texts)):
    assert ta.removePatterns(texts[i], parameters) == expected[i]
  

def test_expand_contractions(setup):
  '''Tests that contractions are expanded'''
  __, parameters = setup
  texts = [
    'some test text that should NOT CHANGE',
    'If I wasnt sad before I am now',
    'so tired of my peers dyingi mean any murder is sad but were so youngmfs just graduating hs n getting killed',
    'RT  Its hard to fix a damaged heart I been trying for years',
    'all Ive been doing is crying for the past two days lol',
    'hey yall',
    'Nah Id kill myself no cap',
    'why you wanna love me so bad havent you heard about my miserable past',
    'And I mean frustrated thats with everything',
    'Everyone Im in hell',
  ]
  expected = [
    'some test text that should NOT CHANGE',
    'If I was not sad before I am now',
    'so tired of my peers dyingi mean any murder is sad but we are so youngmfs just graduating hs n getting killed',
    'RT  It Has / It Is hard to fix a damaged heart I been trying for years',
    'all I have been doing is crying for the past two days lol',
    'hey you all',
    'Nah I had / I would kill myself no cap',
    'why you wanna love me so bad have not you heard about my miserable past',
    'And I mean frustrated that has / that is with everything',
    'Everyone I am in he shall / he will',
  ]
  parameters['expandContractions'] = True
  for i in range(0, len(texts)):
    assert ta.expandContractions(texts[i], parameters) == expected[i]


def test_remove_non_ascii(setup):
  '''Tests that ascii characters are removed from text'''
  __, parameters = setup
  texts = [
    'Finished +3 over final five holes brutal ¬',
    '¨ Betting Teasers Optimal Number of Teams you Should Play to get the Best Odds and _covers answer ¬ ¨à´',
    'If you have any good bets share Ô£øº´° Free shot ',
    '¬Æ Hersh on Hoops is BACK ¬Æ',
    'Oh I need some ß before I read the rest'
    'Hosting a handicapping tournament over the next 2 weekends ¬¬Äö±Ñ¢îàè®25 handicappers invite only',
  ]
  expected = [
    'Finished +3 over final five holes brutal ',
    ' Betting Teasers Optimal Number of Teams you Should Play to get the Best Odds and _covers answer  ',
    'If you have any good bets share  Free shot ',
    ' Hersh on Hoops is BACK ',
    'Oh I need some  before I read the rest'
    'Hosting a handicapping tournament over the next 2 weekends 25 handicappers invite only',
  ]
  parameters['removeNonAscii'] = True
  for i in range(0, len(texts)):
    assert ta.removeNonAscii(texts[i], parameters) == expected[i]


# def test_split_data(setup):
#   '''Test that datasets are split and stratified correctly'''
#   parameters, __ = setup
#   df, rowCounts = ta.readData('./tests/amt-max-1000-per-class.csv', parameters)
#   df, encoding = ta.encodeData(df)
#   df = ta.cleanText(df, parameters)
#   split = 0.2
#   tolerance = 0.001
#   # Split of 0.2 without stratification
#   xTrain, xTest, yTrain, yTest = ta.splitData(df, seed=321, testSize=split)
#   assert isinstance(xTrain, pd.Series)
#   assert isinstance(xTest, pd.Series)
#   assert isinstance(yTrain, pd.DataFrame)
#   assert isinstance(yTest, pd.DataFrame)
#   assert xTrain.shape[0] == yTrain.shape[0]
#   assert xTest.shape[0] == yTest.shape[0]
#   assert abs(( xTest.shape[0] / (xTrain.shape[0] + xTest.shape[0]) ) - split) < tolerance
#   assert abs(( yTest.shape[0] / (yTrain.shape[0] + yTest.shape[0]) ) - split) < tolerance

#   df, rowCounts = ta.readData('./tests/amt-max-10000-per-class.csv', parameters)
#   df, encoding = ta.encodeData(df)
#   df = ta.cleanText(df, parameters)
#   split = 0.3
#   # Split of 0.3 with stratification
#   xTrain, xTest, yTrain, yTest = ta.splitData(df, seed=321, testSize=split, stratify=True)
#   assert abs(( xTest.shape[0] / (xTrain.shape[0] + xTest.shape[0]) ) - split) < tolerance
#   assert abs(( yTest.shape[0] / (yTrain.shape[0] + yTest.shape[0]) ) - split) < tolerance

#   # Ensure the datasets are stratified (same class ratios in training/test sets)
#   trainClassSizes = yTrain['y'].value_counts().tolist()
#   trainRatios = [size/sum(trainClassSizes) for size in trainClassSizes]
#   testClassSizes = yTest['y'].value_counts().tolist()
#   testRatios = [size/sum(testClassSizes) for size in testClassSizes]
#   assert all([abs(trainRatios[i] - testRatios[i]) < tolerance for i in range(len(trainRatios))])


# def test_run_model(setup):
#   '''Tests that text models can be trained on a valid dataset'''
#   parameters, __ = setup
#   model(config, xTrain, xTest, yTrain, yTest, originalLabels, encodedLabels, searchParams, parameters, search='grid', 
#   nproc=1, seed=321):